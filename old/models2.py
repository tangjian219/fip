# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class ApiFirst(models.Model):
    general_id = models.AutoField(primary_key=True)
    id = models.CharField(max_length=10, blank=True, null=True)
    x1 = models.FloatField(blank=True, null=True)
    x2 = models.FloatField(blank=True, null=True)
    x3 = models.FloatField(blank=True, null=True)
    x4 = models.FloatField(blank=True, null=True)
    x5 = models.FloatField(blank=True, null=True)
    y1 = models.FloatField(blank=True, null=True)
    y2 = models.FloatField(blank=True, null=True)
    y3 = models.FloatField(blank=True, null=True)
    y4 = models.FloatField(blank=True, null=True)
    y5 = models.FloatField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'API_FIRST'


class S11(models.Model):
    general_id = models.AutoField(primary_key=True)
    id = models.CharField(max_length=10, blank=True, null=True)
    v01 = models.FloatField(blank=True, null=True)
    v02 = models.FloatField(blank=True, null=True)
    v03 = models.FloatField(blank=True, null=True)
    v04 = models.FloatField(blank=True, null=True)
    v05 = models.FloatField(blank=True, null=True)
    v06 = models.FloatField(blank=True, null=True)
    v07 = models.FloatField(blank=True, null=True)
    v08 = models.FloatField(blank=True, null=True)
    v09 = models.FloatField(blank=True, null=True)
    v10 = models.FloatField(blank=True, null=True)
    v11 = models.FloatField(blank=True, null=True)
    v12 = models.FloatField(blank=True, null=True)
    v13 = models.FloatField(blank=True, null=True)
    v14 = models.FloatField(blank=True, null=True)
    v15 = models.FloatField(blank=True, null=True)
    v16 = models.FloatField(blank=True, null=True)
    v17 = models.FloatField(blank=True, null=True)
    v18 = models.FloatField(blank=True, null=True)
    v19 = models.FloatField(blank=True, null=True)
    v20 = models.FloatField(blank=True, null=True)
    v21 = models.FloatField(blank=True, null=True)
    v22 = models.FloatField(blank=True, null=True)
    v23 = models.FloatField(blank=True, null=True)
    v24 = models.FloatField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'S1_1'


class S12(models.Model):
    general_id = models.AutoField(primary_key=True)
    id = models.CharField(max_length=10, blank=True, null=True)
    x1 = models.FloatField(blank=True, null=True)
    x2 = models.FloatField(blank=True, null=True)
    x3 = models.FloatField(blank=True, null=True)
    x4 = models.FloatField(blank=True, null=True)
    x5 = models.FloatField(blank=True, null=True)
    y1 = models.FloatField(blank=True, null=True)
    y2 = models.FloatField(blank=True, null=True)
    y3 = models.FloatField(blank=True, null=True)
    y4 = models.FloatField(blank=True, null=True)
    y5 = models.FloatField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'S1_2'


class S21(models.Model):
    general_id = models.AutoField(primary_key=True)
    t = models.CharField(db_column='T', max_length=10, blank=True, null=True)  # Field name made lowercase.
    v01 = models.FloatField(blank=True, null=True)
    v02 = models.FloatField(blank=True, null=True)
    v03 = models.FloatField(blank=True, null=True)
    v04 = models.FloatField(blank=True, null=True)
    v05 = models.FloatField(blank=True, null=True)
    v06 = models.FloatField(blank=True, null=True)
    v07 = models.FloatField(blank=True, null=True)
    v08 = models.FloatField(blank=True, null=True)
    v09 = models.FloatField(blank=True, null=True)
    v10 = models.FloatField(blank=True, null=True)
    v11 = models.FloatField(blank=True, null=True)
    v12 = models.FloatField(blank=True, null=True)
    v13 = models.FloatField(blank=True, null=True)
    v14 = models.FloatField(blank=True, null=True)
    v15 = models.FloatField(blank=True, null=True)
    v16 = models.FloatField(blank=True, null=True)
    v17 = models.FloatField(blank=True, null=True)
    v18 = models.FloatField(blank=True, null=True)
    v19 = models.FloatField(blank=True, null=True)
    v20 = models.FloatField(blank=True, null=True)
    v21 = models.FloatField(blank=True, null=True)
    v22 = models.FloatField(blank=True, null=True)
    v23 = models.FloatField(blank=True, null=True)
    v24 = models.FloatField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'S2_1'


class S22(models.Model):
    general_id = models.AutoField(primary_key=True)
    t = models.CharField(db_column='T', max_length=10, blank=True, null=True)  # Field name made lowercase.
    v01 = models.FloatField(blank=True, null=True)
    v02 = models.FloatField(blank=True, null=True)
    v03 = models.FloatField(blank=True, null=True)
    v04 = models.FloatField(blank=True, null=True)
    v05 = models.FloatField(blank=True, null=True)
    v06 = models.FloatField(blank=True, null=True)
    v07 = models.FloatField(blank=True, null=True)
    v08 = models.FloatField(blank=True, null=True)
    v09 = models.FloatField(blank=True, null=True)
    v10 = models.FloatField(blank=True, null=True)
    v11 = models.FloatField(blank=True, null=True)
    v12 = models.FloatField(blank=True, null=True)
    v13 = models.FloatField(blank=True, null=True)
    v14 = models.FloatField(blank=True, null=True)
    v15 = models.FloatField(blank=True, null=True)
    v16 = models.FloatField(blank=True, null=True)
    v17 = models.FloatField(blank=True, null=True)
    v18 = models.FloatField(blank=True, null=True)
    v19 = models.FloatField(blank=True, null=True)
    v20 = models.FloatField(blank=True, null=True)
    v21 = models.FloatField(blank=True, null=True)
    v22 = models.FloatField(blank=True, null=True)
    v23 = models.FloatField(blank=True, null=True)
    v24 = models.FloatField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'S2_2'


