import pymysql
import os

def opendb():
    db = pymysql.connect(
        '114.215.24.89',
        'sd',
        'sd12345678',
        'api',
        port=3306,
        charset='utf8'
    )
    cursor = db.cursor()
    return db, cursor

def init_api_first():
    res = {
        "Information":
        [
            {
                "id": "1",
                "x": [1, 2, 3, 4, 5],
                "y": [123, 234, 345, 456, 567]
            },
            {
                "id": "2",
                "x": [6, 7, 8, 9, 10],
                "y": [1, 5, 3, 7, 9]
            },
            {
                "id": "3",
                "x": [11, 12, 13, 14, 15],
                "y": [1, 10, 100, 1000, 10000]
            }
        ]
    }
    return set_api_first(res)


def get_api_first():
    db, cursor = opendb()
    try:
        res = {}
        res["Information"] = []
        sql = "SELECT * FROM API_FIRST"
        cursor.execute(sql)
        results = cursor.fetchall()
        for row in results:
            temp = {
                "id": row[1],
                "x": list(row[2:7]),
                "y": list(row[7:12])
            }
            res["Information"].append(temp)
        return res
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()


def set_api_first(data):
    db, cursor = opendb()
    try:
        sql = "DELETE FROM API_FIRST"    
        cursor.execute(sql)
        for d in data["Information"]:
            id = tuple([d["id"]])
            x = tuple(d["x"][0:5])
            y = tuple(d["y"][0:5])
            field = "(id, x1, x2, x3, x4, x5, y1, y2, y3, y4, y5)"
            query = id + x + y
            sql = "INSERT INTO API_FIRST{0} VALUES{1}".format(field, str(query))
            cursor.execute(sql)
        db.commit()
        return True
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()


def init_info1():
    res = {
        "Information":
        {
            "Info1": 
            {
                "0": 
                {
                    "id": "G00001",
                    "value": [[10, 20], [20, 30], [20, 40], [30, 20], [10, 40], [30, 50], [40, 20], [10, 60]]
                },
                "1":
                {
                    "id": "G00002",
                    "value": [[110, 120], [210, 130], [120, 140], [130, 210], [10, 40], [310, 150], [140, 20], [110, 60]]
                }
            },
            "Info2":
            {
                "0":
                {
                    "id": "S00001",
                    "value": [[110, 120], [210, 130], [120, 140], [130, 210], [10, 40], [310, 150], [140, 20], [110, 60]]
                },
                "1":
                {
                    "id": "S00002",
                    "value": [[10, 20], [20, 30], [20, 40], [30, 20], [10, 40], [30, 50], [40, 20], [10, 60]]
                }
            }
        }
    }
    return set_info1(res)


def get_info1():
    db, cursor = opendb()
    try:
        res = {}
        res["Information"] = {}
        sql1 = "SELECT * FROM G1_1"
        cursor.execute(sql1)
        results = cursor.fetchall()
        res["Information"]["Info1"] = {}
        for row in results:
            value = []
            for i in range(3, 19, 2):
                value.append([row[i], row[i+1]])
            temp = {
                "id": row[2],
                "value": value
            }
            res["Information"]["Info1"][row[1]] = temp
        sql2 = "SELECT * FROM G1_2"
        cursor.execute(sql2)
        results = cursor.fetchall()
        res["Information"]["Info2"] = {}
        for row in results:
            value = []
            for i in range(3, 19, 2):
                value.append([row[i], row[i+1]])
            temp = {
                "id": row[2],
                "value": value
            }
            res["Information"]["Info2"][row[1]] = temp
        return res
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()


def set_info1(data):
    db, cursor = opendb()
    try:
        sql1 = "SELECT * FROM G1_2"
        cursor.execute(sql1)
        description1 = cursor.description
        sql2 = "SELECT * FROM G1_2"
        cursor.execute(sql2)
        description2 = cursor.description
        sql1 = "DELETE FROM G1_1"    
        cursor.execute(sql1)
        sql2 = "DELETE FROM G1_2"
        cursor.execute(sql2)
        for k, v in data["Information"]["Info1"].items():
            num = tuple([k])
            id = tuple([v["id"]])
            value = []
            for d in v["value"]:
                value.append(d[0])
                value.append(d[1])
            value = tuple(value)
            field = []
            for i in range(1, 19):
                field.append(description1[i][0])
            field = str(tuple(field)).replace("'", "")
            query = num + id + value
            sql = "INSERT INTO G1_1{0} VALUES{1}".format(field, str(query))
            cursor.execute(sql)
        for k, v in data["Information"]["Info2"].items():
            num = tuple([k])
            id = tuple([v["id"]])
            value = []
            for d in v["value"]:
                value.append(d[0])
                value.append(d[1])
            value = tuple(value)
            field = []
            for i in range(1, 19):
                field.append(description2[i][0])
            field = str(tuple(field)).replace("'", "")
            query = num + id + value
            sql = "INSERT INTO G1_2{0} VALUES{1}".format(field, str(query))
            cursor.execute(sql)
        db.commit()
        return True
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()


def init_info2():
    res = {
        "Information":
        {
            "0":
            {
                "9": [136.27, 159.72, 2905.73, 641.42, 1306.3, 1915.57, 1277.44, 1701.5, 124.94, 3064.78, 1583.04, 0],
                "8": [124.36, 145.58, 2562.81, 554.48, 1095.28, 1631.08, 1050.15, 1302.9, 114.15, 2540.1, 1360.56, 0],
                "7": [118.29, 128.85, 2207.34, 477.59, 929.6, 1414.9, 980.57, 1154.33, 113.82, 2261.86, 1163.08, 0],
                "6": [112.83, 122.58, 2034.59, 313.58, 907.95, 1302.02, 916.72, 1088.94, 111.8, 2100.11, 1095.96, 0],
                "5": [101.26, 110.19, 1804.72, 311.97, 762.1, 1133.42, 783.8, 915.38, 101.84, 1816.31, 986.02, 0],
                "4": [88.8, 103.35, 1461.81, 276.77, 634.94, 939.43, 672.76, 750.14, 93.81, 1545.05, 925.1, 0],
                "3": [88.68, 112.38, 1400, 262.42, 589.56, 882.41, 625.61, 684.6, 90.26, 1461.51, 892.83, 0],
                "2": [87.36, 105.28, 1370.43, 276.3, 522.8, 798.43, 568.69, 605.79, 83.45, 1367.58, 814.1, 0],
                "1": [84.11, 89.91, 1064.05, 215.19, 420.1, 615.8, 488.23, 504.8, 81.02, 1162.45, 717.85, 0],
                "0": [82.44, 84.21, 956.84, 197.8, 374.69, 590.2, 446.17, 474.2, 79.68, 1110.44, 685.2, 0]
            },
            "1":
            {
                "9": [3752.48, 5928.32, 13126.86, 6635.26, 8037.69, 12152.15, 5611.48, 5962.41, 7927.89, 25203.28, 16555.58, 0],
                "8": [3388.38, 4840.23, 10707.68, 5234, 6367.69, 9976.82, 4506.31, 5025.15, 7218.32, 21753.93, 14297.93, 0],
                "7": [2855.55, 3987.84, 8959.83, 3993.8, 5114, 7906.34, 3541.92, 4060.72, 6001.78, 18566.37, 11908.49, 0],
                "6": [2626.41, 3709.78, 8701.34, 4242.36, 4376.19, 7158.84, 3097.12, 4319.75, 6085.84, 16993.34, 11567.42, 0],
                "5": [2509.4, 2892.53, 7201.88, 3454.49, 3193.67, 5544.14, 2475.45, 3695.58, 5571.06, 14471.26, 10154.25, 0],
                "4": [2191.43, 2457.08, 6110.43, 2755.66, 2374.96, 4566.83, 1915.29, 3365.31, 4969.95, 12282.89, 8511.51, 0],
                "3": [2026.51, 2135.07, 5271.57, 2357.04, 1773.21, 3869.4, 1580.83, 2971.68, 4381.2, 10524.96, 7164.75, 0],
                "2": [1853.58, 1685.93, 4301.73, 1919.4, 1248.27, 3061.62, 1329.68, 2487.04, 3892.12, 8437.99, 6250.38, 0],
                "1": [1487.15, 1337.31, 3417.56, 1463.38, 967.49, 2898.89, 1098.37, 2084.7, 3209.02, 6787.11, 5096.38, 0],
                "0": [1249.99, 1069.08, 2911.69, 1134.31, 754.78, 2609.85, 943.49, 1843.6, 2622.45, 5604.49, 4090.48, 0]
            },
            "2": 
            {
                "9": [12363.18, 5219.24, 8483.17, 3960.87, 5015.89, 8158.98, 3679.91, 4918.09, 11142.86, 20842.21, 14180.23, 0],
                "8": [10600.84, 4238.65, 7123.77, 3412.38, 4209.03, 6849.37, 3111.12, 4040.55, 9833.51, 17131.45, 12063.82, 0],
                "7": [9179.19, 3405.16, 6068.31, 2886.92, 3696.65, 5891.25, 2756.26, 3371.95, 8930.85, 13629.07, 9918.78, 0],
                "6": [8375.76, 2886.65, 5276.04, 2759.46, 3212.06, 5207.72, 2412.26, 2905.68, 7872.23, 11888.53, 8799.31, 0],
                "5": [7236.15, 2250.04, 4600.72, 2257.99, 2467.41, 4486.74, 2025.44, 2493.04, 6821.11, 9730.91, 7613.46, 0],
                "4": [5837.55, 1902.31, 3895.36, 1846.18, 1934.35, 3798.26, 1687.07, 2096.35, 5508.48, 7914.11, 6281.86, 0],
                "3": [4854.33, 1658.19, 3340.54, 1611.07, 1542.26, 3295.45, 1413.83, 1857.42, 4776.2, 6612.22, 5360.1, 0],
                "2": [4092.27, 1319.76, 2805.47, 1375.67, 1270, 2811.95, 1223.64, 1657.77, 4097.26, 5198.03, 4584.22, 0],
                "1": [3435.95, 1150.81, 2439.68, 1176.65, 1000.79, 2487.85, 1075.48, 1467.9, 3404.19, 4493.31, 3890.79, 0],
                "0": [2982.57, 997.47, 2149.75, 992.69, 811.47, 2258.17, 958.88, 1319.4, 3038.9, 3891.92, 3227.99, 0]
            }
        }
    }
    return set_info2(res)


def get_info2():
    db, cursor = opendb()
    try:
        res = {}
        res["Information"] = {}
        sql = "SELECT * FROM G2"
        cursor.execute(sql)
        results = cursor.fetchall()
        for row in results:
            if row[1] not in res["Information"]:
                res["Information"][row[1]] = {}
            res["Information"][row[1]][row[2]] = list(row[3:15])
        return res
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()


def set_info2(data):
    db, cursor = opendb()
    try:
        sql = "SELECT * FROM G2"
        cursor.execute(sql)
        description = cursor.description
        sql = "DELETE FROM G2"
        cursor.execute(sql)
        for k1, v1 in data["Information"].items():
            id = tuple([k1])
            for k2, v2 in v1.items():
                date = tuple([k2])
                value = tuple(v2)
                field = []
                for i in range(1, 15):
                    field.append(description[i][0])
                field = str(tuple(field)).replace("'", "")
                query = id + date + value
                sql = "INSERT INTO G2{0} VALUES{1}".format(field, str(query))
                cursor.execute(sql)
        db.commit()
        return True
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()


def init_info3():
    res = {
        "state": 1,
        "point1": [821.28, 809.64, 641.44, 640.34, 754.49, 529.95, 645.53, 783.58, 680.62, 821.24, 738.23, 632.29, 521.75, 556.2, 684.39, 766.0, 719.33, 553.99, 607.14, 773.83, 500.3, 794.71, 538.19, 808.55],
        "point2": [792.85, 724.4, 526.81, 808.92, 701.78, 530.89, 785.28, 595.79, 538.14, 779.41, 529.49, 606.19, 640.89, 581.87, 562.73, 572.44, 793.11, 536.1, 724.93, 825.85, 611.61, 778.73, 532.16, 582.83],
        "point3": [797.36, 715.43, 682.13, 514.54, 710.47, 602.7, 643.52, 681.03, 676.58, 731.65, 818.1, 803.26, 556.33, 681.42, 561.39, 507.6, 691.21, 618.76, 550.88, 618.09, 689.33, 557.94, 787.49, 509.36]
    }
    return set_info3(res)


def get_info3():
    db, cursor = opendb()
    try:
        res = {}
        sql = "SELECT * FROM G3"
        cursor.execute(sql)
        results = cursor.fetchall()
        for row in results:
            res["point1"] = list(row[1:25])
            res["point2"] = list(row[25:49])
            res["point3"] = list(row[49:73])
        return res
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()


def set_info3(data):
    db, cursor = opendb()
    try:
        sql = "SELECT * FROM G3"
        cursor.execute(sql)
        description = cursor.description
        sql = "DELETE FROM G3"
        cursor.execute(sql)
        point1 = tuple(data["point1"])
        point2 = tuple(data["point2"])
        point3 = tuple(data["point3"])
        field = []
        for i in range(1, 73):
            field.append(description[i][0])
        field = str(tuple(field)).replace("'", "")
        query = point1 + point2 + point3
        sql = "INSERT INTO G3{0} VALUES{1}".format(field, str(query))
        cursor.execute(sql)
        db.commit()
        return True
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()


def init_info5():
    res = {
        "quantity": 1,
        "profit": 2,
        "count": 2,
        "sheet1":
        {
            "1": 
            {
                "Nq": 1,
                "Nmax": 2,
                "Nmin": 3,
                "Cq": 4,
                "Cmax": 5,
                "Cmin": 6
            },
            "2":
            {
                "Nq": 1,
                "Nmax": 2,
                "Nmin": 3,
                "Cq": 4,
                "Cmax": 5,
                "Cmin": 6
            }
        },
        "sheet2": 
        {
            "1": 
            {
                "info": 
                {
                    "id": "1",
                    "Q": 1,
                    "count": 2,
                    "Cq": 3,
                    "Cp": 4,
                    "eps": 5,
                    "Ccost": 6,
                    "CPerfit": 7,
                    "Fq": 8,
                    "Fc": 9,
                    "Fp": 10,
                    "spend": 11,
                    "pcost": 12
                },
                "sheet2_1": 
                {
                    "t1": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                    "t2": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                    "t3": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                    "t4": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                },
                "sheet2_2": 
                {
                    "1": 
                    {
                        "minL": 1,
                        "maxL": 2,
                        "price": 3
                    },
                    "2": 
                    {
                        "minL": 1,
                        "maxL": 2,
                        "price": 3
                    }
                }
            },
            "2": 
            {
                "info": 
                {
                    "id": "2",
                    "Q": 1,
                    "count": 2,
                    "Cq": 3,
                    "Cp": 4,
                    "eps": 5,
                    "Ccost": 6,
                    "CPerfit": 7,
                    "Fq": 8,
                    "Fc": 9,
                    "Fp": 10,
                    "spend": 11,
                    "pcost": 12
                },
                "sheet2_1": 
                {
                    "t1": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                    "t2": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                    "t3": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                    "t4": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                },
                "sheet2_2": 
                {
                    "1": 
                    {
                        "minL": 1,
                        "maxL": 2,
                        "price": 3
                    },
                    "2": 
                    {
                        "minL": 1,
                        "maxL": 2,
                        "price": 3
                    }
                }
            }
        },
        "data": 
        {
            "1": 
            {
                "id": "1",
                "Cq": 1,
                "Nq": 2,
                "delta": 3,
                "Np": 4,
                "Cp": 5,
                "Nperfit": 6,
                "Npperfit": 7,
                "Fq": 8,
                "Fp": 9,
                "Fperfit": 10,
                "perfit": 11,
                "pperfit": 12
            },
            "2": 
            {
                "id": "2",
                "Cq": 1,
                "Nq": 2,
                "delta": 3,
                "Np": 4,
                "Cp": 5,
                "Nperfit": 6,
                "Npperfit": 7,
                "Fq": 8,
                "Fp": 9,
                "Fperfit": 10,
                "perfit": 11,
                "pperfit": 12
            }
        }
    }
    return set_info5(res)

def get_info5():
    db, cursor = opendb()
    try:
        res = {}
        sql1 = "SELECT * FROM G5_1"
        cursor.execute(sql1)
        results = cursor.fetchall()
        for row in results:
            res["quantity"] = row[1]
            res["profit"] = row[2]
            res["count"] = row[3]
        sql2 = "SELECT * FROM G5_2"
        cursor.execute(sql2)
        results = cursor.fetchall()
        res["sheet1"] = {}
        for row in results:
            temp = {
                "Nq": row[2],
                "Nmax": row[3],
                "Nmin": row[4],
                "Cq": row[5],
                "Cmax": row[6],
                "Cmin": row[7]
            }
            res["sheet1"][row[1]] = temp
        sql3 = "SELECT * FROM G5_3"
        cursor.execute(sql3)
        results = cursor.fetchall()
        res["sheet2"] = {}
        for row in results:
            if row[1] not in res["sheet2"]:
                res["sheet2"][row[1]] = {}
            temp = {
                "id": row[1],
                "Q": row[2],
                "count": row[3],
                "Cq": row[4],
                "Cp": row[5],
                "eps": row[6],
                "Ccost": row[7],
                "CPerfit": row[8],
                "Fq": row[9],
                "Fc": row[10],
                "Fp": row[11],
                "spend": row[12],
                "pcost": row[13]
            }
            res["sheet2"][row[1]]["info"] = temp
        sql4 = "SELECT * FROM G5_4"
        cursor.execute(sql4)
        results = cursor.fetchall()
        for row in results:
            if row[1] not in res["sheet2"]:
                res["sheet2"][row[1]] = {}
            temp = {
                "t1": list(row[2:12]),
                "t2": list(row[12:22]),
                "t3": list(row[22:32]),
                "t4": list(row[32:42]),
            }
            res["sheet2"][row[1]]["sheet2_1"] = temp
        sql5 = "SELECT * FROM G5_5"
        cursor.execute(sql5)
        results = cursor.fetchall()
        for row in results:
            if row[1] not in res["sheet2"]:
                res["sheet2"][row[1]] = {}
            temp = {
                "minL": row[3],
                "maxL": row[4],
                "price": row[5]
            }
            if "sheet2_2" not in res["sheet2"][row[1]]:
                res["sheet2"][row[1]]["sheet2_2"] = {}
            res["sheet2"][row[1]]["sheet2_2"][row[2]] = temp
        sql6 = "SELECT * FROM G5_6"
        cursor.execute(sql6)
        results = cursor.fetchall()
        res["data"] = {}
        for row in results:
            temp = {
                "id": row[1],
                "Cq": row[2],
                "Nq": row[3],
                "delta": row[4],
                "Np": row[5],
                "Cp": row[6],
                "Nperfit": row[7],
                "Npperfit": row[8],
                "Fq": row[9],
                "Fp": row[10],
                "Fperfit": row[11],
                "perfit": row[12],
                "pperfit": row[13]
            }
            res["data"][row[1]] = temp
        return res
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()


def set_info5(data):
    db, cursor = opendb()
    try:
        sql1 = "SELECT * FROM G5_1"
        cursor.execute(sql1)
        description1 = cursor.description
        sql2 = "SELECT * FROM G5_2"
        cursor.execute(sql2)
        description2 = cursor.description
        sql3 = "SELECT * FROM G5_3"
        cursor.execute(sql3)
        description3 = cursor.description
        sql4 = "SELECT * FROM G5_4"
        cursor.execute(sql4)
        description4 = cursor.description
        sql5 = "SELECT * FROM G5_5"
        cursor.execute(sql5)
        description5 = cursor.description
        sql6 = "SELECT * FROM G5_6"
        cursor.execute(sql6)
        description6 = cursor.description
        sql1 = "DELETE FROM G5_1"
        cursor.execute(sql1)
        sql2 = "DELETE FROM G5_2"
        cursor.execute(sql2)
        sql3 = "DELETE FROM G5_3"
        cursor.execute(sql3)
        sql4 = "DELETE FROM G5_4"
        cursor.execute(sql4)
        sql5 = "DELETE FROM G5_5"
        cursor.execute(sql5)
        sql6 = "DELETE FROM G5_6"
        cursor.execute(sql6)
        field = [description1[1][0], description1[2][0], description1[3][0]]
        field = str(tuple(field)).replace("'", "")
        query = (data["quantity"], data["profit"], data["count"])
        sql1 = "INSERT INTO G5_1{0} VALUES{1}".format(field, str(query))
        cursor.execute(sql1)
        for k1, v1 in data["sheet1"].items():
            field = []
            for i in range(1, 8):
                field.append(description2[i][0])
            field = str(tuple(field)).replace("'", "")
            query = []
            query.append(k1)
            for k2, v2 in v1.items():
                query.append(v2)
            query = tuple(query)
            sql2 = "INSERT INTO G5_2{0} VALUES{1}".format(field, str(query))
            cursor.execute(sql2)
        for k1, v1 in data["sheet2"].items():
            field = []
            for i in range(1, 14):
                field.append(description3[i][0])
            field = str(tuple(field)).replace("'", "")
            query = []
            for k2, v2 in v1["info"].items():
                query.append(v2)
            query = tuple(query)
            sql3 = "INSERT INTO G5_3{0} VALUES{1}".format(field, str(query))
            
            cursor.execute(sql3)
            field = []
            for i in range(1, 42):
                field.append(description4[i][0])
            field = str(tuple(field)).replace("'", "")
            query = tuple([k1])
            query = query + tuple(v1["sheet2_1"]["t1"])
            query = query + tuple(v1["sheet2_1"]["t2"])
            query = query + tuple(v1["sheet2_1"]["t3"])
            query = query + tuple(v1["sheet2_1"]["t4"])
            sql4 = "INSERT INTO G5_4{0} VALUES{1}".format(field, str(query))
            cursor.execute(sql4)
            for k2, v2 in v1["sheet2_2"].items():
                field = []
                for i in range(1, 6):
                    field.append(description5[i][0])
                field = str(tuple(field)).replace("'", "")
                query = []
                query.append(k1)
                query.append(k2)
                query.append(v2["minL"])
                query.append(v2["maxL"])
                query.append(v2["price"])
                query = tuple(query)
                sql5 = "INSERT INTO G5_5{0} VALUES{1}".format(field, str(query))
                cursor.execute(sql5)
        for k1, v1 in data["data"].items():
            field = []
            for i in range(1, 14):
                field.append(description6[i][0])
            field = str(tuple(field)).replace("'", "")
            query = []
            for k2, v2 in v1.items():
                query.append(v2)
            query = tuple(query)
            sql6 = "INSERT INTO G5_6{0} VALUES{1}".format(field, str(query)) 
            cursor.execute(sql6)
        db.commit()
        return True
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()


def init_info6():
    res = {
        "state": 1,
        "count": 2,
        "data": 
        [
            {
                "id": "1",
                "Q": 234,
                "count": 234,
                "Cq": 234,
                "Cp": 234,
                "eps": 234,
                "Ccost": 234,
                "CPerfit": 234,
                "Fq": 234,
                "Fc": 234,
                "Fp": 234,
                "spend": 234,
                "pcost": 234
            },
            {
                "id": "2",
                "Q": 234,
                "count": 234,
                "Cq": 234,
                "Cp": 234,
                "eps": 234,
                "Ccost": 234,
                "CPerfit": 234,
                "Fq": 234,
                "Fc": 234,
                "Fp": 234,
                "spend": 234,
                "pcost": 234
            }
        ],
        "sheet1": 
        {
            "0": 
            {
                "q": 234,
                "maxN": 234,
                "minN": 234,
                "Nq": 234,
                "Nmax": 234,
                "Nmin": 234
            },
            "1": 
            {
                "q": 234,
                "maxN": 234,
                "minN": 234,
                "Nq": 234,
                "Nmax": 234,
                "Nmin": 234
            }
        }
    }
    return set_info6(res)


def get_info6():
    db, cursor = opendb()
    try:
        res = {}
        sql1 = "SELECT * FROM G6_1"
        cursor.execute(sql1)
        results = cursor.fetchall()
        for row in results:
            res["count"] = row[1]
        sql2 = "SELECT * FROM G6_2"
        cursor.execute(sql2)
        results = cursor.fetchall()
        res["data"] = []
        for row in results:
            temp = {
                "id": row[1],
                "Q": row[2],
                "count": row[3],
                "Cq": row[4],
                "Cp": row[5],
                "eps": row[6],
                "Ccost": row[7],
                "Cperfit": row[8],
                "Fq": row[9],
                "Fc": row[10],
                "Fp": row[11],
                "spend": row[12],
                "pcost": row[13]
            }
            res["data"].append(temp)
        sql3 = "SELECT * FROM G6_3"
        cursor.execute(sql3)
        results = cursor.fetchall()
        res["sheet1"] = {}
        for row in results:
            temp = {
                "q": row[2],
                "maxN": row[3],
                "minN": row[4],
                "Nq": row[5],
                "Nmax": row[6],
                "Nmin": row[7]
            }
            res["sheet1"][row[1]] = temp
        return res
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()


def set_info6(data):
    db, cursor = opendb()
    try:
        sql1 = "SELECT * FROM G6_1"
        cursor.execute(sql1)
        description1 = cursor.description
        sql2 = "SELECT * FROM G6_2"
        cursor.execute(sql2)
        description2 = cursor.description
        sql3 = "SELECT * FROM G6_3" 
        cursor.execute(sql3)
        description3 = cursor.description
        sql1 = "DELETE FROM G6_1"
        cursor.execute(sql1)
        sql2 = "DELETE FROM G6_2"
        cursor.execute(sql2)
        sql3 = "DELETE FROM G6_3"
        cursor.execute(sql3)
        field = description1[1][0].replace("'", "")
        query = data["count"]
        sql1 = "INSERT INTO G6_1({0}) VALUES({1})".format(field, str(query))
        cursor.execute(sql1)
        for d in data["data"]:
            field = []
            for i in range(1, 14):
                field.append(description2[i][0])
            field = str(tuple(field)).replace("'", "")
            query = []
            for k, v in d.items():
                query.append(v)
            query = tuple(query)
            sql2 = "INSERT INTO G6_2{0} VALUES{1}".format(field, str(query))
            cursor.execute(sql2)
        for k1, v1 in data["sheet1"].items():
            field = []
            for i in range(1, 8):
                field.append(description3[i][0])
            field = str(tuple(field)).replace("'", "")
            query = []
            query.append(k1)
            for k2, v2 in v1.items():
                query.append(v2)
            query = tuple(query)
            sql3 = "INSERT INTO G6_3{0} VALUES{1}".format(field, str(query))
            cursor.execute(sql3)
        db.commit()
        return True
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()


def func():
    db, cursor = opendb()
    try:
        return True
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()

if __name__ == '__main__':
    pass
