from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponse
from .models import ApiFirst, S11, S12
from .dbinterface import *
import json
# Create your views here.


class Test(APIView):

    def query(self):
        return get_api_first()

    def post(self, request):

        if request.method == 'POST':
            data = json.loads(request.body)
            res = {
                'state': data["state"],
                'Information': self.query()
            }
            return Response(res)


class PostInfo1(APIView):
    def query(self):
        return get_info1()

    def post(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            '''res = {
                'state': data["state"],
                'Information':
                {
                    'Info1':
                    {
                        '0':
                        {
                            'id': 'G00001',
                            'value': [[10,20],[20,30],[20,40],[30,20],[10,40],[30,50],[40,20],[10,60]]
                            },
                        '1': 
                        {
                            'id': 'G00002',
                            'value': [[110,120],[210,130],[120,140],[130,210],[10,40],[310,150],[140,20],[110,60]]
                            }
                        },
                    'Info2':
                    {
                        '0':
                        {
                            'id': 'S00001',
                            'value': [[110,120],[210,130],[120,140],[130,210],[10,40],[310,150],[140,20],[110,60]]
                            },
                        '1': 
                        {
                            'id': 'S00002',
                            'value': [[10,20],[20,30],[20,40],[30,20],[10,40],[30,50],[40,20],[10,60]]
                            }
                        }
                    }
                }'''
            res = {
                'state': data['state'],
                'Information': self.query()
            }
            return Response(res)


class PostInfo2(APIView):
    def query(self):
        return get_info2()
    def post(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            res = dict()
            if "Information" in data:
                res = {
                    'state': data["state"],
                    'Information': data["Information"]
                }
            else:
                res = {
                    'state': data['state'],
                    'Information': self.query()
                }
                '''res = {
                    'state': data["state"],
                    'Information': 
                    {
                        0:{
                            9:[136.27,159.72,2905.73,641.42,1306.3,1915.57,1277.44,1701.5,124.94,3064.78,1583.04],
                            8:[124.36,145.58,2562.81,554.48,1095.28,1631.08,1050.15,1302.9,114.15,2540.1,1360.56],
                            7:[118.29,128.85,2207.34,477.59,929.6,1414.9,980.57,1154.33,113.82,2261.86,1163.08],
                            6:[112.83,122.58,2034.59,313.58,907.95,1302.02,916.72,1088.94,111.8,2100.11,1095.96],
                            5:[101.26,110.19,1804.72,311.97,762.1,1133.42,783.8,915.38,101.84,1816.31,986.02],
                            4:[88.8,103.35,1461.81,276.77,634.94,939.43,672.76,750.14,93.81,1545.05,925.1],
                            3:[88.68,112.38,1400,262.42,589.56,882.41,625.61,684.6,90.26,1461.51,892.83],
                            2:[87.36,105.28,1370.43,276.3,522.8,798.43,568.69,605.79,83.45,1367.58,814.1],
                            1:[84.11,89.91,1064.05,215.19,420.1,615.8,488.23,504.8,81.02,1162.45,717.85],
                            0:[82.44,84.21,956.84,197.8,374.69,590.2,446.17,474.2,79.68,1110.44,685.2]
                            },
                        1:{
                            9:[3752.48,5928.32,13126.86,6635.26,8037.69,12152.15,5611.48,5962.41,7927.89,25203.28,16555.58],
                            8:[3388.38,4840.23,10707.68,5234,6367.69,9976.82,4506.31,5025.15,7218.32,21753.93,14297.93],
                            7:[2855.55,3987.84,8959.83,3993.8,5114,7906.34,3541.92,4060.72,6001.78,18566.37,11908.49],
                            6:[2626.41,3709.78,8701.34,4242.36,4376.19,7158.84,3097.12,4319.75,6085.84,16993.34,11567.42],
                            5:[2509.4,2892.53,7201.88,3454.49,3193.67,5544.14,2475.45,3695.58,5571.06,14471.26,10154.25],
                            4:[2191.43,2457.08,6110.43,2755.66,2374.96,4566.83,1915.29,3365.31,4969.95,12282.89,8511.51],
                            3:[2026.51,2135.07,5271.57,2357.04,1773.21,3869.4,1580.83,2971.68,4381.2,10524.96,7164.75],
                            2:[1853.58,1685.93,4301.73,1919.4,1248.27,3061.62,1329.68,2487.04,3892.12,8437.99,6250.38],
                            1:[1487.15,1337.31,3417.56,1463.38,967.49,2898.89,1098.37,2084.7,3209.02,6787.11,5096.38],
                            0:[1249.99,1069.08,2911.69,1134.31,754.78,2609.85,943.49,1843.6,2622.45,5604.49,4090.48]
                            },
                        2:{
                            9:[12363.18,5219.24,8483.17,3960.87,5015.89,8158.98,3679.91,4918.09,11142.86,20842.21,14180.23],
                            8:[10600.84,4238.65,7123.77,3412.38,4209.03,6849.37,3111.12,4040.55,9833.51,17131.45,12063.82],
                            7:[9179.19,3405.16,6068.31,2886.92,3696.65,5891.25,2756.26,3371.95,8930.85,13629.07,9918.78],
                            6:[8375.76,2886.65,5276.04,2759.46,3212.06,5207.72,2412.26,2905.68,7872.23,11888.53,8799.31],
                            5:[7236.15,2250.04,4600.72,2257.99,2467.41,4486.74,2025.44,2493.04,6821.11,9730.91,7613.46],
                            4:[5837.55,1902.31,3895.36,1846.18,1934.35,3798.26,1687.07,2096.35,5508.48,7914.11,6281.86],
                            3:[4854.33,1658.19,3340.54,1611.07,1542.26,3295.45,1413.83,1857.42,4776.2,6612.22,5360.1],
                            2:[4092.27,1319.76,2805.47,1375.67,1270,2811.95,1223.64,1657.77,4097.26,5198.03,4584.22],
                            1:[3435.95,1150.81,2439.68,1176.65,1000.79,2487.85,1075.48,1467.9,3404.19,4493.31,3890.79],
                            0:[2982.57,997.47,2149.75,992.69,811.47,2258.17,958.88,1319.4,3038.9,3891.92,3227.99]
                        }
                    }
                }'''
            return Response(res)

class PostInfo3(APIView):

    def query(self):
        return get_info3()

    def post(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            res = dict()
            if "Information" in data:
                res = {
                    'state': data["state"],
                    'Information': data["Information"]
                }
            else:
                '''res = {
                    'state': data["state"],
                    "point1": [821.28, 809.64, 641.44, 640.34, 754.49, 529.95, 645.53, 783.58, 680.62, 821.24, 738.23, 632.29, 521.75, 556.20, 684.39, 766.00, 719.33, 553.99, 607.14, 773.83, 500.30, 794.71, 538.19, 808.55],
                    "point2": [792.85, 724.40, 526.81, 808.92, 701.78, 530.89, 785.28, 595.79, 538.14, 779.41, 529.49, 606.19, 640.89, 581.87, 562.73, 572.44, 793.11, 536.10, 724.93, 825.85, 611.61, 778.73, 532.16, 582.83],
                    "point3": [797.36, 715.43, 682.13, 514.54, 710.47, 602.70, 643.52, 681.03, 676.58, 731.65, 818.10, 803.26, 556.33, 681.42, 561.39, 507.60, 691.21, 618.76, 550.88, 618.09, 689.33, 557.94, 787.49, 509.36]
                    }'''
                res = {
                    'state': data['state'],
                    'Information': self.query()
                        }
            return Response(res)

class PostInfo4(APIView):

    def post(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            res = dict()
            if "Information" in data:
                res = {
                   'state': data["state"],
                   'Information': data["Information"]
                }
            else:
                res = {
                   'state': data["state"]
                }
            return Response(res)

class PostInfo5(APIView):

    def query(self):
        return get_info5()

    def post(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            res = dict()
            if "Information" in data:
                res = {
                   'state': data["state"],
                   'Information': data["Information"]
                }
            else:
                '''
                res = {
                        'state': data["state"],
                      		"quantity": 1,
		"profit": 2,
		"sheet1":
		{
			"1":
			{
				"Nq": 1,
				"Nmax": 2,
				"Nmin": 3,
				"Cq": 4,
				"Cmax": 5,
				"Cmin": 6
			},
			"2":
			{
				"Nq": 1,
				"Nmax": 2,
				"Nmin": 3,
				"Cq": 4,
				"Cmax": 5,
				"Cmin": 6
			}
		},
		"sheet2":
		{
			"1":
			{
				"info":
				{
					"Q": 1,
					"count": 2,
					"Cq": 3,
					"Cp": 4,
					"eps": 5,
					"Ccost": 6,
					"CPerfit": 7,
					"Fq": 8,
					"Fc": 9,
					"Fp": 10,
					"spend": 11,
					"pcost": 12
				},
				"sheet2_1":
				{
					"t1": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
					"t2": [0, 1, 2, 3, 4, 5 ,6 ,7 ,8 ,9],
					"t3": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
					"t4": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
				},
				"sheet2_2":
				{
					"1":
					{
						"minL": 1,
						"maxL": 2,
						"price": 3 
					},
					"2":
					{
						"minL": 1,
						"maxL": 2,
						"price": 3
					}
				}
			},
			"2":
			{
				"info":
				{
					"Q": 1,
					"count": 2,
					"Cq": 3,
					"Cp": 4,
					"eps": 5,
					"Ccost": 6,
					"CPerfit": 7,
					"Fq": 8,
					"Fc": 9,
					"Fp": 10,
					"spend": 11,
					"pcost": 12
				},
				"sheet2_1":
				{
					"t1": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
					"t2": [0, 1, 2, 3, 4, 5 ,6 ,7 ,8 ,9],
					"t3": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
					"t4": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
				},
				"sheet2_2":
				{
					"1":
					{
						"minL": 1,
						"maxL": 2,
						"price": 3 
					},
					"2":
					{
						"minL": 1,
						"maxL": 2,
						"price": 3
					}
				}
			}
		},
		"sheet3":
		{
			"1":
			{
				"Cq": 1,
				"Nq": 2,
				"delta": 3,
				"Np": 4,
				"Cp": 5,
				"Nperfit": 6, 
				"Npperfit": 7,
				"Fq": 8,
				"Fp": 9,
				"Fperfit": 10,
				"perfit": 11,
				"pperfit": 12				
			},
			"2":
			{
				"Cq": 1,
				"Nq": 2,
				"delta": 3,
				"Np": 4,
				"Cp": 5,
				"Nperfit": 6, 
				"Npperfit": 7,
				"Fq": 8,
				"Fp": 9,
				"Fperfit": 10,
				"perfit": 11,
				"pperfit": 12				
			}
		}
                      }'''
                res = self.query()
                res['state'] = data['state']
            return Response(res)

class PostInfo6(APIView):

    def query(self):
        return get_info6()
    def post(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            res = dict()
            if "Information" in data:
                res = {
                    'state': data["state"],
                    'Information': data["Information"]
                }
            else:
                res = self.query()
                res["state"] = data["state"]
                    
                    '''
                    "state": data["state"],
                    "count": 2,
                    "data":
                    [
                        {
                            "id": 10000,
                            "Q": "user-0",
                            "count": "女",
                            "Cq": "城市-0",
                            "Cp": "签名-0",
                            "eps": 255,
                            "Ccost": 24,
                            "CPerfit": 82830700,
                            "Fq": "作家",
                            "Fc": 57,
                            "Fp": 57,
                            "spend": 57,
                            "pcost": 57
                            },
                        {
                            "id": 10000,
                            "Q": "user-0",
                            "count": "女",
                            "Cq": "城市-0",
                            "Cp": "签名-0",
                            "eps": 255,
                            "Ccost": 24,
                            "CPerfit": 82830700,
                            "Fq": "作家",
                            "Fc": 57,
                            "Fp": 57,
                            "spend": 57,
                            "pcost": 57
                            }
                        ],
                    "sheet1":
                    {
                        "0":
                        {
                            "q": 234,
                            "maxN": 234,
                            "minN": 234,
                            "Nq": 234,
                            "Nmax": 234,
                            "Nmin": 234,
                            },
                        "1":
                        {
                            "q": 234,
                            "maxN": 234,
                            "minN": 234,
                            "Nq": 234,
                            "Nmax": 234,
                            "Nmin": 234,
                            }
                        }
                    }'''
            return Response(res)
