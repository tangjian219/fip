from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
import pymysql
import json
# Create your views here.

def opendb():
    db = pymysql.connect(
        '114.215.24.89',
        'sd',
        'sd12345678',
        'api',
        port=3306,
        charset='utf8'
    )
    cursor = db.cursor()
    return db, cursor


class Test(APIView):

    def query(self):
        db, cursor = opendb()
        try:
            res = {}
            res["Information"] = []
            sql = "SELECT * FROM API_FIRST"
            cursor.execute(sql)
            results = cursor.fetchall()
            for row in results:
                temp = {
                    "id": row[1],
                    "x": list(row[2:7]),
                    "y": list(row[7:12])
                }
                res["Information"].append(temp)
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def modify(self, data):
        db, cursor = opendb()
        try:
            sql = "DELETE FROM API_FIRST"    
            cursor.execute(sql)
            for d in data["Information"]:
                id = tuple([d["id"]])
                x = tuple(d["x"][0:5])
                y = tuple(d["y"][0:5])
                field = "(id, x1, x2, x3, x4, x5, y1, y2, y3, y4, y5)"
                query = id + x + y
                sql = "INSERT INTO API_FIRST{0} VALUES{1}".format(field, str(query))
                cursor.execute(sql)
            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def post(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            if data["state"] > 0:
                res = self.query()
                res["state"] = data["state"]
                return Response(res)
            elif data["state"] == -1:
                res = self.modify(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            else:
                return None

class PostInfo1(APIView):

    def query(self):
        db, cursor = opendb()
        try:
            res = {}
            res["Information"] = {}
            sql1 = "SELECT * FROM G1_1"
            cursor.execute(sql1)
            results = cursor.fetchall()
            res["Information"]["Info1"] = {}
            for row in results:
                value = []
                for i in range(3, 19, 2):
                    value.append([row[i], row[i+1]])
                temp = {
                    "id": row[2],
                    "value": value
                }
                res["Information"]["Info1"][row[1]] = temp
            sql2 = "SELECT * FROM G1_2"
            cursor.execute(sql2)
            results = cursor.fetchall()
            res["Information"]["Info2"] = {}
            for row in results:
                value = []
                for i in range(3, 19, 2):
                    value.append([row[i], row[i+1]])
                temp = {
                    "id": row[2],
                    "value": value
                }
                res["Information"]["Info2"][row[1]] = temp
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def modify(self, data):
        db, cursor = opendb()
        try:
            sql1 = "SELECT * FROM G1_2"
            cursor.execute(sql1)
            description1 = cursor.description
            sql2 = "SELECT * FROM G1_2"
            cursor.execute(sql2)
            description2 = cursor.description
            sql1 = "DELETE FROM G1_1"    
            cursor.execute(sql1)
            sql2 = "DELETE FROM G1_2"
            cursor.execute(sql2)
            for k, v in data["Information"]["Info1"].items():
                num = tuple([k])
                id = tuple([v["id"]])
                value = []
                for d in v["value"]:
                    value.append(d[0])
                    value.append(d[1])
                value = tuple(value)
                field = []
                for i in range(1, 19):
                    field.append(description1[i][0])
                field = str(tuple(field)).replace("'", "")
                query = num + id + value
                sql = "INSERT INTO G1_1{0} VALUES{1}".format(field, str(query))
                cursor.execute(sql)
            for k, v in data["Information"]["Info2"].items():
                num = tuple([k])
                id = tuple([v["id"]])
                value = []
                for d in v["value"]:
                    value.append(d[0])
                    value.append(d[1])
                value = tuple(value)
                field = []
                for i in range(1, 19):
                    field.append(description2[i][0])
                field = str(tuple(field)).replace("'", "")
                query = num + id + value
                sql = "INSERT INTO G1_2{0} VALUES{1}".format(field, str(query))
                cursor.execute(sql)
            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def post(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            if data["state"] > 0:
                res = self.query()
                res["state"] = data["state"]
                return Response(res)
            elif data["state"] == -1:
                res = self.modify(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            else:
                return None


class PostInfo2(APIView):

    def query(self):
        db, cursor = opendb()
        try:
            res = {}
            res["Information"] = {}
            sql = "SELECT * FROM G2"
            cursor.execute(sql)
            results = cursor.fetchall()
            for row in results:
                if row[1] not in res["Information"]:
                    res["Information"][row[1]] = {}
                res["Information"][row[1]][row[2]] = list(row[3:15])
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def modify(self, data):
        db, cursor = opendb()
        try:
            sql = "SELECT * FROM G2"
            cursor.execute(sql)
            description = cursor.description
            sql = "DELETE FROM G2"
            cursor.execute(sql)
            for k1, v1 in data["Information"].items():
                id = tuple([k1])
                for k2, v2 in v1.items():
                    date = tuple([k2])
                    value = tuple(v2)
                    field = []
                    for i in range(1, 15):
                        field.append(description[i][0])
                    field = str(tuple(field)).replace("'", "")
                    query = id + date + value
                    sql = "INSERT INTO G2{0} VALUES{1}".format(field, str(query))
                    cursor.execute(sql)
            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def post(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            if data["state"] > 0:
                res = self.query()
                res["state"] = data["state"]
                return Response(res)
            elif data["state"] == -1:
                res = self.modify(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            else:
                return None


class PostInfo3(APIView):

    def query(self):
        db, cursor = opendb()
        try:
            res = {}
            sql = "SELECT * FROM G3"
            cursor.execute(sql)
            results = cursor.fetchall()
            for row in results:
                res["point1"] = list(row[1:25])
                res["point2"] = list(row[25:49])
                res["point3"] = list(row[49:73])
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def modify(self, data):
        db, cursor = opendb()
        try:
            sql = "SELECT * FROM G3"
            cursor.execute(sql)
            description = cursor.description
            sql = "DELETE FROM G3"
            cursor.execute(sql)
            point1 = tuple(data["point1"])
            point2 = tuple(data["point2"])
            point3 = tuple(data["point3"])
            field = []
            for i in range(1, 73):
                field.append(description[i][0])
            field = str(tuple(field)).replace("'", "")
            query = point1 + point2 + point3
            sql = "INSERT INTO G3{0} VALUES{1}".format(field, str(query))
            cursor.execute(sql)
            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def post(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            if data["state"] > 0:
                res = self.query()
                res["state"] = data["state"]
                return Response(res)
            elif data["state"] == -1:
                res = self.modify(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            else:
                return None


class PostInfo4(APIView):

    def query(self):
        db, cursor = opendb()
        try:
            res = {}
            sql = "SELECT * FROM G3"
            cursor.execute(sql)
            results = cursor.fetchall()
            for row in results:
                res["point1"] = list(row[1:25])
                res["point2"] = list(row[25:49])
                res["point3"] = list(row[49:73])
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def modify(self, data):
        db, cursor = opendb()
        try:
            sql = "SELECT * FROM G3"
            cursor.execute(sql)
            description = cursor.description
            sql = "DELETE FROM G3"
            cursor.execute(sql)
            point1 = tuple(data["point1"])
            point2 = tuple(data["point2"])
            point3 = tuple(data["point3"])
            field = []
            for i in range(1, 73):
                field.append(description[i][0])
            field = str(tuple(field)).replace("'", "")
            query = point1 + point2 + point3
            sql = "INSERT INTO G3{0} VALUES{1}".format(field, str(query))
            cursor.execute(sql)
            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def post(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            if data["state"] > 0:
                res = self.query()
                res["state"] = data["state"]
                return Response(res)
            elif data["state"] == -1:
                res = self.modify(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            else:
                return None


class PostInfo5(APIView):

    def query(self):
        db, cursor = opendb()
        try:
            res = {}
            sql1 = "SELECT * FROM G5_1"
            cursor.execute(sql1)
            results = cursor.fetchall()
            for row in results:
                res["quantity"] = row[1]
                res["profit"] = row[2]
                res["count"] = row[3]
            sql2 = "SELECT * FROM G5_2"
            cursor.execute(sql2)
            results = cursor.fetchall()
            res["sheet1"] = {}
            for row in results:
                temp = {
                    "Nq": row[2],
                    "Nmax": row[3],
                    "Nmin": row[4],
                    "Cq": row[5],
                    "Cmax": row[6],
                    "Cmin": row[7]
                }
                res["sheet1"][row[1]] = temp
            sql3 = "SELECT * FROM G5_3"
            cursor.execute(sql3)
            results = cursor.fetchall()
            res["sheet2"] = {}
            for row in results:
                if row[1] not in res["sheet2"]:
                    res["sheet2"][row[1]] = {}
                temp = {
                    "id": row[1],
                    "Q": row[2],
                    "count": row[3],
                    "Cq": row[4],
                    "Cp": row[5],
                    "eps": row[6],
                    "Ccost": row[7],
                    "CPerfit": row[8],
                    "Fq": row[9],
                    "Fc": row[10],
                    "Fp": row[11],
                    "spend": row[12],
                    "pcost": row[13]
                }
                res["sheet2"][row[1]]["info"] = temp
            sql4 = "SELECT * FROM G5_4"
            cursor.execute(sql4)
            results = cursor.fetchall()
            for row in results:
                if row[1] not in res["sheet2"]:
                    res["sheet2"][row[1]] = {}
                temp = {
                    "t1": list(row[2:12]),
                    "t2": list(row[12:22]),
                    "t3": list(row[22:32]),
                    "t4": list(row[32:42]),
                }
                res["sheet2"][row[1]]["sheet2_1"] = temp
            sql5 = "SELECT * FROM G5_5"
            cursor.execute(sql5)
            results = cursor.fetchall()
            for row in results:
                if row[1] not in res["sheet2"]:
                    res["sheet2"][row[1]] = {}
                temp = {
                    "minL": row[3],
                    "maxL": row[4],
                    "price": row[5]
                }
                if "sheet2_2" not in res["sheet2"][row[1]]:
                    res["sheet2"][row[1]]["sheet2_2"] = {}
                res["sheet2"][row[1]]["sheet2_2"][row[2]] = temp
            sql6 = "SELECT * FROM G5_6"
            cursor.execute(sql6)
            results = cursor.fetchall()
            res["data"] = {}
            for row in results:
                temp = {
                    "id": row[1],
                    "Cq": row[2],
                    "Nq": row[3],
                    "delta": row[4],
                    "Np": row[5],
                    "Cp": row[6],
                    "Nperfit": row[7],
                    "Npperfit": row[8],
                    "Fq": row[9],
                    "Fp": row[10],
                    "Fperfit": row[11],
                    "perfit": row[12],
                    "pperfit": row[13]
                }
                res["data"][row[1]] = temp
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def modify(self, data):
        db, cursor = opendb()
        try:
            sql1 = "SELECT * FROM G5_1"
            cursor.execute(sql1)
            description1 = cursor.description
            sql2 = "SELECT * FROM G5_2"
            cursor.execute(sql2)
            description2 = cursor.description
            sql3 = "SELECT * FROM G5_3"
            cursor.execute(sql3)
            description3 = cursor.description
            sql4 = "SELECT * FROM G5_4"
            cursor.execute(sql4)
            description4 = cursor.description
            sql5 = "SELECT * FROM G5_5"
            cursor.execute(sql5)
            description5 = cursor.description
            sql6 = "SELECT * FROM G5_6"
            cursor.execute(sql6)
            description6 = cursor.description
            sql1 = "DELETE FROM G5_1"
            cursor.execute(sql1)
            sql2 = "DELETE FROM G5_2"
            cursor.execute(sql2)
            sql3 = "DELETE FROM G5_3"
            cursor.execute(sql3)
            sql4 = "DELETE FROM G5_4"
            cursor.execute(sql4)
            sql5 = "DELETE FROM G5_5"
            cursor.execute(sql5)
            sql6 = "DELETE FROM G5_6"
            cursor.execute(sql6)
            field = [description1[1][0], description1[2][0], description1[3][0]]
            field = str(tuple(field)).replace("'", "")
            query = (data["quantity"], data["profit"], data["count"])
            sql1 = "INSERT INTO G5_1{0} VALUES{1}".format(field, str(query))
            cursor.execute(sql1)
            for k1, v1 in data["sheet1"].items():
                field = []
                for i in range(1, 8):
                    field.append(description2[i][0])
                field = str(tuple(field)).replace("'", "")
                query = []
                query.append(k1)
                for k2, v2 in v1.items():
                    query.append(v2)
                query = tuple(query)
                sql2 = "INSERT INTO G5_2{0} VALUES{1}".format(field, str(query))
                cursor.execute(sql2)
            for k1, v1 in data["sheet2"].items():
                field = []
                for i in range(1, 14):
                    field.append(description3[i][0])
                field = str(tuple(field)).replace("'", "")
                query = []
                for k2, v2 in v1["info"].items():
                    query.append(v2)
                query = tuple(query)
                sql3 = "INSERT INTO G5_3{0} VALUES{1}".format(field, str(query))
                
                cursor.execute(sql3)
                field = []
                for i in range(1, 42):
                    field.append(description4[i][0])
                field = str(tuple(field)).replace("'", "")
                query = tuple([k1])
                query = query + tuple(v1["sheet2_1"]["t1"])
                query = query + tuple(v1["sheet2_1"]["t2"])
                query = query + tuple(v1["sheet2_1"]["t3"])
                query = query + tuple(v1["sheet2_1"]["t4"])
                sql4 = "INSERT INTO G5_4{0} VALUES{1}".format(field, str(query))
                cursor.execute(sql4)
                for k2, v2 in v1["sheet2_2"].items():
                    field = []
                    for i in range(1, 6):
                        field.append(description5[i][0])
                    field = str(tuple(field)).replace("'", "")
                    query = []
                    query.append(k1)
                    query.append(k2)
                    query.append(v2["minL"])
                    query.append(v2["maxL"])
                    query.append(v2["price"])
                    query = tuple(query)
                    sql5 = "INSERT INTO G5_5{0} VALUES{1}".format(field, str(query))
                    cursor.execute(sql5)
            for k1, v1 in data["data"].items():
                field = []
                for i in range(1, 14):
                    field.append(description6[i][0])
                field = str(tuple(field)).replace("'", "")
                query = []
                for k2, v2 in v1.items():
                    query.append(v2)
                query = tuple(query)
                sql6 = "INSERT INTO G5_6{0} VALUES{1}".format(field, str(query)) 
                cursor.execute(sql6)
            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def post(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            if data["state"] > 0:
                res = self.query()
                res["state"] = data["state"]
                return Response(res)
            elif data["state"] == -1:
                res = self.modify(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            else:
                return None


class PostInfo6(APIView):

    def query(self):
        db, cursor = opendb()
        try:
            res = {}
            sql1 = "SELECT * FROM G6_1"
            cursor.execute(sql1)
            results = cursor.fetchall()
            for row in results:
                res["count"] = row[1]
            sql2 = "SELECT * FROM G6_2"
            cursor.execute(sql2)
            results = cursor.fetchall()
            res["data"] = []
            for row in results:
                temp = {
                    "id": row[1],
                    "Q": row[2],
                    "count": row[3],
                    "Cq": row[4],
                    "Cp": row[5],
                    "eps": row[6],
                    "Ccost": row[7],
                    "Cperfit": row[8],
                    "Fq": row[9],
                    "Fc": row[10],
                    "Fp": row[11],
                    "spend": row[12],
                    "pcost": row[13]
                }
                res["data"].append(temp)
            sql3 = "SELECT * FROM G6_3"
            cursor.execute(sql3)
            results = cursor.fetchall()
            res["sheet1"] = {}
            for row in results:
                temp = {
                    "q": row[2],
                    "maxN": row[3],
                    "minN": row[4],
                    "Nq": row[5],
                    "Nmax": row[6],
                    "Nmin": row[7]
                }
                res["sheet1"][row[1]] = temp
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def modify(self, data):
        db, cursor = opendb()
        try:
            sql1 = "SELECT * FROM G6_1"
            cursor.execute(sql1)
            description1 = cursor.description
            sql2 = "SELECT * FROM G6_2"
            cursor.execute(sql2)
            description2 = cursor.description
            sql3 = "SELECT * FROM G6_3" 
            cursor.execute(sql3)
            description3 = cursor.description
            sql1 = "DELETE FROM G6_1"
            cursor.execute(sql1)
            sql2 = "DELETE FROM G6_2"
            cursor.execute(sql2)
            sql3 = "DELETE FROM G6_3"
            cursor.execute(sql3)
            field = description1[1][0].replace("'", "")
            query = data["count"]
            sql1 = "INSERT INTO G6_1({0}) VALUES({1})".format(field, str(query))
            cursor.execute(sql1)
            for d in data["data"]:
                field = []
                for i in range(1, 14):
                    field.append(description2[i][0])
                field = str(tuple(field)).replace("'", "")
                query = []
                for k, v in d.items():
                    query.append(v)
                query = tuple(query)
                sql2 = "INSERT INTO G6_2{0} VALUES{1}".format(field, str(query))
                cursor.execute(sql2)
            for k1, v1 in data["sheet1"].items():
                field = []
                for i in range(1, 8):
                    field.append(description3[i][0])
                field = str(tuple(field)).replace("'", "")
                query = []
                query.append(k1)
                for k2, v2 in v1.items():
                    query.append(v2)
                query = tuple(query)
                sql3 = "INSERT INTO G6_3{0} VALUES{1}".format(field, str(query))
                cursor.execute(sql3)
            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def post(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            if data["state"] > 0:
                res = self.query()
                res["state"] = data["state"]
                return Response(res)
            elif data["state"] == -1:
                res = self.modify(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            else:
                return None


class PostInfo7(APIView):

    def post(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            res = {
	"sheet1":
	{
	  	"1":
	  	{
	      	"point1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
	      	"point2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
	      	"point3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
	  	},
	  	"2":
        {
	      	"point1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
	      	"point2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
	      	"point3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
	  	},
	},
	"sheet2":
	{
		"1":
		{
	    	"point1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
	      	"point2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
	      	"point3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
		},
	  	"2":
		{
	      	"point1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
	      	"point2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
	      	"point3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
		}
	},
	"sheet3":
	{
		"1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		"2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
	},
	"sheet4":
	{
		"1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		"2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
	},
	"sheet5":
	{
		"1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		"2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
	},
	"sheet6":
	{
		"1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		"2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
	}
}
            res["state"] = data["state"]
            return Response(res)


class PostInfo8(APIView):

    def post(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            res = {
	"sheet1":
	{
		"1":
		{
		  "point1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point4": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
		},
		"2":
		{
		  "point1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point4": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
		}
	},
	"sheet2":
	{
		"1":
		{
		  "point1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point4": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
		},
		"2":
		{
		  "point1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point4": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
		}
	},
	"sheet3":
	{
		"1":
		{
		  "point1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point4": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
		},
		"2":
		{
		  "point1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point4": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
		}
	},
	"sheet4":
	{
		"1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
		"2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
	},
	"sheet5":
	{
		"1":
		{
		  "point1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
		},
		"2":
		{
		  "point1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
		}	
	}
}
            res["state"] = data["state"]
            return Response(res)


class PostInfo9(APIView):

    def post(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            res = {
	"sheet1":
	{
		"1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
		"2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
	},
	"sheet2":
	{
		"1":
		{
		  "point1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
		},
		"2":
		{
		  "point1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
		}	
	},
	"sheet3":
	{
		"1":
		{
		  "point1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
		},
		"2":
		{
		  "point1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
		  "point2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
		}
	}
}
            res["state"] = data["state"]
            return Response(res)


class PostInfo10(APIView):

    def post(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            res = {
	"sheet1":
	{
		"1":
		{
			"1":
			{
				"1":
				{
					"point1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
					"point2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
				}
			}
		}
	},
	"sheet2":
	{
		"1":
		{
			"1":
			{
				"1":
				{
					"point1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
					"point2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
				}
			}
		}
	},
	"sheet3":
	{
		"1":
		{
			"1":
			{
				"1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
				"2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
			}
		}
	},
	"sheet4":
	{
		"1":
		{
			"1":
			{
				"point1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
				"point2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
			}
		}
	} 
}
            res["state"] = data["state"]
            return Response(res)


class PostInfo11(APIView):

    def post(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            res = {
	"data":
	[
		{
			"id": "1",
			"info1":
			{
				"Nq": 1,
				"Cq": 2,
				"Ncost": 3,
				"Ccost": 4,
				"Navg": 5,
				"Cavg": 6
			},
			"info2":
			{
				"Nq": 1,
				"Cq": 2,
				"Ncost": 3,
				"Ccost": 4,
				"Navg": 5,
				"Cavg": 6
			}
		},
		{
			"id": "2",
			"info1":
			{
				"Nq": 1,
				"Cq": 2,
				"Ncost": 3,
				"Ccost": 4,
				"Navg": 5,
				"Cavg": 6
			},
			"info2":
			{
				"Nq": 1,
				"Cq": 2,
				"Ncost": 3,
				"Ccost": 4,
				"Navg": 5,
				"Cavg": 6
			}
		},
		
	] 
}
            res["state"] = data["state"]
            return Response(res)
