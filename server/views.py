from django.shortcuts import render
from django.db.models import Q
from rest_framework.views import APIView
from rest_framework.response import Response
from server import models
from qiniu import Auth, put_file, etag
import qiniu.config
import pymysql
import requests
import json
import sys
import datetime
import time


# from datetime import date, timedelta
# Create your views here.

graphTable = "Graph"
logTable = "logs"
GenOffer = "G1"


def opendb(dbname="api"):
    db = pymysql.connect(
        '119.3.213.127',
        'sd',
        'sd12345678',
        dbname,
        port=3306,
        charset='utf8'
    )
    cursor = db.cursor()
    return db, cursor


# def opendb1():
#     db = pymysql.connect(
#         host='119.3.213.127',
#         user='sd',
#         password='sd12345678',
#         database='api',
#         port=3306,
#         charset='utf8'
#     )
#     cursor = db.cursor()
#     return db, cursor


def getGraph(page, id="", date=""):
    print("running in " + sys._getframe().f_code.co_name + ",page: " + page + ",id: " + id)
    field = "v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17,v18,v19,v20,v21,v22,v23,v24,\
                sheet,caption,axis_x,axis_y,ctitle"
    db, cursor = opendb()
    try:
        res = {}
        # page = data["page"]
        sql = "SELECT " + field + " FROM " + graphTable + " where page='" + page + "'"
        if id not in ["master", "iso"]:
            sql += " AND id='" + id + "'"
        cursor.execute(sql)
        results = cursor.fetchall()

        captions = dict()

        # for c in range(1, 7):
        #     res["sheet%d"%(c)] = {}
        for row in results:
            sheetid = row[24]
            if sheetid not in res:
                res[sheetid] = {}
            num = len(res[sheetid])
            # id = row[0]
            res[sheetid][num] = {}
            res[sheetid][num]["id"] = row[-1]
            res[sheetid][num]["value"] = list(row[:24])
            # if sheetid not in captions.keys():
            captions[sheetid] = row[25:28]  # [row[-3],row[-2],row[-1]]
            # res[sheetid][num]["caption"] = captions[sheetid][0]
        # print(captions)
        for sheetid in captions.keys():
            res[sheetid]["caption"] = captions[sheetid][0]
            res[sheetid]["axis_x"] = captions[sheetid][1]
            res[sheetid]["axis_y"] = captions[sheetid][2]
        # print("the got graph is: ")
        # print(res)
        return res
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()


def setGraph(data):
    print("running in " + sys._getframe().f_code.co_name)
    db, cursor = opendb()
    field = "(id,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17,v18,v19,v20,v21,v22,v23,v24,page,sheet,caption,axis_x,axis_y,ctitle)"
    try:
        page = data["page"]
        # sql = "SELECT * FROM G7_new where page='" + page + "'"
        # description = []
        # cursor.execute(sql)
        # description = cursor.description
        # field = []
        # for i in range(1, len(description)):
        #     field.append(description[i][0])
        # field = str(tuple(field)).replace("'", "")
        # # print(field)

        sql = "DELETE FROM " + graphTable + " where page='" + page + "'"
        cursor.execute(sql)
        # print(description)

        for k1, v1 in data["Information"].items():
            sheet = k1
            caption = v1["caption"]
            axis_x = v1["axis_x"]
            axis_y = v1["axis_y"]
            for k2, v2 in v1["data"].items():
                id = k2
                value = v2
                query = tuple([id]) + tuple(value) + tuple([page]) + tuple([sheet]) + tuple([caption]) + tuple(
                    [axis_x]) + tuple([axis_y]) + tuple([k2])
                query = str(tuple(query))
                sql = "INSERT INTO {0}{1} VALUES{2}".format(graphTable, field, query)
                # print(sql)
                cursor.execute(sql)

        db.commit()
        return True
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()


def setGraph_new(data):
    print("running in " + sys._getframe().f_code.co_name)
    db, cursor = opendb()
    field = "(id,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17,v18,v19,v20,v21,v22,v23,v24,page,sheet,caption,axis_x,axis_y,ctitle)"
    try:
        page = data["page"]
        sql = "DELETE FROM " + graphTable + " where page='" + page + "'"
        cursor.execute(sql)
        for k1, v1 in data["Graphs"].items():
            sheet = k1
            caption = v1["caption"]
            axis_x = v1["axis_x"]
            axis_y = v1["axis_y"]
            for k2, v2 in v1["data"].items():
                id = v2.get("id","")
                value = v2.get("value",[])
                title = v2.get("title","")
                query = tuple([id]) + tuple(value) + tuple([page]) + tuple([sheet]) + tuple([caption]) + tuple(
                    [axis_x]) + tuple([axis_y]) + tuple([title])
                query = str(tuple(query))
                sql = "INSERT INTO {0}{1} VALUES{2}".format(graphTable, field, query)
                cursor.execute(sql)
                # for k3, v3 in v2.items():
                #     id = v3.get("id", default="")
                #     value = v3.get("value", default=[])
                #     title = v3.get("title", default="")
                #     query = tuple([id]) + tuple(value) + tuple([page]) + tuple([sheet]) + tuple([caption]) + tuple(
                #         [axis_x]) + tuple([axis_y]) + tuple([title])
                #     query = str(tuple(query))
                #     sql = "INSERT INTO {0}{1} VALUES{2}".format(graphTable, field, query)
                #     print('ssss')
                #     cursor.execute(sql)

        db.commit()
        return True
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()


def updateGraph(data):
    print("running in " + sys._getframe().f_code.co_name)
    field = "v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17,v18,v19,v20,v21,v22,v23,v24"
    field = field.split(",")
    db, cursor = opendb()
    try:
        page = data["page"]
        res_list = []
        for k1, v1 in data["Information"].items():
            if k1 == 'sheet2' or k1 == 'sheet3':
                for k2, v2 in v1["data"].items():
                    id = k2
                    value = v2
                    sql = []
                    for k3 in range(0, 24):
                        sql.append(field[k3] + "=" + str(value[k3]))
                    sql0 = "SELECT id FROM Graph WHERE id='"+id+"' and page='"+page+"'"
                    cursor.execute(sql0)
                    results = cursor.fetchone()
                    if results:
                        res_list.append(results)
                    sql = "UPDATE " + graphTable + " SET " + ",".join(sql) + " where id='" + id + "' and page='"+page+"'"
                    cursor.execute(sql)
        if not res_list:
            return None
        db.commit()
        return True
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()


# 在log中记录访问
def accesslog(data):
    print("running in " + sys._getframe().f_code.co_name)
    db, cursor = opendb()
    id = data["user"]["id"]
    # if id == "master":
    #     return
    state = data["state"]
    page = data["page"]
    today = datetime.date.today()
    date = today.strftime('%y%m%d')
    # print(date)
    field = ('id', 'date', 'page', 'state')
    field = str(field).replace("'", "")
    query = str(tuple([id]) + tuple([date]) + tuple([page]) + tuple([state]))
    # print(query)
    sql = "INSERT INTO {0}{1} VALUES{2}".format(logTable, field, query)
    cursor.execute(sql)
    # print(sql)
    db.commit()


def querylog(start, end):
    print("running in " + sys._getframe().f_code.co_name)
    db, cursor = opendb()
    try:
        res = {}
        sql = "SELECT DISTINCT id FROM " + logTable + " WHERE time>='" + start + "' AND time<'" \
              + end + "' AND page='config' And state < 0"
        # print(sql)
        cursor.execute(sql)
        results = cursor.fetchall()
        records = []
        for submit in results:
            if submit[0] not in {"master", "iso"}:
                records.append(submit[0])
        # res["user"] = records
        # print(res)
        return records
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()


def getGenOffer(cursor, id):
    print("running in " + sys._getframe().f_code.co_name)
    if id == "master" or id == 'iso':
        sql = "SELECT * FROM " + GenOffer + " ORDER BY id"
        print(sql)
    else:
        sql = "SELECT * FROM " + GenOffer + " where id='" + id + "'"  # G1_1 where id='"+id+"'"
    cursor.execute(sql)
    ret = cursor.fetchall()
    return ret


def updateGenOffer(data):
    print("running in " + sys._getframe().f_code.co_name)
    # db, cursor = opendb()
    print(data)
    field = ["x1", "y1", "x2", "y2", "x3", "y3", "x4", "y4", "x5", "y5", "x6", "y6"]
    try:
        db, cursor = opendb()
        # id = data["Information"]['sheet1'][0]['id']
        # value = data["Information"]['sheet1'][0]["value"]

        # sql = "SELECT * FROM " + GenOffer + " where id='"+id+"'"
        # cursor.execute(sql)
        # description = cursor.description
        # sql = "DELETE FROM " + GenOffer + " where id='"+id+"'"
        # cursor.execute(sql)

        # field = []
        # for i in range(1, len(description)):
        #     field.append(description[i][0])
        # field = str(tuple(field)).replace("'", "")
        # for k in data["Information"]["sheet1"]:
        # role = tuple(["G"])
        # id = tuple([k["id"]])
        # query = id
        # lower = float(k["value"][0][1])
        # for d in k["value"]:
        #     if float(d[1]) < lower:
        #         return None
        #     lower = float(d[1])

        #     x, y = tuple([float(d[0])]), tuple([float(d[1])])
        #     query += x+y

        # # print(query)
        # query = str(query)
        # sql = "INSERT INTO {0}{1} VALUES{2}".format(GenOffer, field, query)

        id = data["Information"]["sheet1"][0]["id"]
        d = data["Information"]["sheet1"][0]["value"]
        if float(d[0][1]) <= float(d[1][1]) <= float(d[2][1]) <= float(d[3][1]) <= \
                float(d[4][1]) <= float(d[5][1]):
            value = [d[i][j] for i in range(6) for j in range(2)]
            sql = [field[i] + "=" + str(value[i]) for i in range(12)]
            sql = "UPDATE " + GenOffer + " SET " + ",".join(sql) + " where id='" + id + "'"

            record = "id: " + id + ", " + sql
            print(record)
            cursor.execute(sql)
        else:
            return None

            # record = "id: "+id[0]+", "+sql
            # print(record)
            # cursor.execute(sql)
        db.commit()
        return True
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()


def resetGenOffer(data):
    print("running in " + sys._getframe().f_code.co_name)
    field = "(id, x1, y1, x2, y2, x3, y3, x4, y4, x5, y5, x6, y6)"
    # field1 = "id, x1, y1, x2, y2, x3, y3, x4, y4, x5, y5, x6, y6"
    db, cursor = opendb()
    try:
        # sql = "SELECT * FROM " + GenOffer #G1_1"
        # cursor.execute(sql)
        # description = cursor.description
        sql = "DELETE FROM " + GenOffer  # G1_1"
        cursor.execute(sql)
        # field = []
        # for i in range(1, len(description)):
        #     field.append(description[i][0])
        # field = str(tuple(field)).replace("'", "")
        for k in data["Information"]["sheet1"]:
            # id = tuple([k["id"]])
            # query = id
            # for d in k["value"]:
            #     x, y = tuple([d[0]]), tuple([d[1]])
            #     query += x+y

            # print(query)
            # query = str(query)
            # sql = "INSERT INTO {0}{1} VALUES{2}".format(GenOffer, field, query)
            # print(sql)
            value = [k["value"][i][j] for i in range(6) for j in range(2)]
            value.insert(0, k["id"])
            sql1 = "INSERT INTO {0}{1} VALUES{2}".format(GenOffer, field, tuple(value))

            cursor.execute(sql1)
        # del data["Information"]["Info1"]
        db.commit()
        return True
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()


def getGenIDs():
    db, cursor = opendb()
    sql = "SELECT id FROM " + GenOffer  # G1_1"
    # print(sql)
    cursor.execute(sql)
    results = cursor.fetchall()
    # print(results)
    IDs = [row[0] for row in results]
    return IDs


def getSellIDs():
    db, cursor = opendb()
    sql = "SELECT id FROM " + graphTable + " where page = 'config' "
    # print(sql)
    cursor.execute(sql)
    results = cursor.fetchall()
    userS = [row[0] for row in results]
    return userS


def getDoneIDs(start, end):
    print("running in " + sys._getframe().f_code.co_name)
    db, cursor = opendb()
    try:
        res = {}
        sql = "SELECT id FROM " + GenOffer + " WHERE time>='" + start + "'"
        print(sql)
        cursor.execute(sql)
        results = cursor.fetchall()
        Gen = []
        for submit in results:
            Gen.append(submit[0])
        sql = "SELECT id FROM " + GenOffer + " WHERE time<'" + start + "'"
        print(sql)
        cursor.execute(sql)
        results = cursor.fetchall()
        GenDoing = []
        for submit in results:
            GenDoing.append(submit[0])

        sql = "SELECT id FROM " + graphTable + " WHERE page='config' AND time>='" + start + "'"
        print(sql)
        cursor.execute(sql)
        results = cursor.fetchall()
        Sale = []
        for submit in results:
            Sale.append(submit[0])
        sql = "SELECT id FROM " + graphTable + " WHERE page='config' AND time<'" + start + "'"
        print(sql)
        cursor.execute(sql)
        results = cursor.fetchall()
        SaleDoing = []
        for submit in results:
            SaleDoing.append(submit[0])

        records = [Gen, GenDoing, Sale, SaleDoing]

        return records
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()


import hashlib


# def getAllFields(table, field, k1="",v1=""):
#     order6 = field6.split(",")
#     sql = "SELECT " + field + " FROM "+table
#     if id1:
#         sql+=" where "+k1+"='"+v1+"'"

class APInew(APIView):
    def post(self, request):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        print(request.body)
        data = json.loads(request.body)  # , strict=False
        # print(data)
        # print("APInew")
        # print(self.__class__.__name__)
        if data["page"] == "front":
            page = PostInfo0()
            res = page.post(request)
            return None
        elif data["page"] == "config":
            page = PostInfo1()
            res = page.post(request)
            return res
        elif data["page"] == "history":
            page = PostInfo2()
            res = page.post(request)
        elif data["page"] == "hist":
            page = PostInfo_hist()
            res = page.post(request)
            return res
        elif data["page"] == "match":
            page = PostInfo4()
            res = page.post(request)
            return res
        elif data["page"] == "gen_analysis":
            page = PostInfo5()
            res = page.post(request)
            return res
        elif data["page"] in ["sale_analysis"]:  # , "sale_analysis_sub"]:
            page = PostInfo6()
            res = page.post(request)
            return res
        elif data["page"] == "settle_analysis":
            page = PostInfo7()
            res = page.post(request)
            return res
        elif data["page"] == "price_analysis":
            page = PostInfo7()
            res = page.post(request)
            return res
        elif data["page"] == "stat":
            page = PostInfo9()
            res = page.post(request)
            return res
        elif data["page"] == "nav":
            page = PostInfoNavi()
            res = page.post(request)
            return res
        elif data["page"] == "login":
            page = PostInfoNavi()
            res = page.login(request)
            return res
        elif data["page"] == "usrlst":
            page = PostInfoUser()
            res = page.user(request)
            return res
        elif data["page"] == "auth":
            page = PostInfoAuth()
            res = page.auth(request)
            return res
        elif data["page"] == "group":
            page = PostInfoGroup()
            res = page.role(request)
            return res
        elif data["page"] == "permission":
            page = PostInfoPermission()
            res = page.permission(request)
            return res
        elif data["page"] == "news":
            page = PostInfoNews()
            res = page.post(request)
            return res
        elif data["page"] == "setting":
            page = PostInfoCapacity()
            res = page.post(request)
            return res
        else:
            return None


# navigation
class PostInfoNavi():
    def md5(self, str):
        m = hashlib.md5()
        str = "MOMOLOVE" + str
        m.update(str.encode("utf8"))
        # print(m.hexdigest())
        return m.hexdigest()

    # 登录
    def login(self, request):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb("power")
        data = json.loads(request.body)
        state = data.get('state')
        try:
            id = data["id"].upper()
            pwd = self.md5(data["pwd"])
            # sql = "SELECT count( * ) FROM `manager` WHERE user='"+id+"' AND pwd='"+pwd+"'"
            sql = "SELECT title FROM auth_group g, manager m,auth_group_access a WHERE user='" + id + "' AND pwd='" + pwd + "' AND m.id=a.uid AND a.group_id=g.id"
            # print(sql)
            # SELECT title FROM auth_group g, manager m,auth_group_access a WHERE user='N1-G-000002' AND pwd='988819bf84004d74443a712baec0e7da' AND m.id=a.uid AND a.group_id=g.id
            cursor.execute(sql)
            result = cursor.fetchone()
            if result:
                # return Response({"state": 1})
                return Response({"state": 1, "group": result[0]})
            else:
                return Response({"state": 0})
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    # 获取导航数据
    def post(self, request):
        # return None
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        # return Response({"state": -200})

        if request.method == 'POST':
            data = json.loads(request.body)
            db, cursor = opendb("power")

            if data["state"] > 0:
                try:
                    id = data["user"]["id"]
                    # print(id)
                    sql = "SELECT g.rules FROM auth_group g, auth_group_access a, manager m WHERE m.user='" + id + "' AND a.uid=m.id AND g.id=a.group_id"
                    # print(sql)
                    cursor.execute(sql)
                    result = cursor.fetchone()
                    right = result
                    # sql = "SELECT name,title FROM auth_rule WHERE FIND_IN_SET(id, '{0}') AND nav=0".format(right)
                    # cursor.execute(sql)
                    # result = cursor.fetchall()
                    result_list = []
                    if result:
                        for num in right[0].split(','):
                            if num:
                                sql2 = "SELECT name,title FROM auth_rule WHERE id=%s AND nav=0"
                                cursor.execute(sql2,(num))
                                result1 = cursor.fetchall()
                                if result1:
                                    result_list.append(result1[0])
                        result_tuple = tuple(result_list)
                        res = [group for group in result_tuple]
                        return Response({"nav": res})
                except Exception:
                    db.rollback()
                    return None
                finally:
                    db.close()


# 权限
class PostInfoAuth():
    # 修改权限数据
    def modify(self, data):
        user = data.get('user').get('id')
        auth = data.get('auth')
        id = auth.get('id')
        title = auth.get('title')
        name = auth.get('name')
        pid = auth.get('pid')
        if user == 'master':
            update_obj = models.AuthRule.objects.filter((Q(title=title) | Q(name=name))).first()
            if update_obj == None:
                result = models.AuthRule.objects.filter(id=id).update(
                    title=title, name=name, pid=pid, type=1, status=1, nav=1
                )
                if result == 1:
                    return True
                else:
                    return False
            return False

    # 添加权限数据
    def addtion(self, data):

        user = data.get('user').get('id')
        title = data.get('auth').get('title')
        name = data.get('auth').get('name')
        pid = data.get('auth').get('pid')
        result = models.AuthRule.objects.filter((Q(title=title) | Q(name=name))).first()
        if user == 'master':
            if result == None:
                add_rule_obj = models.AuthRule.objects.create(
                    title=title,
                    name=name,
                    type=1,
                    status=1,
                    pid=pid,
                    nav=1
                )
                res = add_rule_obj.id
                return res
            return False

    # 删除权限数据
    def delete(self, data):
        user = data.get('user').get('id')
        if user == 'master':
            del_auth_id = data.get('auth').get('id')
            del_auth_obj = models.AuthRule.objects.filter(id=del_auth_id).first()
            if del_auth_obj:
                del_auth_obj.delete()
                auth_groups = models.AuthGroup.objects.all()
                for group in auth_groups:
                    rules_list = group.rules.split(',')
                    bool = str(del_auth_id) in rules_list
                    if bool == True:
                        rules = group.rules
                        new_rules = rules.replace(str(del_auth_id) + ',', '')
                        group.rules = new_rules
                        group.save()
                return True

    # 获取权限数据
    def auth_list(self, data):
        db, cursor = opendb("power")
        try:
            if data["state"] > 0:
                id = data["user"]["id"]
                if id == 'master':
                    auth_all = models.AuthRule.objects.all()
                    res = [[auth.id, auth.name, auth.title] for auth in auth_all]
                    return res
            else:
                return None
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def auth(self, request):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        if request.method == 'POST':
            data = json.loads(request.body)
            if data["state"] > 0:
                res = self.auth_list(data)
                return Response({"auth": res})
            elif data['state'] == -1:
                res = self.addtion(data)
                if res:
                    return Response({"state": -200, 'id': res})
                else:
                    return Response({"state": -400})
            elif data["state"] == -2:
                res = self.modify(data)
                print(res)

                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            elif data["state"] == -3:
                res = self.delete(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})

            elif data["state"] == -3:
                res = self.delete(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})



# 用户
class PostInfoUser():
    def md5(self, str):
        m = hashlib.md5()
        str = "MOMOLOVE" + str
        m.update(str.encode("utf8"))
        # print(m.hexdigest())
        return m.hexdigest()

    # 添加用户数据
    def addtion_user(self, data):
        db, cursor = opendb('power')
        try:
            userinfo_list = data.get('userinfo')
            user = data.get('user')
            for userinfo in userinfo_list:
                if user == 'master':
                    username = userinfo.get('user')
                    pwd_user = userinfo.get('pwd')
                    role_id = userinfo.get('role')
                    pwd = self.md5(pwd_user)
                    display = userinfo.get('display')
                    user_obj = models.Manager.objects.filter(user=username).first()
                    if user_obj == None:
                        obj = models.Manager.objects.create(
                            user=username,
                            pwd=pwd,
                            display=display,
                            registertime=int(time.time())
                        )
                        sql = "SELECT id FROM auth_group where id=%s"
                        cursor.execute(sql,(role_id))
                        res_id = cursor.fetchone()

                        if res_id:
                            sql1 = "INSERT INTO auth_group_access VALUES(%s,%s)"
                            cursor.execute(sql1,(obj.id,res_id))
                            db.commit()
                            return obj.id
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    # 修改用户数据
    def update_user(self, data):
        db,cursor = opendb('power')
        try:
            user = data.get('user')
            userinfo = data.get('userinfo')
            pwd_user = userinfo.get('pwd')
            pwd = self.md5(pwd_user)
            if user == 'master' and len(userinfo)>1:
                id = userinfo.get('id')
                username = userinfo.get('user')
                display = userinfo.get('display')
                role_id = userinfo.get('role')

                user_obj = models.Manager.objects.filter(user=username).first()
                if user_obj:
                    sql = "SELECT group_id FROM auth_group_access where uid=%s"
                    cursor.execute(sql,(user_obj.id))
                    res_sql = cursor.fetchone()
                    group_obj = models.AuthGroup.objects.filter(id=res_sql[0]).first()
                    if pwd == user_obj.pwd and display == user_obj.display and group_obj.id==role_id:
                        return False
                    elif pwd != user_obj.pwd:
                        user_obj.pwd = pwd
                        user_obj.save()
                        return True
                    elif display != user_obj.display:
                        user_obj.display = display
                        user_obj.save()
                        return True
                    elif role_id != group_obj.id:
                        sql1 = "SELECT id FROM auth_group where id=%s"
                        cursor.execute(sql1, (role_id))
                        res_id = cursor.fetchone()
                        if res_id:
                            sql2 = "update auth_group_access set group_id=%s where uid=%s"
                            cursor.execute(sql2, (res_id[0],id))
                            db.commit()
                            return True
                else:
                    manager_id = models.Manager.objects.filter(id=id).update(
                        user=username,
                        pwd=pwd,
                        display=display,
                        registertime=int(time.time())
                    )
                    if manager_id == 1:
                        return True
                    else:
                        return False
            else:
                update_user = models.Manager.objects.filter(user=user).first()
                if update_user:
                    # 6-12
                    len_pwd = len(pwd_user)
                    if 6 <= len_pwd <= 12:
                        update_user.pwd = pwd
                        update_user.save()
                        return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()
    # 删除用户数据
    def delete_user(self, data):
        user = data.get('user')
        db, cursor = opendb('power')
        try:
            if user == 'master':
                user_id = data.get('userinfo').get('id')
                del_user_obj = models.Manager.objects.filter(id=user_id).first()
                if del_user_obj:
                    del_user_obj.delete()
                    sql = "delete from auth_group_access where uid=%s"
                    cursor.execute(sql, (user_id))
                    db.commit()
                    return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    # 用户列表
    def user_list(self, data):
        db, cursor = opendb("power")
        try:
            sql = "SELECT m.id,m.user, auth_group.title,m.lasttime FROM manager m LEFT JOIN auth_group_access ON FIND_IN_SET(m.id, auth_group_access.uid) LEFT JOIN auth_group ON auth_group_access.group_id=auth_group.id"
            cursor.execute(sql)
            results = cursor.fetchall()
            info = []
            for row in results:
                res = {}
                id = row[0]
                name = row[1]
                group = row[2]
                last = row[3]
                res['id'] = id
                res['name'] = name
                res['group'] = group
                lacal = time.localtime(last)
                res['last'] = time.strftime("%Y-%m-%d %H:%M:%S", lacal)
                info.append(res)
            # print(info)
            return info
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    # 用户数据入口
    def user(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            state = data.get('state')
            if state == 1:
                res = self.user_list(data)
                return Response({'usrlst': res})
            elif state == -1:
                res = self.addtion_user(data)
                if res:
                    return Response({'state': -200,"id":res})
                else:
                    return Response({'state': -400})
            elif state == -2:
                res = self.update_user(data)
                if res == True:
                    return Response({'state': -200})
                else:
                    return Response({'state': -400})
            elif state == -3:
                res = self.delete_user(data)
                if res == True:
                    return Response({'state': -200})
                else:
                    return Response({'state': -400})


# 角色
class PostInfoGroup():
    # 获取角色信息数据
    def roles_list(self, data):
        id = data.get('id')
        if id == 'master':
            roles = models.AuthGroup.objects.all()
            res = {'role': []}
            for role in roles:
                info = {}
                info['id'] = role.id
                info['title'] = role.title
                res['role'].append(info)
            return res

    # 添加角色
    def addtion_role(self, data):
        id = data.get('id')
        if id == 'master':
            role_list = data.get('role')
            rule_obj_list = []
            for role in role_list:
                title = role.get('title')
                status = role.get('status')
                rules = role.get('rules')
                role_obj = models.AuthGroup.objects.filter(title=title).first()
                if role_obj == None:
                    rules_list = set([i for i in rules.split(',')])
                    rules = []
                    for i in rules_list:
                        rule_obj = models.AuthRule.objects.filter(id=int(i)).first()
                        if rule_obj:
                            rules.append(i)
                    new_rule = ','.join(rules)
                    obj = models.AuthGroup(title=title, status=status, rules=new_rule + ',')
                    rule_obj_list.append(obj)
            res = models.AuthGroup.objects.bulk_create(rule_obj_list)
            if res:
                return True

    # 更新角色
    def update_role(self, data):
        id = data.get('id')
        if id == 'master':
            role = data.get('role')
            role_id = role.get('id')
            status = role.get('status')
            rules = role.get('rules')
            print(data)
            update_role_obj = models.AuthGroup.objects.filter(id=role_id).first()
            if update_role_obj:
                rule = []
                for rule_id in rules.split(','):
                    rule_obj = models.AuthRule.objects.filter(id=int(rule_id)).first()
                    if rule_obj:
                        if rule_id not in rule:
                            rule.append(rule_id)
                if rule:
                    new_rules = ','.join(rule) + ','
                    role_title = update_role_obj.title
                    if role_title:
                        update_role_obj.status = status
                        update_role_obj.rules = new_rules
                        update_role_obj.save()
                        return True
                    #     rule = models.AuthGroup.objects.filter(status=status, rules=rules + ',').first()
                    #     if rule:
                    #         return False
                    #     else:
                    #         update_role_obj.status = status
                    #         update_role_obj.rules = new_rules
                    #         update_role_obj.save()
                    #         return True
                    # else:
                    #     update_role_obj.title = role_title
                    #     update_role_obj.status = status
                    #     update_role_obj.rules = new_rules
                    #     update_role_obj.save()
                    #     return True
            else:
                return False

    # 删除角色
    def delete_role(self, data):
        id = data.get('id')
        db, cursor = opendb('power')
        try:
            if id == 'master':
                role_id = data.get('role')
                del_obj = models.AuthGroup.objects.filter(id=int(role_id)).first()
                if del_obj:
                    del_obj.delete()
                    sql = "delete from auth_group_access where group_id=%s"
                    cursor.execute(sql, (role_id))
                    db.commit()
                    return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def role(self, request):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        data = json.loads(request.body)
        state = data.get('state')
        if state == 1:
            res = self.roles_list(data)
            return Response(res)
        elif state == -1:
            res = self.addtion_role(data)
            if res == True:
                return Response({'state': -200})
            else:
                return Response({'state': -400})
        elif state == -2:
            res = self.update_role(data)
            if res == True:
                return Response({'state': -200})
            else:
                return Response({'state': -400})
        elif state == -3:
            res = self.delete_role(data)
            if res == True:
                return Response({'state': -200})
            else:
                return Response({'state': -400})


# 角色权限
class PostInfoPermission():
    def sql(self, id):
        db, cursor = opendb('power')
        try:
            # id为用户id,查询用户对应的角色
            sql1 = "select user from manager where id=%s"
            cursor.execute(sql1, (id))
            user = cursor.fetchone()
            if user:
                sql2 = "SELECT title FROM auth_group g, manager m,auth_group_access a WHERE user=%s AND m.id=a.uid AND a.group_id=g.id"
                cursor.execute(sql2, (id))
                res = cursor.fetchone()[0] # 测试

                return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    # 获取角色所有权限
    def permission_list(self, data):

        id = data.get('id')
        # res = self.sql(id)
        if id:
            # group_obj = models.AuthGroup.objects.filter(title=res).first()
            group_obj = models.AuthGroup.objects.filter(id=id).first()
            res = {}
            auth_list = []
            if group_obj:
                for rule in group_obj.rules.split(','):
                    if rule:
                        auth = models.AuthRule.objects.filter(id=rule).first()
                        if auth:
                            auth_info = {}
                            auth_info['id'] = auth.id
                            auth_info['title'] = auth.title
                            auth_info['name'] = auth.name
                            auth_list.append(auth_info)
                res['rule'] = group_obj.rules
                res['auth'] = auth_list
                return res
    # 增加角色权限数据
    def addtion_permission(self, data):
        id = data.get('id')
        user = data.get('user')
        rules_list = data.get('rules')
        # res = self.sql(id) # 返回角色title
        if user == 'master':
            # group_obj = models.AuthGroup.objects.filter(title=res).first()
            group_obj = models.AuthGroup.objects.filter(id=id).first()
            if group_obj:
                rule = []
                for rule_id in rules_list:
                    rule_obj = models.AuthRule.objects.filter(id=rule_id).first()
                    if rule_obj:
                        bool = str(rule_id) in group_obj.rules.split(',')
                        # print(rule_id, bool)
                        # 如果该用户存在该权限则不添加,rules不能重复
                        if bool == False:
                            # print(rule_id)
                            rules = str(rule_id) + ','
                            if rules not in rule:
                                rule.append(rules)
                if rule:
                    new_rule = group_obj.rules + ''.join(rule)
                    group_obj.rules = new_rule
                    group_obj.save()
                    return True

    # 修改角色权限数据
    def update_permission(self, data):
        id = data.get('id')
        user = data.get('user')
        rules_list = data.get('rules')
        # res = self.sql(id)
        if user == 'master':
            # group_obj = models.AuthGroup.objects.filter(title=res).first()
            group_obj = models.AuthGroup.objects.filter(id=id).first()
            if group_obj:
                new_rule = []
                for i in rules_list:
                    rule_obj = models.AuthRule.objects.filter(id=i).first()
                    if rule_obj:
                        if str(i) not in new_rule:
                            new_rule.append(str(i))
                if new_rule:
                    rules = ','.join(new_rule) + ','
                    group_obj.rules = rules
                    group_obj.save()
                    return True
                return False

    # 删除角色权限数据
    def delete_permission(self, data):
        id = data.get('id')
        user = data.get('user')
        rules_list = data.get('rules')
        # res = self.sql(id)
        if user == 'master':
            # group_obj = models.AuthGroup.objects.filter(title=res).first()
            group_obj = models.AuthGroup.objects.filter(id=id).first()
            group_rule_list = group_obj.rules.split(',')
            if group_obj:
                for rule_id in rules_list:
                    bool = str(rule_id) in group_rule_list
                    if bool == True:
                        new_rules = group_obj.rules.replace(str(rule_id) + ',', '')
                        group_obj.rules = new_rules
                        group_obj.save()
                return True

    # 权限数据请求入口
    def permission(self, request):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        if request.method == 'POST':
            data = json.loads(request.body)
            if data["state"] == 1:  # 获取所有权限数据,state更改
                res = self.permission_list(data)
                return Response(res)
            elif data["state"] == -1:  # 增加权限数据,state更改
                res = self.addtion_permission(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            elif data["state"] == -2:  # 修改权限,state更改
                res = self.update_permission(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            elif data["state"] == -3:  # 删除权限,state更改
                res = self.delete_permission(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})


# 0.front page
class PostInfo0(APIView):

    def query(self):
        db, cursor = opendb()
        try:
            res = {}
            res["Information"] = []
            sql = "SELECT * FROM G0"
            cursor.execute(sql)
            results = cursor.fetchall()
            for row in results:
                temp = {
                    "id": row[1],
                    "x": list(row[2:7]),
                    "y": list(row[7:12])
                }
                res["Information"].append(temp)
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def modify(self, data):
        db, cursor = opendb()
        try:
            sql = "DELETE FROM G0"
            cursor.execute(sql)
            for d in data["Information"]:
                id = tuple([d["id"]])
                x = tuple(d["x"][0:5])
                y = tuple(d["y"][0:5])
                field = "(id, x1, x2, x3, x4, x5, y1, y2, y3, y4, y5)"
                query = id + x + y
                sql = "INSERT INTO G0{0} VALUES{1}".format(field, str(query))
                cursor.execute(sql)
            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def post(self, request):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        return Response({"state": -200})
        if request.method == 'POST':
            data = json.loads(request.body)
            if data["state"] > 0:
                res = {}
                # res = self.query()
                res["state"] = data["state"] + 1
                return Response(res)
            elif data["state"] == -1:
                res = self.modify(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            else:
                return None


# 1.config
class PostInfo1(APIView):

    def getRole(self, id):
        print("running in " + sys._getframe().f_code.co_name)
        db, cursor = opendb()
        try:
            print(id)
            # sql = "SELECT title FROM auth_group g, manager m,auth_group_access a WHERE user='"+id+"' AND m.id=a.uid AND a.group_id=g.id"
            # cursor.execute(sql)
            # result=cursor.fetchone()
            # print(sql)
            roles = []
            # if result[0] == "超级管理员":
            #     roles.append('M')
            # elif result[0] == "发电企业":
            #     roles.append('G')
            # elif result[0] == "售电企业":
            #     roles.append('S')
            # elif result[0] == "ISO":
            #     roles.append('I')

            # for one in id:
            # print("id")
            if id in ["master"]:
                roles.append("M")
            elif id in ["iso"]:
                roles.append("I")
            else:
                # print(one[3])
                roles.append(id[3])
            return roles
        except Exception:
            db.rollback()
            return ['M']
        finally:
            db.close()

    def query_new(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb()
        try:
            id = data["user"]["id"]
            role = self.getRole(id)
            res = {}
            res["Information"] = {}
            res["Information"]["sheet1"] = {}
            res["Information"]["sheet2"] = {}
            res["Information"]["sheet3"] = {}
            print(role[0])
            if role[0] in ["M", "I"]:
                sql11 = "select id from Graph where page='config'"
                cursor.execute(sql11)
                re = cursor.fetchall()
                Info = getGraph(data["page"], id)
                # SHEET2
                for num in range(len(Info["sheet2"]) - 3):
                    value = Info["sheet2"][num]["value"]
                    Info["sheet2"][num]["value"] = [[i,v] for i, v in enumerate(value)]
                res["Information"]["sheet2"] = Info["sheet2"]
                res["Information"]["sheet2"]["title"] = "售电商"

                # SHEET3
                for num in range(len(Info["sheet3"]) - 3):
                    value = Info["sheet3"][num]["value"]
                    Info["sheet3"][num]["value"] = [[i,v] for i, v in enumerate(value)]
                res["Information"]["sheet3"] = Info["sheet3"]
                res["Information"]["sheet3"]["title"] = "新能源预测"

                # SHEET1
                results = getGenOffer(cursor, id)
                Info1, Info2 = {}, {}
                num1, num2 = 0, 0
                for row in results:
                    Info = {
                        "id": row[1],
                        "value": [[row[2], row[3]], [row[4], row[5]], [row[6], row[7]], \
                                  [row[8], row[9]], [row[10], row[11]], [row[12], row[13]]]
                    }
                    res["Information"]["sheet1"][str(num1)] = Info
                    num1 += 1
                res["Information"]["sheet1"]["caption"] = "电价出力曲线"
                res["Information"]["sheet1"]["axis_x"] = "出力"
                res["Information"]["sheet1"]["axis_y"] = "报价"
                res["Information"]["sheet1"]["title"] = "发电商"
            elif role[0] in ["S"]:
                Info = getGraph(data["page"], id)
                if Info:
                    for num in range(len(Info["sheet2"]) - 3):
                        value = Info["sheet2"][num]["value"]
                        Info["sheet2"][num]["value"] = [[i, v] for i, v in enumerate(value)]
                    res["Information"]["sheet2"] = Info["sheet2"]
                else:
                    return {}
            elif role[0] in ["G"]:
                if id[5] == '1':
                    # SHEET3
                    Info = getGraph(data["page"], id)
                    for num in range(len(Info["sheet3"]) - 3):
                        value = Info["sheet3"][num]["value"]
                        Info["sheet3"][num]["value"] = [[i, v] for i, v in enumerate(value)]
                    res["Information"]["sheet3"] = Info["sheet3"]
                    res["Information"]["sheet3"]["title"] = "新能源预测"
                else:
                    # sheet1
                    results = getGenOffer(cursor, id)
                    num1, num2 = 0, 0
                    for row in results:
                        Info = {
                            "id": row[1],
                            "value": [[row[2], row[3]], [row[4], row[5]], [row[6], row[7]], \
                                      [row[8], row[9]], [row[10], row[11]], [row[12], row[13]]]
                        }
                        res["Information"]["sheet1"][str(num1)] = Info
                        num1 += 1
                    res["Information"]["sheet1"]["caption"] = "电价出力曲线"
                    res["Information"]["sheet1"]["axis_x"] = "出力"
                    res["Information"]["sheet1"]["axis_y"] = "报价"

            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def setgraph(self,data):
        db,cursor = opendb()
        try:
            field = "(id,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17,v18,v19,v20,v21,v22,v23,v24,page,sheet,caption,axis_x,axis_y,ctitle)"
            page = data["page"]

            for key,value in data['Information'].items():
                sql = "DELETE FROM " + graphTable + " where page='" + page + "' and sheet='" + key +"'"
                cursor.execute(sql)

                if key == 'sheet2' or key == 'sheet3':
                    caption = value.get('caption')
                    axis_x = value.get('axis_x')
                    axis_y = value.get('axis_y')
                    data_dict = value.get('data')
                    if data_dict:
                        for key1,value1 in data_dict.items():
                            query = tuple([key1]) + tuple(value1) + tuple([page]) + tuple([key]) + tuple([caption]) + tuple(
                                [axis_x]) + tuple([axis_y]) + tuple([key1])
                            sql0 = "INSERT INTO {0}{1} VALUES{2}".format(graphTable, field, query)
                            cursor.execute(sql0)
            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def reset(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        ret1 = resetGenOffer(data)
        ret2 = self.setgraph(data)
        if ret1 == True or ret2 == True:
            return True


        # db, cursor = opendb()
        # try:
        #     sql = "SELECT * FROM " + GenOffer #G1_1"
        #     cursor.execute(sql)
        #     description = cursor.description
        #     sql = "DELETE FROM " + GenOffer #G1_1"
        #     cursor.execute(sql)

        #     field = []
        #     for i in range(1, len(description)):
        #         field.append(description[i][0])
        #     field = str(tuple(field)).replace("'", "")
        #     for k in data["Information"]["Info1"]:
        #         # role = tuple(["G"])
        #         id = tuple([k["id"]])
        #         query = id
        #         for d in k["value"]:
        #             x, y = tuple([d[0]]), tuple([d[1]])
        #             query += x+y

        #         print(query)
        #         query = str(query)
        #         sql = "INSERT INTO {0}{1} VALUES{2}".format(GenOffer, field, query)
        #         print(sql)
        #         cursor.execute(sql)
        #         # db.commit()
        #     del data["Information"]["Info1"]
        #     print("data is :")
        #     print(data)
        #     db.commit()

        #     setGraph(data)

        #     return True
        # except Exception:
        #     db.rollback()
        #     return None
        # finally:
        #     db.close()

    def update(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        ret1 = None
        ret2 = None
        for key in data["Information"]:
            if key == 'sheet1':
                ret1 = updateGenOffer(data)
            if key == 'sheet2' or key == 'sheet3':
                ret2 = updateGraph(data)
                print(ret2)
        print(ret1,ret2)
        if ret1 == True or ret2 == True:
            return True

    def modify(self,data):
        db, cursor = opendb()
        try:
            ret1 = resetGenOffer(data)
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def post(self, request):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        if request.method == 'POST':
            data = json.loads(request.body)
            accesslog(data)
            if data["state"] == 2:
                res = self.query_new(data)
                if res:
                    res["state"] = data["state"]
                    return Response(res)
                else:
                    return Response({})
            elif data["state"] == -3:
                res = self.update(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            elif data["state"] == -2:
                res = self.reset(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            else:
                return None


# 2.hist
class PostInfo_hist(APIView):
    field = "v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17,v18,v19,v20,v21,v22,v23,v24,sheet,caption,axis_x,axis_y,ctitle,time"
    field1 = "Dq,Dp,Cq,Cp,eps,Ccost,CPerfit,Fq,Fc,Fp,spend,pcost,load_profits,load_pure_profits,date_format(time, '%Y-%m-%d')"
    field2 = "pmin,sp1,k1,sp2,k2,sp3,k3,sp4,k4,pmax,k5,Nq,Np,Cq,Cp,delta,Nperfit,Npperfit,Fq,Fp,Fperfit,perfit,pperfit,gen_cost,gen_pure_profits,date_format(time, '%Y-%m-%d')"

    def query(self, id):
        db, cursor = opendb()
        try:
            res = {}
            res["sheet1"] = {}
            res["sheet2"] = {}
            res["table"] = {}

            res["sheet1"]["caption"] = "售电侧出力曲线图"
            res["sheet1"]["axis_x"] = "时间"
            res["sheet1"]["axis_y"] = "负荷（MW）"
            res["sheet1"]["days"] = []
            data = []
            temp = {}
            temp["title"] = "节点1负荷"
            temp["value"] = [600, 650, 700, 750, 800, 850, 900, 950, 1000, 1050, 1100, 1150, 1200, 1100, 1000, 900, 800,
                             900, 1000, 1100, 1200, 1000, 800, 600]
            data.append(temp)
            temp = {}
            temp["title"] = "节点1电价"
            temp["value"] = [600, 650, 700, 750, 800, 850, 900, 950, 1000, 1050, 1100, 1150, 1200, 1100, 1000, 900, 800,
                             900, 1000, 1100, 1200, 1000, 800, 600]
            data.append(temp)
            res["sheet1"]["days"].append({"date": "20200214", "data": data})
            res["sheet1"]["days"].append({"date": "20200215", "data": data})

            res["sheet2"]["caption"] = "售电侧电价曲线图"
            res["sheet2"]["axis_x"] = "时间"
            res["sheet2"]["axis_y"] = "电价（元）"
            res["sheet2"]["days"] = []
            res["sheet2"]["days"].append({"date": "20200214", "data": data})
            res["sheet2"]["days"].append({"date": "20200215", "data": data})

            res["table"]["caption"] = "售电侧出力曲线图"
            res["table"]["col"] = ["撮合日期", "申报电量", "撮合日期", "申报电量", "撮合日期", "申报电量", "撮合日期", "申报电量", "撮合日期", "申报电量",
                                   "撮合日期", "申报电量", "撮合日期", "申报电量"]

            res["table"]["data"] = []
            temp = {}
            temp["date"] = "20200214"
            temp["value"] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
            res["table"]["data"].append(temp)
            temp = {}
            temp["date"] = "20200215"
            temp["value"] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
            res["table"]["data"].append(temp)

            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    # 数据循环方法
    def for_loop(self,row,date,days):
        for days_dic in days:
            days_dic_date = days_dic.get("date")
            if date == days_dic_date:
                days_dic_data = days_dic.get("data")
                days_dic_data.append({"title": row[-2], "value": row[:24]})
                break
        else:
            new_days_dic = {"date": date, "data": [{"title": row[-2], "value": row[:24]}]}
            days.append(new_days_dic)

    #  数据循环方法1 针对 table
    def for_loop1(self,db,cursor,sql1,sql2,table1,table2):
        cursor.execute(sql1)
        results3 = cursor.fetchall()
        for row3 in results3:
            table1["caption"] = "售电企业申报数据及出清结果"
            table1["col"] = ["申报电量(MWh)", "日前结算(元)", "实时电量(MWh)", "实时结算(元)", "预测精度",
                             "现货购电费用(元)", "现货单位成本(元/MWh)", "中长期电量(MWh)",
                             "中长期价格(元/MWh)",
                             "中长期收益(元)", "购电费用(元)", "单位成本(元/MWh)", "售电收入(元)",
                             "售电企业纯利润(元)"]
            date = row3[-1].replace('-', '')
            data_list1 = table1.setdefault("data", [])
            for li in data_list1:
                if li['date'] == date and li['id'] == row3[0]:
                    li['value'] = list(row3[1:15])
                    break
            else:
                info = {"id": row3[0], "date": date, "value": list(row3[1:15])}
                data_list1.append(info)

        cursor.execute(sql2)
        results4 = cursor.fetchall()
        for row4 in results4:
            table2["caption"] = "发电企业申报数据及出清结果"
            table2['col'] = ["最低技术出力(MW)", "功率分段点1(MW)", "第一段报价(元/MWh)",
                             "功率分段点2(MW)", "第二段报价(元/MWh)",
                             "功率分段点3(MW)", "第三段报价(元/MWh)", "功率分段点4(MW)",
                             "第四段报价(元/MWh)", "最高技术出力(MW)",
                             "第五段报价（元/MWh）", "日前发电量（MWh）", "日前结算（元）", "实时发电量（MWh）",
                             "实时结算（元）", "发电偏差",
                             "现货收益（元）", "现货单位收益（元/MWh）", "中长期电量（MWh）",
                             "中长期价格（元/MWh）", "中长期收益（元）",
                             "发电收益（元）", "单位收益（元/MWh）", "发电成本（元）", "发电企业利润"]
            data_list2 = table2.setdefault("data", [])
            date = row4[-1].replace('-', '')
            for li in data_list2:
                if li['date'] == date and li['id'] == row4[0]:
                    li['value'] = list(row4[1:26])
                    break
            else:
                info = {"id": row4[0], "date": date, "value": list(row4[1:26])}
                data_list2.append(info)

    # 售电/发电侧数据
    def query_new(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb()

        try:
            content = data.get('content')
            user = data.get('user')
            id = None
            if user:
                id = user.get('id')
            page = data.get('page')
            id_list = data.get('id')
            start = data.get('date')[0]
            t1 = start[:4]
            t2 = start[4:6]
            t3 = start[6:]
            start_date = t1 + '-' + t2 + '-' + t3

            end = data.get('date')[1]
            t1 = end[:4]
            t2 = end[4:6]
            t3 = end[6:]
            end_date = t1 + '-' + t2 + '-' + t3
            if content:
                if len(content) == 1:
                    if content[0] == 'sheet':
                        if id:
                            if id.upper() == 'MASTER' or id.upper() == 'ISO':
                                res2 = {}
                                res2['sheet1'] = {"caption": "售电侧负荷电价曲线图", "axis_x": "时间", "axis_y": "电价（元/MWh）", "days": []}
                                res2['sheet2'] = {"caption": "售电侧负荷电价曲线图", "axis_x": "时间", "axis_y": "负荷（MW）", "days": []}
                                res2['sheet3'] = {"caption": "发电侧出力电价曲线图", "axis_x": "时间", "axis_y": "电价（元/MWh）", "days": []}
                                res2['sheet4'] = {"caption": "发电侧出力电价曲线图", "axis_x": "时间", "axis_y": "出力（MW）", "days": []}
                                sql = "select " + self.field + " from Graph where page='" + page + "' and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                                cursor.execute(sql)
                                results2 = cursor.fetchall()
                                for row in results2:
                                    axisy = row[-3]
                                    caption2 = row[-5]
                                    if caption2 == '售电侧负荷电价曲线图':
                                        if axisy == '电价（元/MWh）':
                                            date = row[-1].strftime('%Y-%m-%d').replace('-', '')
                                            days = res2['sheet1']["days"]
                                            self.for_loop(row, date, days)

                                        elif axisy == '负荷（MW）':
                                            date = row[-1].strftime('%Y-%m-%d').replace('-', '')
                                            days = res2['sheet2']["days"]
                                            self.for_loop(row, date, days)

                                    elif caption2 == '发电侧出力电价曲线图':
                                        if axisy == '电价（元/MWh）':
                                            date = row[-1].strftime('%Y-%m-%d').replace('-', '')
                                            days = res2['sheet3']["days"]
                                            self.for_loop(row, date, days)

                                        elif axisy == '出力（MW）':
                                            date = row[-1].strftime('%Y-%m-%d').replace('-', '')
                                            days = res2['sheet4']["days"]
                                            self.for_loop(row, date, days)
                                return res2
                            if id[3].upper() == 'S' or id[4].upper() == 'S':
                                # sql = "select " + self.field + " from Graph where page='" + page + "' and id='" + id + "' and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                                sql = "select " + self.field + " from Graph where page='" + page + "' and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                                cursor.execute(sql)
                                results = cursor.fetchall()
                                res = {'sheet1': {"caption": "售电侧负荷电价曲线图", "axis_x": "时间", "axis_y": "电价（元/MWh）", "days": []},
                                       'sheet2': {"caption": "售电侧负荷电价曲线图", "axis_x": "时间", "axis_y": "负荷（MW）", "days": []}}
                                for row in results:
                                    date = row[-1].strftime('%Y-%m-%d').replace('-', '')

                                    caption, axis_x, axis_y = row[25:28]
                                    if caption == '售电侧负荷电价曲线图':
                                        if axis_y == '电价（元/MWh）':
                                            days = res['sheet1']["days"]
                                            self.for_loop(row, date, days)
                                        if axis_y == '负荷（MW）':
                                            days = res['sheet2']["days"]
                                            self.for_loop(row, date, days)
                                return res
                            if id[3].upper() == 'G' or id[4].upper() == 'G':
                                    # sql = "select " + self.field + " from Graph where page='" + page + "' and id='" + id + "' and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                                    sql = "select " + self.field + " from Graph where page='" + page + "' and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                                    cursor.execute(sql)
                                    results = cursor.fetchall()
                                    res = {'sheet1': {"caption": "发电侧出力电价曲线图", "axis_x": "时间", "axis_y": "电价（元/MWh）"},
                                           'sheet2': {"caption": "发电侧出力电价曲线图", "axis_x": "时间", "axis_y": "出力（MW）"}}
                                    for row in results:
                                        date = row[-1].strftime('%Y-%m-%d').replace('-', '')

                                        caption, axis_x, axis_y = row[25:28]
                                        if caption == '发电侧出力电价曲线图':
                                            if axis_y == '电价（元/MWh）':
                                                days = res['sheet1'].setdefault("days", [])
                                                self.for_loop(row, date, days)
                                            if axis_y == '出力（MW）':
                                                days = res['sheet2'].setdefault("days", [])
                                                self.for_loop(row, date, days)
                                    return res
                        else:
                            res2 = {}
                            res2['sheet1'] = {"caption": "售电侧负荷电价曲线图", "axis_x": "时间", "axis_y": "电价（元/MWh）",
                                              "days": []}
                            res2['sheet2'] = {"caption": "售电侧负荷电价曲线图", "axis_x": "时间", "axis_y": "负荷（MW）", "days": []}
                            res2['sheet3'] = {"caption": "发电侧出力电价曲线图", "axis_x": "时间", "axis_y": "电价（元/MWh）",
                                              "days": []}
                            res2['sheet4'] = {"caption": "发电侧出力电价曲线图", "axis_x": "时间", "axis_y": "出力（MW）", "days": []}
                            sql = "select " + self.field + " from Graph where page='" + page + "' and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                            cursor.execute(sql)
                            results2 = cursor.fetchall()
                            for row in results2:
                                axisy = row[-3]
                                caption2 = row[-5]
                                if caption2 == '售电侧负荷电价曲线图':
                                    if axisy == '电价（元/MWh）':
                                        date = row[-1].strftime('%Y-%m-%d').replace('-', '')
                                        days = res2['sheet1']["days"]
                                        self.for_loop(row, date, days)

                                    elif axisy == '负荷（MW）':
                                        date = row[-1].strftime('%Y-%m-%d').replace('-', '')
                                        days = res2['sheet2']["days"]
                                        self.for_loop(row, date, days)

                                elif caption2 == '发电侧出力电价曲线图':
                                    if axisy == '电价（元/MWh）':
                                        date = row[-1].strftime('%Y-%m-%d').replace('-', '')
                                        days = res2['sheet3']["days"]
                                        self.for_loop(row, date, days)

                                    elif axisy == '出力（MW）':
                                        date = row[-1].strftime('%Y-%m-%d').replace('-', '')
                                        days = res2['sheet4']["days"]
                                        self.for_loop(row, date, days)

                            return res2
                    if content[0] == 'table':
                        if id:
                            if id.upper() == 'MASTER' or id.upper() == 'ISO':
                                if id_list:
                                    table_res = {}
                                    table1 = {}
                                    table2 = {}
                                    for name_list in id_list:
                                        for name in name_list:
                                            table_sfield = 'id,' + self.field1
                                            sql1 = "SELECT " + table_sfield + " FROM G2_3_new WHERE id='" + name + "'and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"

                                            table_gfield = 'id,' + self.field2
                                            sql2 = "SELECT " + table_gfield + " FROM G2_4_new WHERE id='" + name + "'and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"

                                            table_info = self.for_loop1(db,cursor,sql1,sql2,table1,table2)
                                            table_res['table1'] = table1
                                            table_res['table2'] = table2
                                    return table_res
                                else:
                                    table_res = {}
                                    table1 = {}
                                    table2 = {}
                                    table_sfield = 'id,' + self.field1
                                    sql1 = "SELECT " + table_sfield + " FROM G2_3_new WHERE date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"


                                    table_gfield = 'id,' + self.field2
                                    sql2 = "SELECT " + table_gfield + " FROM G2_4_new WHERE date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"

                                    table_info = self.for_loop1(db, cursor, sql1, sql2, table1, table2)
                                    table_res['table1'] = table1
                                    table_res['table2'] = table2
                                    return table_res
                            if id[3].upper() == 'S' or id[4].upper() == 'S':

                                # g2-3 table
                                table = {}
                                table_sfield = 'id,' + self.field1
                                sql1 = "SELECT " + table_sfield + " FROM G2_3_new WHERE id='" + id + "' and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                                cursor.execute(sql1)
                                results1 = cursor.fetchall()
                                res1 = {}
                                for row2 in results1:
                                    res1["caption"] = "售电企业申报数据及出清结果"
                                    res1["col"] = ["申报电量(MWh)", "日前结算(元)", "实时电量(MWh)", "实时结算(元)", "预测精度", "现货购电费用(元)", "现货单位成本(元/MWh)", "中长期电量(MWh)", "中长期价格(元/MWh)",
                                                   "中长期收益(元)", "购电费用(元)", "单位成本(元/MWh)", "售电收入(元)", "售电企业纯利润(元)"]
                                    date = row2[-1].replace('-', '')
                                    data_list = res1.setdefault("data", [])
                                    for li in data_list:
                                        if li['date'] == date:
                                            li['value'] = list(row2[1:15])
                                            break
                                    else:
                                        info = {"id": row2[0], "date": date, "value": list(row2[1:15])}
                                        data_list.append(info)
                                table['table'] = res1
                                return table
                            if id[3].upper() == 'G' or id[4].upper() == 'G':
                                # g4_new
                                table2 = {}
                                table_gfield = 'id,' + self.field2
                                sql1 = "SELECT " + table_gfield + " FROM G2_4_new WHERE id='" + id + "' and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                                cursor.execute(sql1)
                                results1 = cursor.fetchall()
                                res1 = {}
                                for row1 in results1:
                                    res1["caption"] = "发电企业申报数据及出清结果"
                                    date = row1[-1].replace('-', '')
                                    res1['col'] = ["最低技术出力(MW)", "功率分段点1(MW)", "第一段报价(元/MWh)", "功率分段点2(MW)", "第二段报价(元/MWh)",
                                                   "功率分段点3(MW)", "第三段报价(元/MWh)", "功率分段点4(MW)", "第四段报价(元/MWh)", "最高技术出力(MW)",
                                                   "第五段报价（元/MWh）", "日前发电量（MWh）", "日前结算（元）", "实时发电量（MWh）", "实时结算（元）", "发电偏差",
                                                   "现货收益（元）",
                                                   "现货单位收益（元/MWh）", "中长期电量（MWh）", "中长期价格（元/MWh）", "中长期收益（元）", "发电收益（元）",
                                                   "单位收益（元/MWh）",
                                                   "发电成本（元）", "发电企业利润"]
                                    data_list = res1.setdefault("data", [])
                                    for li in data_list:
                                        if li['date'] == date:
                                            li['value'] = list(row1[1:26])
                                            # li['id'] = row1[0]
                                            break
                                    else:
                                        info = {"id": row1[0], "date": date, "value": list(row1[1:26])}
                                        data_list.append(info)
                                table2['table'] = res1
                                return table2

                elif len(content) == 2:
                    info = content[0]
                    info1 = content[1]
                    if (info == 'sheet' and info1 == 'table') or (info == 'table' and info1 == 'sheet'):
                        # master/iso n g
                        if id:
                            if id.upper() == 'MASTER' or id.upper() == 'ISO':
                                res2 = {}
                                res2['sheet1'] = {"caption": "售电侧负荷电价曲线图", "axis_x": "时间", "axis_y": "电价（元/MWh）",
                                                  "days": []}
                                res2['sheet2'] = {"caption": "售电侧负荷电价曲线图", "axis_x": "时间", "axis_y": "负荷（MW）",
                                                  "days": []}
                                res2['sheet3'] = {"caption": "发电侧出力电价曲线图", "axis_x": "时间", "axis_y": "电价（元/MWh）",
                                                  "days": []}
                                res2['sheet4'] = {"caption": "发电侧出力电价曲线图", "axis_x": "时间", "axis_y": "出力（MW）",
                                                  "days": []}
                                sql = "select " + self.field + " from Graph where page='" + page + "' and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                                print(sql,'sl')
                                cursor.execute(sql)
                                results2 = cursor.fetchall()
                                print(results2)
                                for row in results2:
                                    axisy = row[-3]
                                    caption2 = row[-5]
                                    if caption2 == '售电侧负荷电价曲线图':
                                        if axisy == '电价（元/MWh）':
                                            date = row[-1].strftime('%Y-%m-%d').replace('-', '')
                                            days = res2['sheet1']["days"]
                                            self.for_loop(row, date, days)

                                        elif axisy == '负荷（MW）':
                                            date = row[-1].strftime('%Y-%m-%d').replace('-', '')
                                            days = res2['sheet2']["days"]
                                            self.for_loop(row, date, days)

                                    elif caption2 == '发电侧出力电价曲线图':
                                        if axisy == '电价（元/MWh）':
                                            date = row[-1].strftime('%Y-%m-%d').replace('-', '')
                                            days = res2['sheet3']["days"]
                                            self.for_loop(row, date, days)

                                        elif axisy == '出力（MW）':
                                            date = row[-1].strftime('%Y-%m-%d').replace('-', '')
                                            days = res2['sheet4']["days"]
                                            self.for_loop(row, date, days)
                                if id_list:
                                    table1 = {}
                                    table2 = {}
                                    for name_list in id_list:
                                        for name in name_list:
                                            table_sfield = 'id,' + self.field1
                                            sql1 = "SELECT " + table_sfield + " FROM G2_3_new WHERE id='" + name + "'and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"

                                            table_gfield = 'id,' + self.field2
                                            sql2 = "SELECT " + table_gfield + " FROM G2_4_new WHERE id='" + name + "'and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"

                                            table_info = self.for_loop1(db, cursor, sql1, sql2, table1, table2)
                                            res2['table1'] = table1
                                            res2['table2'] = table2
                                    return res2
                                else:

                                    # table.
                                    # g23
                                    table1 = {}
                                    table2 = {}
                                    table_sfield = 'id,' + self.field1
                                    sql1 = "SELECT " + table_sfield + " FROM G2_3_new WHERE date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                                    # g24
                                    table_gfield = 'id,' + self.field2
                                    sql2 = "SELECT " + table_gfield + " FROM G2_4_new WHERE date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                                    table_info = self.for_loop1(db, cursor, sql1, sql2, table1, table2)
                                    res2['table1'] = table1
                                    res2['table2'] = table2

                                    return res2
                            if id[3].upper() == 'S' or id[4].upper() == 'S':
                                # sql = "select " + self.field + " from Graph where page='" + page + "' and id='" + id + "' and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                                sql = "select " + self.field + " from Graph where page='" + page + "' and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                                cursor.execute(sql)
                                results = cursor.fetchall()
                                res = {
                                    'sheet1': {"caption": "售电侧负荷电价曲线图", "axis_x": "时间", "axis_y": "电价（元/MWh）", "days": []},
                                    'sheet2': {"caption": "售电侧负荷电价曲线图", "axis_x": "时间", "axis_y": "负荷（MW）", "days": []}}
                                for row in results:
                                    date = row[-1].strftime('%Y-%m-%d').replace('-', '')
                                    caption, axis_x, axis_y = row[25:28]
                                    if caption == '售电侧负荷电价曲线图':
                                        if axis_y == '电价（元/MWh）':
                                            days = res['sheet1']["days"]
                                            self.for_loop(row, date, days)
                                        if axis_y == '负荷（MW）':
                                            days = res['sheet2']["days"]
                                            self.for_loop(row, date, days)
                                # g2-3 table
                                table_sfield = 'id,' + self.field1
                                sql1 = "SELECT " + table_sfield + " FROM G2_3_new WHERE id='" + id + "' and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                                cursor.execute(sql1)
                                results1 = cursor.fetchall()
                                res1 = {}
                                for row2 in results1:
                                    res1["caption"] = "售电企业申报数据及出清结果"
                                    res1["col"] = ["申报电量(MWh)", "日前结算(元)", "实时电量(MWh)", "实时结算(元)", "预测精度", "现货购电费用(元)", "现货单位成本(元/MWh)", "中长期电量(MWh)", "中长期价格(元/MWh)",
                                                   "中长期收益(元)", "购电费用(元)", "单位成本(元/MWh)", "售电收入(元)", "售电企业纯利润(元)"]
                                    date = row2[-1].replace('-', '')
                                    data_list = res1.setdefault("data", [])
                                    for li in data_list:
                                        if li['date'] == date:
                                            li['value'] = list(row2[1:15])
                                            break
                                    else:
                                        info = {"id": row2[0], "date": date, "value": list(row2[1:15])}
                                        data_list.append(info)
                                res['table'] = res1
                                return res
                            if id[3].upper() == 'G' or id[4].upper() == 'G':
                                        # sql = "select " + self.field + " from Graph where page='" + page + "' and id='" + id + "' and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                                        sql = "select " + self.field + " from Graph where page='" + page + "' and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                                        cursor.execute(sql)
                                        results = cursor.fetchall()
                                        res = {'sheet1': {"caption": "发电侧出力电价曲线图", "axis_x": "时间", "axis_y": "电价（元/MWh）"},
                                               'sheet2': {"caption": "发电侧出力电价曲线图", "axis_x": "时间", "axis_y": "出力（MW）"}}
                                        for row in results:
                                            date = row[-1].strftime('%Y-%m-%d').replace('-', '')
                                            caption, axis_x, axis_y = row[25:28]
                                            if caption == '发电侧出力电价曲线图':
                                                if axis_y == '电价（元/MWh）':
                                                    days = res['sheet1'].setdefault("days", [])
                                                    self.for_loop(row, date, days)
                                                if axis_y == '出力（MW）':
                                                    days = res['sheet2'].setdefault("days", [])
                                                    self.for_loop(row, date, days)
                                        # g4_new
                                        table_gfield = 'id,' + self.field2
                                        sql1 = "SELECT " + table_gfield + " FROM G2_4_new WHERE id='" + id + "' and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                                        cursor.execute(sql1)
                                        results1 = cursor.fetchall()
                                        res1 = {}
                                        for row1 in results1:
                                            res1["caption"] = "发电企业申报数据及出清结果"
                                            date = row1[-1].replace('-', '')
                                            res1['col'] = ["最低技术出力(MW)", "功率分段点1(MW)", "第一段报价(元/MWh)", "功率分段点2(MW)",
                                                           "第二段报价(元/MWh)","功率分段点3(MW)", "第三段报价(元/MWh)", "功率分段点4(MW)", "第四段报价(元/MWh)",
                                                           "最高技术出力(MW)","第五段报价（元/MWh）", "日前发电量（MWh）", "日前结算（元）", "实时发电量（MWh）","实时结算（元）", "发电偏差","现货收益（元）",
                                                           "现货单位收益（元/MWh）", "中长期电量（MWh）", "中长期价格（元/MWh）", "中长期收益（元）","发电收益（元）", "单位收益（元/MWh）","发电成本（元）", "发电企业利润"]
                                            data_list = res1.setdefault("data", [])
                                            for li in data_list:
                                                if li['date'] == date:
                                                    li['value'] = list(row1[1:26])
                                                    # li['id'] = row1[0]
                                                    break
                                            else:
                                                info = {"id": row1[0], "date": date, "value": list(row1[1:26])}
                                                data_list.append(info)
                                        res['table'] = res1
                                        return res
                        else:
                            res2 = {}
                            res2['sheet1'] = {"caption": "售电侧负荷电价曲线图", "axis_x": "时间", "axis_y": "电价（元/MWh）",
                                              "days": []}
                            res2['sheet2'] = {"caption": "售电侧负荷电价曲线图", "axis_x": "时间", "axis_y": "负荷（MW）", "days": []}
                            res2['sheet3'] = {"caption": "发电侧出力电价曲线图", "axis_x": "时间", "axis_y": "电价（元/MWh）",
                                              "days": []}
                            res2['sheet4'] = {"caption": "发电侧出力电价曲线图", "axis_x": "时间", "axis_y": "出力（MW）", "days": []}
                            sql = "select " + self.field + " from Graph where page='" + page + "' and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                            cursor.execute(sql)
                            results2 = cursor.fetchall()
                            for row in results2:
                                axisy = row[-3]
                                caption2 = row[-5]
                                if caption2 == '售电侧负荷电价曲线图':
                                    if axisy == '电价（元/MWh）':
                                        date = row[-1].strftime('%Y-%m-%d').replace('-', '')
                                        days = res2['sheet1']["days"]
                                        self.for_loop(row, date, days)

                                    elif axisy == '负荷（MW）':
                                        date = row[-1].strftime('%Y-%m-%d').replace('-', '')
                                        days = res2['sheet2']["days"]
                                        self.for_loop(row, date, days)

                                elif caption2 == '发电侧出力电价曲线图':
                                    if axisy == '电价（元/MWh）':
                                        date = row[-1].strftime('%Y-%m-%d').replace('-', '')
                                        days = res2['sheet3']["days"]
                                        self.for_loop(row, date, days)

                                    elif axisy == '出力（MW）':
                                        date = row[-1].strftime('%Y-%m-%d').replace('-', '')
                                        days = res2['sheet4']["days"]
                                        self.for_loop(row, date, days)
                            return res2
            else:
                # sheet
                if id == 'MASTER' or id == 'ISO':
                    res2 = {}
                    res2['sheet1'] = {"caption": "售电侧负荷电价曲线图", "axis_x": "时间", "axis_y": "电价（元/MWh）", "days": []}
                    res2['sheet2'] = {"caption": "售电侧负荷电价曲线图", "axis_x": "时间", "axis_y": "负荷（MW）", "days": []}
                    res2['sheet3'] = {"caption": "发电侧出力电价曲线图", "axis_x": "时间", "axis_y": "电价（元/MWh）", "days": []}
                    res2['sheet4'] = {"caption": "发电侧出力电价曲线图", "axis_x": "时间", "axis_y": "出力（MW）", "days": []}
                    sql = "select " + self.field + " from Graph where page='" + page + "' and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                    cursor.execute(sql)
                    results2 = cursor.fetchall()
                    for row in results2:
                        axisy = row[-3]
                        caption2 = row[-5]
                        if caption2 == '售电侧负荷电价曲线图':
                            if axisy == '电价（元/MWh）':
                                date = row[-1].strftime('%Y-%m-%d').replace('-', '')
                                days = res2['sheet1']["days"]
                                self.for_loop(row, date, days)

                            elif axisy == '负荷（MW）':
                                date = row[-1].strftime('%Y-%m-%d').replace('-', '')
                                days = res2['sheet2']["days"]
                                self.for_loop(row, date, days)
                        elif caption2 == '发电侧出力电价曲线图':
                            if axisy == '电价（元/MWh）':
                                date = row[-1].strftime('%Y-%m-%d').replace('-', '')
                                days = res2['sheet3']["days"]
                                self.for_loop(row, date, days)

                            elif axisy == '出力（MW）':
                                date = row[-1].strftime('%Y-%m-%d').replace('-', '')
                                days = res2['sheet4']["days"]
                                self.for_loop(row, date, days)

                    # table.
                    # g23
                    table1 = {}
                    table_sfield = 'id,' + self.field1
                    sql1 = "SELECT " + table_sfield + " FROM G2_3_new WHERE date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                    cursor.execute(sql1)
                    results3 = cursor.fetchall()
                    for row3 in results3:
                        table1["caption"] = "售电企业申报数据及出清结果"
                        table1["col"] = ["申报电量", "日前结算", "实时电量", "实时结算", "预测精度", "现货购电费用", "现货单位收益", "中长期电量", "中长期价格",
                                         "中长期收益", "购电费用", "单位成本", "售电收入", "售电企业纯利润"]
                        date = row3[-1].replace('-', '')
                        data_list1 = table1.setdefault("data", [])
                        for li in data_list1:
                            if li['date'] == date and li['id'] == row3[0]:
                                li['value'] = list(row3[1:15])
                                break
                        else:
                            info = {"id": row3[0], "date": date, "value": list(row3[1:15])}
                            data_list1.append(info)
                    # g24
                    table_gfield = 'id,' + self.field2
                    sql2 = "SELECT " + table_gfield + " FROM G2_4_new WHERE date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                    cursor.execute(sql2)
                    results4 = cursor.fetchall()
                    table2 = {}
                    for row4 in results4:
                        table2["caption"] = "发电企业申报数据及出清结果"
                        table2['col'] = ["最低技术出力(MW)", "功率分段点1(MW)", "第一段报价(元/MWh)", "功率分段点2(MW)", "第二段报价(元/MWh)",
                                         "功率分段点3(MW)", "第三段报价(元/MWh)", "功率分段点4(MW)", "第四段报价(元/MWh)", "最高技术出力(MW)",
                                         "第五段报价（元/MWh）", "日前发电量（MWh）", "日前结算（元）", "实时发电量（MWh）", "实时结算（元）", "发电偏差",
                                         "现货收益（元）", "现货单位收益（元/MWh）", "中长期电量（MWh）", "中长期价格（元/MWh）", "中长期收益（元）",
                                         "发电收益（元）", "单位收益（元/MWh）", "发电成本（元）", "发电企业利润"]
                        data_list2 = table2.setdefault("data", [])
                        date = row4[-1].replace('-', '')
                        for li in data_list2:
                            if li['date'] == date and li['id'] == row4[0]:
                                li['value'] = list(row4[1:26])
                                break
                        else:
                            info = {"id": row4[0], "date": date, "value": list(row4[1:26])}
                            data_list2.append(info)
                    res2['table1'] = table1
                    res2['table2'] = table2

                    return res2
                elif id[3] == 'S' or id[3] == 'G':
                    sql = "select " + self.field + " from Graph where page='" + page + "' and id='" + id + "' and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                    cursor.execute(sql)
                    results = cursor.fetchall()
                    res = {'sheet1': {}, 'sheet2': {}}
                    for row in results:
                        date = row[-1].strftime('%Y-%m-%d').replace('-', '')
                        sheetid = row[-6]
                        if sheetid == 'sheet1':
                            sheet_data = res[sheetid]
                        if sheetid == 'sheet2':
                            sheet_data = res[sheetid]
                        if sheetid == 'sheet3':
                            sheet_data = res["sheet1"]
                        if sheetid == 'sheet4':
                            sheet_data = res["sheet2"]
                        caption, axis_x, axis_y = row[25:28]
                        sheet_data.setdefault("caption", caption)
                        sheet_data.setdefault("axis_x", axis_x)
                        sheet_data.setdefault("axis_y", axis_y)
                        days = sheet_data.setdefault("days", [])
                        for days_dic in days:
                            days_dic_date = days_dic.get("date")
                            if date == days_dic_date:
                                days_dic_data = days_dic.get("data")
                                days_dic_data.append({"title": row[-2], "value": row[:24]})
                                break
                        else:
                            new_days_dic = {"date": date, "data": [{"title": row[-2], "value": row[:24]}]}
                            days.append(new_days_dic)
                    if id[3] == 'S':
                        # g2-3 table
                        sql1 = "SELECT " + self.field1 + " FROM G2_3_new WHERE id='" + id + "' and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                        cursor.execute(sql1)
                        results1 = cursor.fetchall()
                        res1 = {}
                        for row2 in results1:
                            res1["caption"] = "售电企业申报数据及出清结果"
                            res1["col"] = ["申报电量", "日前结算", "实时电量", "实时结算", "预测精度", "现货购电费用", "现货单位收益", "中长期电量", "中长期价格",
                                           "中长期收益","购电费用", "单位成本", "售电收入", "售电企业纯利润"]
                            date = row2[-1].replace('-', '')
                            data_list = res1.setdefault("data", [])
                            for li in data_list:
                                if li['date'] == date:
                                    li['value'] = list(row2[:14])
                                    break
                            else:
                                info = {"date": date, "value": list(row2[:14])}
                                data_list.append(info)
                        res['table'] = res1
                        return res
                    if id[3] == 'G':
                        # g4_new
                        sql1 = "SELECT " + self.field2 + " FROM G2_4_new WHERE id='" + id + "' and date_format(time, '%Y-%m-%d') between '" + start_date + "' and '" + end_date + "'"
                        cursor.execute(sql1)
                        results1 = cursor.fetchall()
                        res1 = {}
                        for row1 in results1:
                            res1["caption"] = "发电企业申报数据及出清结果"
                            date = row1[-1].replace('-', '')
                            res1['col'] = ["最低技术出力(MW)", "功率分段点1(MW)", "第一段报价(元/MWh)", "功率分段点2(MW)", "第二段报价(元/MWh)",
                                           "功率分段点3(MW)", "第三段报价(元/MWh)", "功率分段点4(MW)", "第四段报价(元/MWh)", "最高技术出力(MW)",
                                           "第五段报价（元/MWh）", "日前发电量（MWh）", "日前结算（元）", "实时发电量（MWh）", "实时结算（元）", "发电偏差",
                                           "现货收益（元）",
                                           "现货单位收益（元/MWh）", "中长期电量（MWh）", "中长期价格（元/MWh）", "中长期收益（元）", "发电收益（元）",
                                           "单位收益（元/MWh）",
                                           "发电成本（元）", "发电企业利润"]
                            data_list = res1.setdefault("data", [])
                            for li in data_list:
                                if li['date'] == date:
                                    li['value'] = list(row1[0:25])
                                    break
                            else:
                                info = {"date": date, "value": list(row1[0:25])}
                                data_list.append(info)
                        res['table'] = res1
                        return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    # 历史新增
    def add(self, data):
        db, cursor = opendb()
        try:
            data1 = {
                "state": -2,
                "page": "hist"
            }
            self.delete(data1)

            sql = "SELECT * FROM Graph"
            cursor.execute(sql)
            description = cursor.description
            page = ((data.get('page')),)

            for key, value in data.items():
                if key[0:5] == 'sheet':
                    # print(value)
                    for key1, value1 in value.items():
                        sheetid = tuple([key])  # sheet
                        caption = ((value['caption']),)
                        axis_x = ((value['axis_x']),)
                        axis_y = ((value['axis_y']),)

                    for key2, value2 in value.get('data').items():
                        ctitle = ((key2),)  # 节点xxx
                        value_tuple = tuple(value2)  # 节点对应值
                        field = []
                        for i in range(1, 32):
                            if description[i][0] != 'id':
                                field.append(description[i][0])
                        field = str(tuple(field)).replace("'", "")
                        query =  value_tuple + page + sheetid + caption + axis_x + axis_y + ctitle
                        sql_1 = "INSERT INTO Graph{0} VALUES{1}".format(field, str(query))
                        # print(sql_1,'123123')
                        cursor.execute(sql_1)

                if key[0:5] == 'table':
                    for k, v in data[key].items():
                        field = []
                        query = []
                        query.append(k)
                        if k[3] == 'S':
                            sql1 = "SELECT * FROM G2_3_new"
                            cursor.execute(sql1)
                        if k[3] == 'G':
                            sql2 = "SELECT * FROM G2_4_new"
                            cursor.execute(sql2)
                        description = cursor.description
                        fields = []
                        for i in range(1, len(description)):
                            if description[i][0] != 'date' and description[i][0] != 'time':
                                fields.append(description[i][0])
                        for k1, v1 in v.items():
                            if k1 not in field:
                                field.append(k1)
                                query.append(v1)
                        newquery = tuple(query)

                        if k[3] == 'S':

                            sql1_1 = "INSERT INTO G2_3_new(id,Dq,Dp,Cq,Cp,eps,Ccost,CPerfit,Fq,Fc,Fp,spend,pcost,load_profits,load_pure_profits) VALUES{0}".format(str(newquery))
                            # print(sql1_1,'11111111111')
                            cursor.execute(sql1_1)
                        if k[3] == 'G':
                            sql2_1 = "INSERT INTO G2_4_new(id, pmin, sp1, k1, sp2, k2, sp3, k3, sp4, k4, pmax, k5, Nq, Np, Cq, Cp, delta, Nperfit, Npperfit, Fq, Fp, Fperfit, perfit, pperfit, gen_cost, gen_pure_profits) VALUES{0}".format(str(newquery))
                            # print(sql2_1,'22222222222')
                            cursor.execute(sql2_1)

            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    # 删除历史
    def delete(self, data):
        info = data.get('date')
        db, cursor = opendb()
        try:
            if info == None:
                info = time.strftime('%Y-%m-%d', time.localtime(time.time())).replace('-', '')
            t1 = info[:4]
            t2 = info[4:6]
            t3 = info[6:]
            date = t1 + '-' + t2 + '-' + t3
            page = data['page']
            # delete from 表名 where 条件 时间=时间,page=page
            # sql = "SELECT *  FROM Graph WHERE page='" + page + "' AND Date(time)='" + date + "'"
            sql = "DELETE FROM Graph WHERE page='" + page + "' AND date_format(time, '%Y-%m-%d')='" + date + "'"
            # print(sql)
            cursor.execute(sql)
            sql2 = "DELETE FROM G2_3_new WHERE date_format(time, '%Y-%m-%d')='" + date + "'"
            # print(sql2)
            cursor.execute(sql2)
            sql3 = "DELETE FROM G2_4_new WHERE date_format(time, '%Y-%m-%d')='" + date + "'"
            # print(sql3)
            cursor.execute(sql3)
            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def post(self, request):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        if request.method == 'POST':
            data = json.loads(request.body)
            # print(data)
            if data["state"] == 2:
                res = self.query(id)
                res["state"] = data["state"]
                return Response(res)
            elif data['state'] == 1:
                res = self.query_new(data)
                if res:
                    res['state'] = 1
                return Response(res)

            elif data["state"] == -1:
                res = self.add(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            elif data['state'] == -2:
                res = self.delete(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            else:
                return Response({"state": -400})


# 4.Match
class PostInfo4(APIView):

    def query(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        ret = getGraph(data["page"], "master")

        # today=datetime.date.today()
        # tomorrow = datetime.date.today() + datetime.timedelta(days=1)
        # print(today)
        # print("today: "+today.strftime('%y%m%d'))
        # userdone = querylog(today.strftime('%y%m%d'),tomorrow.strftime('%y%m%d'))
        # userG = getGenIDs()
        # userS = getSellIDs()
        # userGdone = [user for user in userG if user in userdone]
        # userGdoing = [user for user in userG if user not in userdone]
        # userSdone = [user for user in userS if user in userdone]
        # userSdoing = [user for user in userS if user not in userdone]

        # userGdone, userGdoing, userSdone, userSdoing = getDoneIDs(today.strftime('%y%m%d'),tomorrow.strftime('%y%m%d'))
        db, cursor = opendb()
        try:
            res = {}
            sql = "SELECT id FROM " + GenOffer + " WHERE date_format(time,'%Y-%m-%d')=date_format(now(),'%Y-%m-%d')"
            print(sql)
            cursor.execute(sql)
            results = cursor.fetchall()
            GenDone = []
            for submit in results:
                GenDone.append(submit[0])
            sql = "SELECT id FROM " + GenOffer + " WHERE date_format(time,'%Y-%m-%d')<date_format(now(),'%Y-%m-%d')"
            print(sql)
            cursor.execute(sql)
            results = cursor.fetchall()
            GenDoing = []
            for submit in results:
                GenDoing.append(submit[0])

            sql = "SELECT id FROM " + graphTable + " WHERE date_format(time,'%Y-%m-%d')=date_format(now(),'%Y-%m-%d') AND page='config'"
            print(sql)
            cursor.execute(sql)
            results = cursor.fetchall()
            SaleDone = []
            for submit in results:
                SaleDone.append(submit[0])
            sql = "SELECT id FROM " + graphTable + " WHERE date_format(time,'%Y-%m-%d')<date_format(now(),'%Y-%m-%d') AND page='config'"
            print(sql)
            cursor.execute(sql)
            results = cursor.fetchall()
            SaleDoing = []
            for submit in results:
                SaleDoing.append(submit[0])

            # ret = [userGdoing,userGdone,Sale,SaleDoing]
            # print(userGdone)
            ret[0] = GenDoing
            ret[1] = GenDone
            ret[2] = SaleDoing
            ret[3] = SaleDone
            ret["ratio"] = [round(len(ret[1]) / (len(ret[0]) + len(ret[1])), 3),
                            round(len(ret[3]) / (len(ret[2]) + len(ret[3])), 3)]

            return ret
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def work(self):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        data = {"state": -1}
        # url = "http://114.215.111.61/rest_service"
        url = "http://127.0.0.1:8081/rest_service"
        data_json = json.dumps(data)
        headers = {'Content-type': 'application/json'}
        response = requests.post(url, data=data_json, headers=headers)
        return True

    def modify(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        ret = setGraph(data)

        return ret

    def post(self, request):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        # if request.method == 'POST':
        data = json.loads(request.body)
        data["user"] = {"id": "master"}
        accesslog(data)
        res = None
        if data["state"] > 0:
            res = self.query(data)
            res["state"] = data["state"]
            return Response(res)
        elif data["state"] == -1:
            res = self.work()
            # if res != None:
            #     return Response({"state": -200})
            # else:
            #     return Response({"state": -400})
        elif data["state"] == -2:
            res = self.modify(data)
            # if res != None:
            #     return Response({"state": -200})
            # else:
            #     return Response({"state": -400})
        # else:
        #     return None

        if res != None:
            return Response({"state": -200})
        else:
            return Response({"state": -400})


# 5.generator analysis
class PostInfo5(APIView):
    def query(self, id=""):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb()
        try:
            res = {}
            res["quantity"] = 0
            res["profit"] = 0
            res["count"] = 0
            res["sheet1"] = []
            # res["sheet2"] = {}
            field1 = "quantity,profit,count"
            field2 = "name,Nq,Nmax,Nmin,Cq,Cmax,Cmin"
            field6 = "id,Cq,Nq,delta,Np,Cp,Nperfit,Npperfit,Fq,Fp,Fperfit,perfit,pperfit"
            order1 = field1.split(",")
            order2 = field2.split(",")
            order6 = field6.split(",")
            if id:
                sql6 = "SELECT " + field6 + " FROM G5_6 where id='" + id + "'"
            else:
                sql1 = "SELECT " + field1 + " FROM G5_1"
                cursor.execute(sql1)
                results = cursor.fetchall()
                for row in results:
                    for k, v in enumerate(order1):
                        res[v] = row[k]
        
                sql2 = "SELECT " + field2 + " FROM G5_2"
                cursor.execute(sql2)
                results = cursor.fetchall()
                for row in results:
                    temp = {}
                    for k, v in enumerate(order2):
                        temp[v] = row[k]
                    res["sheet1"].append(temp)
                sql6 = "SELECT " + field6 + " FROM G5_6"
        
            cursor.execute(sql6)
            results = cursor.fetchall()
            print(results,'rererere')
            res["data"] = []
            for row in results:
                temp = {}
                for k, v in enumerate(order6):
                    temp[v] = row[k]
                res["data"].append(temp)
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()



        # *****请求主页面1.1新*******

        # print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        # db, cursor = opendb()
        # try:
        #     res = {}
        #     res["quantity"] = 0
        #     res["profit"] = 0
        #     res["count"] = 0
        #     res["sheet1"] = {}
        #     res["sheet2"] = {}
        #     field1 = "quantity,profit,count"
        #     field2 = "name,Nq,Nmax,Nmin,Cq,Cmax,Cmin"
        #     field6 = "id,Cq,Nq,delta,Np,Cp,Nperfit,Npperfit,Fq,Fp,Fperfit,perfit,pperfit"
        #     order1 = field1.split(",")
        #     if id:
        #         sql6 = "SELECT " + field6 + " FROM G5_6 where id='" + id + "'"
        #     else:
        #         sql1 = "SELECT " + field1 + " FROM G5_1"
        #         cursor.execute(sql1)
        #         results = cursor.fetchall()
        #         for row in results:
        #             for k, v in enumerate(order1):
        #                 res[v] = row[k]

        #         sql2 = "SELECT " + field2 + " FROM G5_2"
        #         cursor.execute(sql2)
        #         results2 = cursor.fetchall()
        #         res["sheet1"] = {"0": ["name", "Nq", "Nmax", "Nmin", "Cq", "Cmax", "Cmin"]}
        #         for num2 in range(len(results2)):
        #             res["sheet1"][str(num2+1)] = list(results2[num2])
        #         sql6 = "SELECT " + field6 + " FROM G5_6"

        #     cursor.execute(sql6)
        #     results3 = cursor.fetchall()
        #     res["sheet2"] = {"0": ["id","Cq","Nq","delta","Np","Cp","Nperfit","Npperfit","Fq","Fp","Fperfit","perfit","pperfit"]}
        #     for num3 in range(len(results3)):
        #         res["sheet2"][str(num3 + 1)] = list(results3[num3])
        #     return res
        # except Exception:
        #     db.rollback()
        #     return None
        # finally:
        #     db.close()



        # def querydetail(self,data):
        #     print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        #     db, cursor = opendb()
        #     try:
        #         res = {}
        #         id = data["user"]["id"]
        #         res["quantity"] = 0
        #         res["profit"] = 0
        #         res["count"] = 0
        #         res["sheet1"] = {}
        #         res["sheet2"] = {}

        #         sql6 = "SELECT * FROM G5_6 where id='"+id+"'"
        #         cursor.execute(sql6)
        #         results = cursor.fetchall()
        #         res["data"] = []
        #         for row in results:
        #             temp = {
        #                 "id": row[1],
        #                 "Cq": row[2],
        #                 "Nq": row[3],
        #                 "delta": row[4],
        #                 "Np": row[5],
        #                 "Cp": row[6],
        #                 "Nperfit": row[7],
        #                 "Npperfit": row[8],
        #                 "Fq": row[9],
        #                 "Fp": row[10],
        #                 "Fperfit": row[11],
        #                 "perfit": row[12],
        #                 "pperfit": row[13]
        #             }
        #             # res["data"][row[1]] = temp
        #             res["data"].append(temp)
        #         return res
        #     except Exception:
        #         db.rollback()
        #         return None
        #     finally:
        #         db.close()

    def querysubpage(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb()
        try:
            res = {}
            id = data["id"]
            res = getGraph(data["page"], id)

            group = "sheet2"
            results = getGenOffer(cursor, id)
            # print(results)
            res[group] = {}
            row = results[0]
            res[group]["value"] = [[row[2], row[3]], [row[4], row[5]], [row[6], row[7]], \
                                   [row[8], row[9]], [row[10], row[11]], [row[12], row[13]]]
            res[group]["caption"] = "电价出力曲线"
            res[group]["axis_x"] = "出力"
            res[group]["axis_y"] = "报价"
            # print(res)

            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()
                
    # new
    def new_querysubpage(self,data):
        field = "v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17,v18,v19,v20,v21,v22,v23,v24,\
                        sheet,caption,axis_x,axis_y,ctitle"
        db, cursor = opendb()
        try:
            id = data.get('id')
            id_endpoint = id[:2]
            page = data.get("page")
            # today
            today = datetime.datetime.now()
            date = today.strftime('%Y-%m-%d')
            # yesterday
            offset = datetime.timedelta(days=-1)
            yesterday = (today + offset).strftime('%Y-%m-%d')

            sql = "SELECT " + field + " FROM " + graphTable + " WHERE page='" + page + "' and date_format(time, '%Y-%m-%d')='"+date+"'"
            sql1 = "SELECT " + field + " FROM " + graphTable + " WHERE page='" + page + "' and date_format(time, '%Y-%m-%d')='"+yesterday+"'"

            if id not in ["master", "iso"]:
                sql += " and id='" + id + "'"
                sql1 += " and id='" + id + "'"
            cursor.execute(sql)
            results = cursor.fetchall()

            res = {"sheet1":{"caption": "发电机出力与电价曲线图","axis_x": "时间","axis_y": "出力（MW）","data":[]},
                   "sheet2":{"caption": "发电机出力与电价曲线图","axis_x": "时间","axis_y": "电价（元/MWh）","data":[]},
                   "sheet3":{"caption": "电价出力曲线","axis_x": "出力","axis_y": "报价"}}
            if results == ():
                cursor.execute(sql1)
                results = cursor.fetchall()
            for row in results:
                gen_dict = {}
                axis_y = row[-2]
                if axis_y[:2] == '出力':
                    gen_dict['title'] = row[-1]
                    gen_dict['value'] = list(row[:24])
                    res['sheet1']['data'].append(gen_dict)
                if axis_y[:2] == '电价':
                    gen_dict['title'] = row[-1]
                    gen_dict['value'] = list(row[:24])
                    res['sheet2']['data'].append(gen_dict)


            results = getGenOffer(cursor, id)
            row = results[0]
            res["sheet3"]["value"] = [[row[2], row[3]], [row[4], row[5]], [row[6], row[7]], \
                                   [row[8], row[9]], [row[10], row[11]], [row[12], row[13]]]
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def modify(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb()
        try:
            sql1 = "SELECT * FROM G5_1"
            cursor.execute(sql1)
            description1 = cursor.description
            sql2 = "SELECT * FROM G5_2"
            cursor.execute(sql2)
            description2 = cursor.description
            sql6 = "SELECT * FROM G5_6"
            cursor.execute(sql6)
            description6 = cursor.description
            sql1 = "DELETE FROM G5_1"
            cursor.execute(sql1)
            sql2 = "DELETE FROM G5_2"
            cursor.execute(sql2)
            sql6 = "DELETE FROM G5_6"
            cursor.execute(sql6)
            field = [description1[1][0], description1[2][0], description1[3][0]]
            field = str(tuple(field)).replace("'", "")
            query = (data["quantity"], data["profit"], data["count"])
            sql1 = "INSERT INTO G5_1{0} VALUES{1}".format(field, str(query))
            cursor.execute(sql1)
            for k1, v1 in data["sheet1"].items():
                field = []
                for i in range(1, 8):
                    field.append(description2[i][0])
                field = str(tuple(field)).replace("'", "")
                query = []
                for k2, v2 in v1.items():
                    query.append(v2)
                query = tuple(query)
                sql2 = "INSERT INTO G5_2{0} VALUES{1}".format(field, str(query))
                cursor.execute(sql2)

            for k1, v1 in data["data"].items():
                field = []
                for i in range(1, 14):
                    field.append(description6[i][0])
                field = str(tuple(field)).replace("'", "")
                query = []
                for k2, v2 in v1.items():
                    query.append(v2)
                query = tuple(query)
                sql6 = "INSERT INTO G5_6{0} VALUES{1}".format(field, str(query))
                cursor.execute(sql6)

            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def reset(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb()
        try:
            sql1 = "SELECT * FROM G5_1"
            cursor.execute(sql1)
            description1 = cursor.description
            sql2 = "SELECT * FROM G5_2"
            cursor.execute(sql2)
            description2 = cursor.description
            sql6 = "SELECT * FROM G5_6"
            cursor.execute(sql6)
            description6 = cursor.description
            sql1 = "DELETE FROM G5_1"
            cursor.execute(sql1)
            sql2 = "DELETE FROM G5_2"
            cursor.execute(sql2)
            sql6 = "DELETE FROM G5_6"
            cursor.execute(sql6)
            field = [description1[1][0], description1[2][0], description1[3][0]]
            field = str(tuple(field)).replace("'", "")
            query = (data["quantity"], data["profit"], data["count"])
            sql1 = "INSERT INTO G5_1{0} VALUES{1}".format(field, str(query))
            cursor.execute(sql1)
            for k1, v1 in data["sheet1"].items():
                field = []
                for i in range(1, 8):
                    field.append(description2[i][0])
                field = str(tuple(field)).replace("'", "")
                query = []
                for k2, v2 in v1.items():
                    query.append(v2)
                query = tuple(query)
                sql2 = "INSERT INTO G5_2{0} VALUES{1}".format(field, str(query))
                cursor.execute(sql2)

            for k1, v1 in data["data"].items():
                field = []
                for i in range(1, 14):
                    field.append(description6[i][0])
                field = str(tuple(field)).replace("'", "")
                query = []
                for k2, v2 in v1.items():
                    query.append(v2)
                query = tuple(query)
                sql6 = "INSERT INTO G5_6{0} VALUES{1}".format(field, str(query))
                cursor.execute(sql6)

            db.commit()

            if "Graphs" in data:
                setGraph_new(data)
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()
                
    # new
    def addtion(self,data):
        db, cursor = opendb()
        try:
            today = datetime.datetime.now()
            date = today.strftime('%Y-%m-%d')
            sql = "DELETE FROM Graph WHERE page='gen_analysis' and date_format(time, '%Y-%m-%d')='"+date+"'"
            cursor.execute(sql)

            sql1 = "SELECT * FROM G5_1"
            cursor.execute(sql1)
            description1 = cursor.description
            sql2 = "SELECT * FROM G5_2"
            cursor.execute(sql2)
            description2 = cursor.description
            sql6 = "SELECT * FROM G5_6"
            cursor.execute(sql6)
            description6 = cursor.description
            sql1 = "DELETE FROM G5_1"
            cursor.execute(sql1)
            sql2 = "DELETE FROM G5_2"
            cursor.execute(sql2)
            sql6 = "DELETE FROM G5_6"
            cursor.execute(sql6)
            field = [description1[1][0], description1[2][0], description1[3][0]]
            field = str(tuple(field)).replace("'", "")
            query = (data["quantity"], data["profit"], data["count"])
            sql1 = "INSERT INTO G5_1{0} VALUES{1}".format(field, str(query))
            cursor.execute(sql1)
            for k1, v1 in data["data"].items():
                field = []
                for i in range(1, 8):
                    field.append(description2[i][0])
                field = str(tuple(field)).replace("'", "")
                query = []
                for k2, v2 in v1.items():
                    query.append(v2)
                query = tuple(query)
                sql2 = "INSERT INTO G5_2{0} VALUES{1}".format(field, str(query))
                cursor.execute(sql2)
            for k1, v1 in data["data1"].items():
                field = []
                for i in range(1, 14):
                    field.append(description6[i][0])
                field = str(tuple(field)).replace("'", "")
                query = []
                for k2, v2 in v1.items():
                    query.append(v2)
                query = tuple(query)
                sql6 = "INSERT INTO G5_6{0} VALUES{1}".format(field, str(query))
                cursor.execute(sql6)

            # "graph"
            info = []
            for key in data:
                if key[:5] == 'sheet':
                    info.append(key)
            for sheetid in info:
                caption = data.get(sheetid).get('caption')
                axis_x = data.get(sheetid).get('axis_x')
                axis_y = data.get(sheetid).get('axis_y')
                for row in data.get(sheetid).get('data'):
                    id = row.get('id')
                    sheetid = sheetid
                    ctitle = row.get('title')
                    value = row.get('value')
                    field_data = "(id,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17,v18,v19,v20,v21,v22,v23,v24,page,sheet,caption,axis_x,axis_y,ctitle)"
                    query_data = (id,) + tuple(value) + ('gen_analysis',) + (sheetid,) + (caption,) + (axis_x,) + (
                    axis_y,) + (ctitle,)
                    sql0 = "INSERT INTO Graph{0} VALUES{1}".format(field_data, str(query_data))
                    cursor.execute(sql0)
            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()


    def post(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            accesslog(data)
            if data["state"] > 0:
                if "subpage" in data:
                    # res = self.querysubpage(data)
                    if data.get("version") == 2:
                        res = self.new_querysubpage(data)
                    else:
                        res = self.querysubpage(data)

                else:
                    id = data["user"]["id"]
                    if id in ["master", "iso"]:
                        res = self.query()
                    else:
                        res = self.query(id)
                        # res = self.querydetail(data)
                res["state"] = 2
                return Response(res)
            elif data["state"] == -1:
                if data.get("version") == 2:
                    res = self.addtion(data)
                else:
                    # res = self.modify(data)
                    res = self.reset(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            else:
                return None



# 6.saler analysis
class PostInfo6(APIView):
    def query(self, id=""):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        field1 = "count,profit,quantity,q,maxN,minN,Nq,Nmax,Nmin"
        order1 = field1.split(",")
        db, cursor = opendb()
        try:
            res = {}
            res["sheet1"] = {}
            res["quantity"] = 0
            res["profit"] = 0
            res["count"] = 0
            # res["label"] = {}
            # id = data["user"]["id"]
            field2 = "id,Q,count,Cq,Cp,eps,Ccost,Cperfit,Fq,Fc,Fp,spend,pcost"
            order2 = field2.split(",")
            if id:
                sql2 = "SELECT " + field2 + " FROM G6_2 where id='" + id + "'"
            else:
                sql1 = "SELECT " + field1 + " FROM G6_1"
                cursor.execute(sql1)
                results = cursor.fetchall()
                for row in results:
                    temp = {
                        "q": row[order1.index("q")],
                        "maxN": row[order1.index("maxN")],
                        "minN": row[order1.index("minN")],
                        "Nq": row[order1.index("Nq")],
                        "Nmax": row[order1.index("Nmax")],
                        "Nmin": row[order1.index("Nmin")]
                    }
                    res["sheet1"][0] = temp
                    res["count"] = row[order1.index("count")]
                    res["profit"] = row[order1.index("profit")]
                    res["quantity"] = row[order1.index("quantity")]
                sql2 = "SELECT " + field2 + " FROM G6_2"

            # sql = "SELECT * FROM G6_2"
            # print(sql2)
            cursor.execute(sql2)
            results = cursor.fetchall()
            # print(results)
            # print(order2)
            res["data"] = []
            for row in results:
                temp = {}
                for k, v in enumerate(order2):
                    temp[v] = row[k]
                res["data"].append(temp)
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()
        # def querydetail(self, data):
        #     print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        #     print(data)
        #     db, cursor = opendb()
        #     try:
        #         res = {}
        #         id = data["user"]["id"]
        #         field2 = "id,Q,count,Cq,Cp,eps,Ccost,Cperfit,Fq,Fc,Fp,spend,pcost"
        #         order2 = field2.split(",")
        #         sql = "SELECT "+field2+" FROM G6_2 where id='" + id + "'"
        #         cursor.execute(sql)
        #         results = cursor.fetchall()
        #         res["data"] = []
        #         for row in results:
        #             temp = {}
        #             for k,v in enumerate(order2):
        #                 temp[v] = row[k]
        #             res["data"].append(temp)

        #             # temp = {
        #             #     "id": row[1],
        #             #     "Q": row[2],
        #             #     "count": row[3],
        #             #     "Cq": row[4],
        #             #     "Cp": row[5],
        #             #     "eps": row[6],
        #             #     "Ccost": row[7],
        #             #     "Cperfit": row[8],
        #             #     "Fq": row[9],
        #             #     "Fc": row[10],
        #             #     "Fp": row[11],
        #             #     "spend": row[12],
        #             #     "pcost": row[13]
        #             # }
        #             # res["data"].append(temp)

        #         res["sheet1"] = {}
        #         res["sheet2"] = {}
        #         res["count"] = 0
        #         res["profit"] = 0
        #         res["quantity"] = 0
        #         return res
        #     except Exception:
        #         db.rollback()
        #         return None
        #     finally:
        #         db.close()
    def querysubpage(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb()
        try:
            # # id = data["user"]["id"]
            # print(data)
            # id = data["id"]
            # print(id)
            # res = getGraph(data["page"], id)
            # print(res)
            # # sql = "SELECT * FROM G6_2 where id='" + id + "'"
            # # print(sql)
            # # cursor.execute(sql)
            # # results = cursor.fetchall()
            # # print(results)
            # # res["data"] = {}
            # # for row in results:
            # #     print(row)
            # #     res["data"]["id"] = row[1],
            # #     res["data"]["Q"] = row[2],
            # #     res["data"]["count"] = row[3],
            # #     res["data"]["Cq"] = row[4],
            # #     res["data"]["Cp"] = row[5],
            # #     res["data"]["eps"] = row[6],
            # #     res["data"]["Ccost"] = row[7],
            # #     res["data"]["Cperfit"] = row[8],
            # #     res["data"]["Fq"] = row[9],
            # #     res["data"]["Fc"] = row[10],
            # #     res["data"]["Fp"] = row[11],
            # #     res["data"]["spend"] =row[12],
            # #     res["data"]["pcost"] = row[13]
            # # print("result is:")
            # # print(res)
            # return res
            # ********************
            # id = data["user"]["id"]
            id = data["id"]
            res = getGraph(data["page"], id)
            sql = "SELECT * FROM G6_2 where id='" + id + "'"
            cursor.execute(sql)
            results = cursor.fetchall()
            res["data"] = {}
            for row in results:
                res["data"]["id"] = row[1]
                res["data"]["Q"] = row[2]
                res["data"]["count"] = row[3]
                res["data"]["Cq"] = row[4]
                res["data"]["Cp"] = row[5]
                res["data"]["eps"] = row[6]
                res["data"]["Ccost"] = row[7]
                res["data"]["Cperfit"] = row[8]
                res["data"]["Fq"] = row[9]
                res["data"]["Fc"] = row[10]
                res["data"]["Fp"] = row[11]
                res["data"]["spend"] = row[12]
                res["data"]["pcost"] = row[13]
            print(res)
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()
                
    # new
    def new_querysubpage(self,data):
        db,cursor = opendb()
        try:
            field = "v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17,v18,v19,v20,v21,v22,v23,v24,\
                         sheet,caption,axis_x,axis_y,ctitle"
            id = data["id"]
            # res = getGraph(data["page"], id)
            # print(res,'rerere')
            res = {}
            sql = "SELECT * FROM G6_2 where id='" + id + "'"
            cursor.execute(sql)
            results = cursor.fetchall()
            res["data"] = {}
            for row in results:
                res["data"]["id"] = row[1]
                res["data"]["Q"] = row[2]
                res["data"]["count"] = row[3]
                res["data"]["Cq"] = row[4]
                res["data"]["Cp"] = row[5]
                res["data"]["eps"] = row[6]
                res["data"]["Ccost"] = row[7]
                res["data"]["Cperfit"] = row[8]
                res["data"]["Fq"] = row[9]
                res["data"]["Fc"] = row[10]
                res["data"]["Fp"] = row[11]
                res["data"]["spend"] = row[12]
                res["data"]["pcost"] = row[13]
            res["sheet1"] = {"caption": "负荷与电价曲线图","axis_x": "时间","axis_y": "电价（元/MWh）","data":[]}
            res["sheet2"] = {"caption": "负荷与电价曲线图","axis_x": "时间","axis_y": "负荷（MW）","data":[]}
            # today
            today = datetime.datetime.now()
            date = today.strftime('%Y-%m-%d')
            # yesterday
            offset = datetime.timedelta(days=-1)
            yesterday = (today + offset).strftime('%Y-%m-%d')

            sql1 = "SELECT " + 'id,'+ field +" FROM Graph WHERE id='" + id + "' and page='sale_analysis' and date_format(time, '%Y-%m-%d')='"+date+"'"
            sql2 = "SELECT " + 'id,'+ field +" FROM Graph WHERE id='" + id + "' and page='sale_analysis' and date_format(time, '%Y-%m-%d')='"+yesterday+"'"
            cursor.execute(sql1)
            results1 = cursor.fetchall()
            if results1 == ():
                cursor.execute(sql2)
                results1 = cursor.fetchall()
            for row1 in results1:
                axis_y = row1[-2]
                
                if axis_y[:2] == '电价':
                    info = {}
                    id = row1[0]
                    value = list(row1[1:25])
                    info['title'] = row1[-1]
                    info['value'] = value
                    res["sheet1"]["data"].append(info)

                if axis_y[:2] == '负荷':
                    info = {}
                    id = row1[0]
                    value = list(row1[1:25])
                    info['title'] = row1[-1]
                    info['value'] = value
                    res["sheet2"]["data"].append(info)
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()


    def modify(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb()
        try:
            sql = "DELETE FROM G6_1"
            cursor.execute(sql)
            sql = "DELETE FROM G6_2"
            cursor.execute(sql)
            # sql = "DELETE FROM G6_3"
            # cursor.execute(sql)
            # sql = "DELETE FROM G6_4"
            # cursor.execute(sql)

            field1 = "(count,profit,quantity,q,maxN,minN,Nq,Nmax,Nmin)"
            order1 = field1.split(",")

            field2 = "(id,Q,count,Cq,Cp,eps,Ccost,Cperfit,Fq,Fc,Fp,spend,pcost)"
            order2 = field2.split(",")

            # field = description1[1][0].replace("'", "")
            # query = data["count"]
            # sql1 = "INSERT INTO G6_1({0}) VALUES({1})".format(field, str(query))
            # cursor.execute(sql1)
            for k1, v1 in data["sheet1"].items():
                # field = []
                # for i in range(1, len(description1)):
                #     field.append(description1[i][0])
                # field = str(tuple(field)).replace("'", "")
                query = []
                # query.append(k1)
                for k2, v2 in v1.items():
                    query.append(v2)
                query = tuple(query)
                sql3 = "INSERT INTO G6_1{0} VALUES{1}".format(field1, str(query))
                print(sql3)
                cursor.execute(sql3)
                # print(sql3)

            for d in data["data"]:
                # field = []
                # for i in range(1, 14):
                #     field.append(description2[i][0])
                # field = str(tuple(field)).replace("'", "")
                query = []
                for k, v in d.items():
                    query.append(v)
                query = tuple(query)
                sql2 = "INSERT INTO G6_2{0} VALUES{1}".format(field2, str(query))
                cursor.execute(sql2)

            db.commit()

            if "Information" in data:
                setGraph(data)

            return True
        except Exception:
            db.rollback()
            print("error occurs and exception")
            return None
        finally:
            db.close()
    
    # new
    def addtion(self,data):
        db, cursor = opendb()
        try:
            today = datetime.datetime.now()
            date = today.strftime('%Y-%m-%d')
            sql0 = "DELETE FROM Graph WHERE page='sale_analysis' and date_format(time, '%Y-%m-%d')='"+date+"'"
            cursor.execute(sql0)

            sql = "DELETE FROM G6_1"
            cursor.execute(sql)
            sql = "DELETE FROM G6_2"
            cursor.execute(sql)

            field1 = "(count,profit,quantity,q,maxN,minN,Nq,Nmax,Nmin)"
            field2 = "(id,Q,count,Cq,Cp,eps,Ccost,Cperfit,Fq,Fc,Fp,spend,pcost)"

            for k1, v1 in data.get("data").items():
                query = []
                for k2, v2 in v1.items():
                    query.append(v2)
                query = tuple(query)
                sql2 = "INSERT INTO G6_1{0} VALUES{1}".format(field1, str(query))
                cursor.execute(sql2)
            for k3, v3 in data.get("data1").items():
                query = []
                for k4, v4 in v3.items():
                    query.append(v4)
                query = tuple(query)
                sql3 = "INSERT INTO G6_2{0} VALUES{1}".format(field2, str(query))
                cursor.execute(sql3)

            info = []
            for key in data:
                if key[:5] == 'sheet':
                    info.append(key)
            for sheetid in info:
                caption = data.get(sheetid).get('caption')
                axis_x = data.get(sheetid).get('axis_x')
                axis_y = data.get(sheetid).get('axis_y')
                for row in data.get(sheetid).get('data'):
                    id = row.get('id')
                    sheetid = sheetid
                    ctitle = row.get('title')
                    value = row.get('value')
                    field_data = "(id,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17,v18,v19,v20,v21,v22,v23,v24,page,sheet,caption,axis_x,axis_y,ctitle)"
                    query_data = (id,) + tuple(value) + ('sale_analysis',) + (sheetid,) + (caption,) + (axis_x,) + (
                    axis_y,) + (ctitle,)
                    sql0 = "INSERT INTO Graph{0} VALUES{1}".format(field_data, str(query_data))
                    cursor.execute(sql0)

            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()


    def post(self, request):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        if request.method == 'POST':
            data = json.loads(request.body)
            accesslog(data)
            if data["state"] > 0:
                if "subpage" in data:
                    if data.get('version') == 2:
                        res = self.new_querysubpage(data)
                    else:
                        res = self.querysubpage(data)
                    res["state"] = 2
                else:
                    id = data["user"]["id"]
                    if id in ["master", "iso"]:
                        res = self.query()
                    else:
                        res = self.query(id)
                    res["state"] = 1
                        # res = self.querydetail(data)

                # if res:
                #     # res["code"] = 0
                #     res["state"] = 2
                return Response(res)
            elif data["state"] == -1:
                if data.get('version') == 2:
                    res = self.addtion(data)
                else:
                    res = self.modify(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            else:
                return None



# 7.price analysis
class PostInfo7(APIView):

    def query_new(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        id = 'master'
        ret = getGraph(data["page"], id)
        print(ret)
        return ret

    def modify(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        ret = setGraph(data)
        return ret
        # db, cursor = opendb()
        # try:
        #     sql = "SELECT * FROM G7_new"
        #     description = []
        #     cursor.execute(sql)
        #     description = cursor.description
        #     sql = "DELETE FROM G7_new"
        #     cursor.execute(sql)
        #     print(description)

        #     field = []
        #     for i in range(1, len(description)):
        #         field.append(description[i][0])
        #     field = str(tuple(field)).replace("'", "")
        #     print(field)

        #     for c in range(1, 7):
        #         for k, v in data["sheet%d"%(c)].items():
        #             query = tuple([k]) + tuple(v) + tuple(["sheet%d"%(c)])
        #             query = str(tuple(query))
        #             sql = "INSERT INTO G7_new{0} VALUES{1}".format(field, query)
        #             print(sql)
        #             cursor.execute(sql)

        #     # description = [None]
        #     # for i in range(1, 7):
        #     #     sql = "SELECT * FROM G7_%d"%(i)
        #     #     cursor.execute(sql)
        #     #     description.append(cursor.description)
        #     #     sql = "DELETE FROM G7_%d"%(i)
        #     #     cursor.execute(sql)

        #     # for c in range(1, 7):
        #     #     field = []
        #     #     for i in range(1, 26):
        #     #         field.append(description[c][i][0])
        #     #     field = str(tuple(field)).replace("'", "")
        #     #     for k, v in data["sheet%d"%(c)].items():
        #     #         query = tuple([k]) + tuple(v)
        #     #         query = str(tuple(query))
        #     #         sql = "INSERT INTO G7_{0}{1} VALUES{2}".format(c, field, query)
        #     #         cursor.execute(sql)

        #     db.commit()
        #     return True
        # except Exception:
        #     db.rollback()
        #     return None
        # finally:
        #     db.close()

    def post(self, request):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        if request.method == 'POST':
            data = json.loads(request.body)
            accesslog(data)
            if data["state"] > 0:
                # if "user" in data:
                res = self.query_new(data)
                # else:
                #     res = self.query()
                res["state"] = data["state"]
                return Response(res)
            elif data["state"] == -1:
                res = self.modify(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            else:
                return None


class PostInfo9(APIView):

    def query(self, data):

        start = data["submit"]["start"]
        end = data["submit"]["end"]
        user = querylog(start, end)
        res = {"user": user}
        return res
        # print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        # db, cursor = opendb()
        # try:
        #     res = {}
        #     start = data["submit"]["start"]
        #     end = data["submit"]["end"]
        #     sql = "SELECT DISTINCT id FROM G1_3 WHERE time>='" + start + "' AND time<'"\
        #                  + end + "' AND page='config'" 
        #     print(sql)
        #     cursor.execute(sql)    
        #     results = cursor.fetchall() 
        #     records = [] 
        #     for submit in results:
        #         records.append(submit[0])
        #     res["user"] = records      
        #     print(res)       
        #     return res
        # except Exception:
        #     db.rollback()
        #     return None
        # finally:
        #     db.close() 

    def post(self, request):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        if request.method == 'POST':
            data = json.loads(request.body)
            if data["state"] > 0:
                res = self.query(data)
                res["state"] = data["state"]
                return Response(res)
            else:
                return None

# news
class PostInfoNews(APIView):
    def post(self,request):
        data = json.loads(request.body)
        if data["state"] == 1:
            content = data.get('content')
            research = content.get('research')
            if 'id' in content:
                res = self.detail_new(data)
            elif research == 'teams':
                res = self.teams(data)
            elif research == 'project':
                res = self.project(data)
            elif research == 'token':
                res = self.get_token()
            else:
                res = self.query_new(data)
            if res:
                res['code'] = 200
                return Response(res)
            else:
                return Response({"state": -400})
        elif data["state"] == -1:
            res = self.addtion_new(data)
            if res == True:
                return Response({"state": -200})
            else:
                return Response({"state": -400})
        elif data["state"] == -2:
            res = self.modify_new(data)
            if res == True:
                return Response({"state": -200})
            else:
                return Response({"state": -400})
        elif data["state"] == -3:
            res = self.delete_new(data)
            if res == True:
                return Response({"state": -200})
            else:
                return Response({"state": -400})
        else:
            return None
                
    def teams(self,data):
        result = {"teams":[]}
        teams_queryset = models.News.objects.using('default1').filter(type='团队成员').all()
        for team in teams_queryset:
            res = {}
            res['id'] = team.id
            res['title'] = team.title
            res['content'] = team.content
            res['pic'] = team.img
            res['type'] = team.type
            res['date'] = team.time.strftime('%Y-%m-%d').replace('-', '')
            result["teams"].append(res)
        return result

    def project(self,data):
        result = {"projects": []}
        project_queryset = models.News.objects.using('default1').filter(type='项目列表').all()
        for project in project_queryset:
            res = {}
            res['id'] = project.id
            res['title'] = project.title
            res['content'] = project.content
            res['pic'] = project.img
            res['type'] = project.type
            res['date'] = project.time.strftime('%Y-%m-%d').replace('-', '')
            result["projects"].append(res)
        return result

    def query_new(self,data):
        res = {"research": [], "business": []}
        results1 = models.News.objects.using('default1').filter(type='学术成果').order_by('-time')[:data.get('content').get('research')]
        results2 = models.News.objects.using('default1').filter(type='行业动态').order_by('-time')[:data.get('content').get('business')]
        for row1 in results1:
            info1 = {}
            info1['id'] = row1.id
            info1['title'] = row1.title
            info1['date'] = row1.time.strftime('%Y-%m-%d').replace('-', '')
            res['research'].append(info1)
        for row2 in results2:
            info2 = {}
            info2['id'] = row2.id
            info2['title'] = row2.title
            info2['date'] = row2.time.strftime('%Y-%m-%d').replace('-', '')
            res['business'].append(info2)
        return res
    
    def detail_new(self,data):
        id = data.get('content').get('id')
        news_obj = models.News.objects.using('default1').filter(id=id).first()
        if news_obj:
            res = {"content":{}}
            res['content']['id'] = news_obj.id
            res['content']['title'] = news_obj.title
            res['content']['content'] = news_obj.content
            res['content']['type'] = news_obj.type
            res['content']['pic'] = news_obj.img
            res['content']['date'] = news_obj.time.strftime('%Y-%m-%d').replace('-', '')
            return res
        return False
        
    def addtion_new(self,data):
        total_list = []
        for key,value in data.items():
            if type(data[key]) == list:
                if data[key]:
                    for row in data[key]:
                        content_type = key
                        title = row.get('title')
                        content = row.get('content')
                        img = row.get('pic')
                        type2=None
                        if content_type == 'research':
                            type2 = '学术成果'
                        elif content_type == 'business':
                            type2 = '行业动态'
                        elif content_type == 'teams':
                            type2 = '团队成员'
                        elif content_type == 'project':
                            type2 = '项目列表'
                        add_obj = models.News(title=title,content=content,type=type2,img=img)
                        total_list.append(add_obj)
        if total_list:
            models.News.objects.using('default1').bulk_create(total_list)
            return True

    def modify_new(self,data):
        for key,value in data.items():
            if type(data[key]) == list:
                if data[key]:
                    for row in data[key]:
                        content_type = key
                        id = row.get('id')
                        title = row.get('title')
                        content = row.get('content')
                        img = row.get('pic')
                        type1 = None
                        if content_type == 'research':
                            type1 = '学术成果'
                        elif content_type == 'business':
                            type1 = '行业动态'
                        elif content_type == 'teams':
                            type1 = '团队成员'
                        elif content_type == 'project':
                            type1 = '项目列表'
                        modify_obj = models.News.objects.using('default1').filter(id=id).first()
                        if modify_obj:
                            modify_obj.title = title
                            modify_obj.content = content
                            modify_obj.img = img
                            modify_obj.type = type1
                            modify_obj.save()
        return True

    def delete_new(self,data):
        id_list = data.get('id')
        for id in id_list:
            models.News.objects.filter()
            del_obj = models.News.objects.using('default1').filter(id=id).first()
            if del_obj:
                del_obj.delete()
            else:
                return False
        return True
    
    # qiniu upload token
    def get_token(self):
        res = {}
        # token过期(s)
        expires_time = 2592000  # 秒
        # 需要填写你的 Access Key 和 Secret Key
        access_key = "Na_6vEUqRPsyLQhAhT2jfG0AjUX2SXAv2RIc6Z7u"
        secret_key = "JEexH2PzvvjtzSkgLHCgY3CKe2EwuiXItsFVpHvZ"
        # 构建鉴权对象
        q = Auth(access_key, secret_key)
        # 要上传的空间
        bucket_name = "fiplatform"
        # 生成上传 Token，可以指定过期时间等
        token = q.upload_token(bucket_name, expires=expires_time)
        res['token'] = token
        return res

class PostInfoCapacity(APIView):
    def modify(self,data):
        capacity1 = data.get('capacity1')
        capacity2 = data.get('capacity2')
        capacity3 = data.get('capacity3')
        if capacity1 == None and capacity2 == None and capacity3 == None:
            return False
        if capacity1:
            models.Params.objects.using('default1').filter(name='capacity1').update(val=capacity1)
        if capacity2:
            models.Params.objects.using('default1').filter(name='capacity2').update(val=capacity2)
        if capacity3:
            models.Params.objects.using('default1').filter(name='capacity3').update(val=capacity3)
        return True

    def query(self,data):
        res = {}
        capacitys = models.Params.objects.using('default1').all()
        for obj in capacitys:
            if obj.name == 'capacity1':
                res["capacity1"] = obj.val
            if obj.name == 'capacity2':
                res["capacity2"] = obj.val
            if obj.name == 'capacity3':
                res["capacity3"] = obj.val
        return res

    def post(self,request):
        data = json.loads(request.body)
        if data["state"] == 1:
            res = self.query(data)
            return Response(res)
        elif data['state'] == -2:
            res = self.modify(data)
            if res:
                return Response({'state':-200})
            else:
                return Response({'state': -400})


