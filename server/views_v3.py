from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
import pymysql
import requests
import json
import sys
import datetime 
# from datetime import date, timedelta
# Create your views here.

graphTable = "G7_new" 
logTable = "logs"
 
def opendb():
    db = pymysql.connect(
        '114.215.24.89',
        'sd',
        'sd12345678',
        'api',
        port=3306,
        charset='utf8'
    )
    cursor = db.cursor()
    return db, cursor
 
def getRole(id):
    print("running in " + sys._getframe().f_code.co_name)
    roles = []
    for one in id:
        # print("id")     
        if one in ["master", "iso"]:
            roles.append(one[0])
        else:
            # print(one[3]) 
            roles.append(one[3])
    return roles

def getGraph(page, id):
    print("running in " + sys._getframe().f_code.co_name + ",page: " + page + ",id: " + id)
    db, cursor = opendb()  
    try:
        res = {}
        # page = data["page"]
        sql = "SELECT * FROM G7_new where page='" + page + "'"
        if id != "master":
            sql += " AND id='" + id + "'"
        cursor.execute(sql)
        results = cursor.fetchall()  

        captions = dict()

        # for c in range(1, 7):
        #     res["sheet%d"%(c)] = {}
        for row in results:
            sheetid = row[-4] 
            if sheetid not in res:
                res[sheetid] = {}
            num = len(res[sheetid])
            id = row[1]
            res[sheetid][num] = {}
            res[sheetid][num]["id"] = id
            res[sheetid][num]["value"] = list(row[2:26])
            if sheetid not in captions.keys():
                captions[sheetid] = [row[-3],row[-2],row[-1]]
        # print(captions)  
        for sheetid in captions.keys():
            res[sheetid]["caption"] = captions[sheetid][0]
            res[sheetid]["axis_x"] = captions[sheetid][1]
            res[sheetid]["axis_y"] = captions[sheetid][2]
        print("the got graph is: ")
        print(res)
        return res
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()

def setGraph(data):
    print("running in " + sys._getframe().f_code.co_name)
    db, cursor = opendb()
    try:
        page = data["page"]
        sql = "SELECT * FROM G7_new where page='" + page + "'"
        description = []  
        cursor.execute(sql)
        description = cursor.description
        sql = "DELETE FROM G7_new where page='" + page + "'"
        cursor.execute(sql)
        print(description)  

        field = []  
        for i in range(1, len(description)):
            field.append(description[i][0])
        field = str(tuple(field)).replace("'", "")
        print(field)

        # for c in range(1, 7):
        for k1, v1 in data["Information"].items():
            sheet = k1
            caption = v1["caption"]
            axis_x  = v1["axis_x"] 
            axis_y  = v1["axis_y"] 
            for k2, v2 in v1["data"].items():
                id = k2
                value = v2
                query = tuple([id]) + tuple(value) + tuple([page]) + tuple([sheet]) + tuple([caption]) + tuple([axis_x]) + tuple([axis_y])
                query = str(tuple(query))
                sql = "INSERT INTO G7_new{0} VALUES{1}".format(field, query)
                print(sql)
                cursor.execute(sql) 

        db.commit()
        return True
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()

def updateGraph(data):
    print("running in " + sys._getframe().f_code.co_name)
    db, cursor = opendb()
    try:
        page = data["page"]
        sql = "SELECT * FROM G7_new where page='" + page + "'"
        description = []  
        cursor.execute(sql)
        description = cursor.description

        field = []  
        for i in range(2, len(description)):
            field.append(description[i][0])
        # field = str(tuple(field)).replace("'", "")
        # print(field)  

        # for c in range(1, 7):
        for k1, v1 in data["Information"].items():
            sheet = k1
            caption = v1["caption"]
            axis_x  = v1["axis_x"] 
            axis_y  = v1["axis_y"]  
            # print(v1)
            for k2, v2 in v1["data"].items():
                id = k2 
                value = v2
                # sql = "UPDATE G7_new SET "
                sql = [] 
                # print(v2)  
                for k3 in range(0,24):
                    # sdj = field[k3] + "=" + str(value[k3])
                    # print(sdj)
                    sql.append(field[k3] + "=" + str(value[k3]))
                    # print(sql) 
                # print(sql)
                sql = "UPDATE G7_new SET " + ",".join(sql) + " where id='" + id + "'"
                    # sql += field[k3] + "=" + v3 + ","
                # print(sql)  
                cursor.execute(sql) 

                # query = tuple([id]) + tuple(value) + tuple([page]) + tuple([sheet]) + tuple([caption]) + tuple([axis_x]) + tuple([axis_y])
                # query = str(tuple(query))
                # sql = "UPDATE G7_new SET VALUES{1}".format(field, query)
                # print(sql)
                # cursor.execute(sql) 

        db.commit()
        return True
    except Exception:
        db.rollback()
        return None
    finally:
        db.close()

def accesslog(data):
    print("running in " + sys._getframe().f_code.co_name)
    db, cursor = opendb()
    id = data["user"]["id"]
    # if id == "master":
    #     return
    state = data["state"]
    page = data["page"]
    today=datetime.date.today() 
    date=today.strftime('%y%m%d')
    # print(date)
    field = ('id','date','page','state') 
    field = str(field).replace("'", "")
    query = str(tuple([id])+tuple([date])+tuple([page])+tuple([state])) 
    # print(query)
    sql = "INSERT INTO {0}{1} VALUES{2}".format(logTable,field, query)
    cursor.execute(sql)
    # print(sql)
    db.commit()
 
def querylog(start, end):
    print("running in " + sys._getframe().f_code.co_name)
    db, cursor = opendb()
    try:
        res = {}
        sql = "SELECT DISTINCT id FROM " + logTable + " WHERE time>='" + start + "' AND time<'"\
                        + end + "' AND page='config' And state < 0" 
        # print(sql)
        cursor.execute(sql)    
        results = cursor.fetchall() 
        records = [] 
        for submit in results:  
            if submit[0] not in {"master", "iso"}:
                records.append(submit[0])
        # res["user"] = records      
        # print(res)       
        return records
    except Exception:
        db.rollback()
        return None
    finally:
        db.close() 


class APInew(APIView):
    def post(self, request):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        data = json.loads(request.body)
        print(data)  
        # print("APInew") 
        print(self.__class__.__name__)
        if data["page"] == "front":
            page = PostInfo0()
            res = page.post(request)
            return res
        elif data["page"] == "config":
            page = PostInfo1()
            res = page.post(request)
            return res
        elif data["page"] == "history":
            page = PostInfo2()
            res = page.post(request)
            return res
        elif data["page"] == "match":
            page = PostInfo4()
            res = page.post(request)
            return res
        elif data["page"] == "gen_analysis":
            page = PostInfo5()
            res = page.post(request)
            return res
        elif data["page"] in ["sale_analysis"]: #, "sale_analysis_sub"]:
            page = PostInfo6()
            res = page.post(request) 
            return res
        elif data["page"] == "settle_analysis":
            page = PostInfo7()
            res = page.post(request)
            return res
        elif data["page"] == "price_analysis":
            page = PostInfo7()
            res = page.post(request)
            return res
        elif data["page"] == "stat":
            page = PostInfo9()
            res = page.post(request)
            return res
        else: 
            return None


class PostInfo0(APIView):

    def query(self):
        db, cursor = opendb()
        try:
            res = {}
            res["Information"] = []
            sql = "SELECT * FROM G0"
            cursor.execute(sql)
            results = cursor.fetchall()
            for row in results:
                temp = {
                    "id": row[1],
                    "x": list(row[2:7]),
                    "y": list(row[7:12])
                }
                res["Information"].append(temp)
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def modify(self, data):  
        db, cursor = opendb()
        try:
            sql = "DELETE FROM G0"    
            cursor.execute(sql)
            for d in data["Information"]:
                id = tuple([d["id"]])
                x = tuple(d["x"][0:5])
                y = tuple(d["y"][0:5])
                field = "(id, x1, x2, x3, x4, x5, y1, y2, y3, y4, y5)"
                query = id + x + y
                sql = "INSERT INTO G0{0} VALUES{1}".format(field, str(query))
                cursor.execute(sql)
            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def post(self, request):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        return Response({"state": -200})
        if request.method == 'POST':
            data = json.loads(request.body)
            if data["state"] > 0:
                res = {}
                # res = self.query()
                res["state"] = data["state"]+1
                return Response(res)
            elif data["state"] == -1:
                res = self.modify(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            else:
                return None

 
class PostInfo1(APIView):


    # def query(self,data):
    #     print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
    #     db, cursor = opendb()
    #     try:
    #         res = {}
    #         res["Information"] = {}
    #         res["Information"]["Info1"] = {}
    #         res["Information"]["Info2"] = {}
    #         sql = "SELECT * FROM G1_new"
    #         cursor.execute(sql)
    #         results = cursor.fetchall()
    #         Info1, Info2 = {}, {}
    #         num1, num2 = 0, 0
    #         for row in results:
    #             if row[1] == "G":
    #                 if row[2] not in Info1:
    #                     Info1[row[2]] = str(num1)
    #                     num1 += 1
    #                     res["Information"]["Info1"][Info1[row[2]]] = {
    #                         "id": row[2],
    #                         "value": []
    #                     }
    #                 res["Information"]["Info1"][Info1[row[2]]]["value"].append([row[3], row[4]])
    #             elif row[1] == "S":
    #                 if row[2] not in Info2:
    #                     Info2[row[2]] = str(num2)
    #                     num2 += 1
    #                     res["Information"]["Info2"][Info2[row[2]]] = {
    #                         "id": row[2],
    #                         "value": []
    #                     }
    #                 res["Information"]["Info2"][Info2[row[2]]]["value"].append([row[3], row[4]])                
    #         return res
    #     except Exception:
    #         db.rollback()
    #         return None
    #     finally:
    #         db.close() 

    # def querydetail(self,data):
    #     print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
    #     db, cursor = opendb()
    #     try:
    #         res = {}
    #         id = data["user"]["id"]
    #         res["Information"] = {}
    #         res["Information"]["Info1"] = {}
    #         res["Information"]["Info2"] = {}
    #         sql = "SELECT * FROM G1_new where id='"+id+"'"
    #         cursor.execute(sql)
    #         results = cursor.fetchall()
    #         Info1, Info2 = {}, {}
    #         num1, num2 = 0, 0
    #         for row in results:
    #             if row[1] == "G":
    #                 if row[2] not in Info1:
    #                     Info1[row[2]] = str(num1)
    #                     num1 += 1
    #                     res["Information"]["Info1"][Info1[row[2]]] = {
    #                         "id": row[2],
    #                         "value": []
    #                     }
    #                 res["Information"]["Info1"][Info1[row[2]]]["value"].append([row[3], row[4]])
    #             elif row[1] == "S":
    #                 if row[2] not in Info2:
    #                     Info2[row[2]] = str(num2)
    #                     num2 += 1
    #                     res["Information"]["Info2"][Info2[row[2]]] = {
    #                         "id": row[2],
    #                         "value": []
    #                     }
    #                 res["Information"]["Info2"][Info2[row[2]]]["value"].append([row[3], row[4]])                
    #         return res
    #     except Exception:
    #         db.rollback()
    #         return None
    #     finally:
    #         db.close()

    def query_new(self,data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb()
        try:  
            id = data["user"]["id"]
            role = getRole([id]) 

            res = {}
            res["Information"] = {}
            res["Information"]["sheet1"] = {}
            res["Information"]["sheet2"] = {}  
            if role[0] in ["S", "m"]: 
                Info = getGraph(data["page"], id) 
                for num in range(len(Info["sheet2"])-3):
                    value = Info["sheet2"][num]["value"]
                    # print(value)
                    Info["sheet2"][num]["value"] = [[i,v] for i,v in enumerate(value)]
                    # print(Info["sheet2"][num]["value"])

                res["Information"]["sheet2"] = Info["sheet2"]

            if role[0] in ["G", "m"]:
                if id == "master":
                    sql = "SELECT * FROM G1_1"
                else:
                    sql = "SELECT * FROM G1_1 where id='"+id+"'"

                cursor.execute(sql)
                results = cursor.fetchall()
                # print(results)
                Info1, Info2 = {}, {}
                num1, num2 = 0, 0
                for row in results: 
                    Info = {
                        "id": row[1],
                        "value": [[row[2], row[3]],[row[4], row[5]],[row[6], row[7]],\
                                [row[8], row[9]],[row[10], row[11]],[row[12], row[13]]]
                    }
                    res["Information"]["sheet1"][str(num1)] = Info
                    num1 += 1
                res["Information"]["sheet1"]["caption"] = "电价出力曲线"
                res["Information"]["sheet1"]["axis_x"] = "出力"
                res["Information"]["sheet1"]["axis_y"] = "报价"

            # else:
            #     Info = getGraph(data["page"], id)
            #     res["Information"] = Info
            #     res["Information"]["sheet1"] = {}
            #     if id == "master":
            #         sql = "SELECT * FROM G1_1"
            #     else:
            #         sql = "SELECT * FROM G1_1 where id='"+id+"'"

            #     cursor.execute(sql)
            #     results = cursor.fetchall()
            #     print(results)
            #     Info1, Info2 = {}, {}
            #     num1, num2 = 0, 0
            #     for row in results: 
            #         Info = {
            #             "id": row[1],
            #             "value": [[row[2], row[3]],[row[4], row[5]],[row[6], row[7]],\
            #                     [row[8], row[9]],[row[10], row[11]],[row[12], row[13]]]
            #         }
            #         res["Information"]["sheet1"][str(num1)] = Info
            #         num1 += 1
            #     res["Information"]["sheet1"]["caption"] = "电价出力曲线"
            #     res["Information"]["sheet1"]["axis_x"] = "出力"
            #     res["Information"]["sheet1"]["axis_y"] = "报价"

            # print(res)
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    # def modify1(self, data):
    #     print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
    #     db, cursor = opendb()
    #     try:
    #         sql = "SELECT * FROM G1_new"
    #         cursor.execute(sql)
    #         description = cursor.description
    #         sql = "DELETE FROM G1_new"
    #         cursor.execute(sql)
    #         field = []
    #         for i in range(1, 5):
    #             field.append(description[i][0])
    #         field = str(tuple(field)).replace("'", "")
    #         for k in data["Information"]["Info1"]:
    #             role = tuple(["G"])
    #             id = tuple([k["id"]])
    #             # query = role+id
    #             for d in k["value"]:
    #                 x, y = tuple([d[0]]), tuple([d[1]])
    #                 query = str(role + id + x + y)
    #                 sql = "INSERT INTO G1_new{0} VALUES{1}".format(field, query)
    #                 cursor.execute(sql)
    #         for k in data["Information"]["Info2"]:
    #             role = tuple(["S"])
    #             id = tuple([k["id"]])
    #             for d in k["value"]:
    #                 x, y = tuple([d[0]]), tuple([d[1]])
    #                 query = str(role + id + x + y)
    #                 sql = "INSERT INTO G1_new{0} VALUES{1}".format(field, query)
    #                 cursor.execute(sql)
    #         db.commit()
    #         return True
    #     except Exception:
    #         db.rollback()
    #         return None
    #     finally:
    #         db.close()

    def reset(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb()
        try:
            sql = "SELECT * FROM G1_1"
            cursor.execute(sql)
            description = cursor.description
            sql = "DELETE FROM G1_1"
            cursor.execute(sql)
            
            field = []
            for i in range(1, len(description)):
                field.append(description[i][0])
            field = str(tuple(field)).replace("'", "")
            for k in data["Information"]["Info1"]:
                # role = tuple(["G"])
                id = tuple([k["id"]]) 
                query = id
                for d in k["value"]:
                    x, y = tuple([d[0]]), tuple([d[1]])
                    query += x+y

                print(query)
                query = str(query)
                sql = "INSERT INTO G1_1{0} VALUES{1}".format(field, query)
                print(sql)
                cursor.execute(sql) 
                # db.commit() 
            del data["Information"]["Info1"]
            print("data is :")
            print(data)
            setGraph(data)

            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def update(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb()
        try:
            if 'sheet1' in data["Information"]:
                id = data["Information"]['sheet1'][0]['id']
                value = data["Information"]['sheet1'][0]["value"]

                sql = "SELECT * FROM G1_1 where id='"+id+"'"
                cursor.execute(sql)
                description = cursor.description
                sql = "DELETE FROM G1_1 where id='"+id+"'"
                cursor.execute(sql) 

                field = [] 
                for i in range(1, len(description)):
                    field.append(description[i][0])
                field = str(tuple(field)).replace("'", "")
                for k in data["Information"]["sheet1"]:
                    # role = tuple(["G"])
                    id = tuple([k["id"]]) 
                    query = id 
                    for d in k["value"]:
                        x, y = tuple([float(d[0])]), tuple([float(d[1])]) 
                        query += x+y

                    print(query)
                    query = str(query)
                    sql = "INSERT INTO G1_1{0} VALUES{1}".format(field, query)
                    print(sql)
                    cursor.execute(sql) 
                    # db.commit() 
                del data["Information"]["sheet1"]
            elif 'sheet2' in data["Information"]:
                updateGraph(data) 
            # print(sql)
             
            # today=datetime.date.today() 
            # # print(today) 
            # date=today.strftime('%y%m%d')
            # # print(date)
            # field = ('id','date','page') 
            # field = str(field).replace("'", "")
            # query = str(tuple([id])+tuple([date])+tuple(['config'])) 
            # # print(query)
            # sql = "INSERT INTO G1_3{0} VALUES{1}".format(field, query)
            # cursor.execute(sql)
            # # print(sql)

            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()
      
    def modify0(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb()
        try:
            if 'Info1' in data["Information"]:
                Info = 'Info1'
                role = tuple(["G"])
            else:
                Info = 'Info2'
                role = tuple(["S"])

            id = data["Information"][Info][0]['id']
            value = data["Information"][Info][0]["value"]

            sql = "SELECT * FROM G1_new where id='"+id+"'"
            cursor.execute(sql)
            description = cursor.description
            sql = "DELETE FROM G1_new where id='"+id+"'"
            cursor.execute(sql) 

            field = []
            for i in range(1, 5):
                field.append(description[i][0])
            field = str(tuple(field)).replace("'", "")

            for d in value: 
                # print(d)
                x, y = tuple([d[0]]), tuple([d[1]])
                query = str(role + tuple([id]) + x + y)
                sql = "INSERT INTO G1_new{0} VALUES{1}".format(field, query)
                cursor.execute(sql)
            # print(sql)
            
            # today=datetime.date.today() 
            # # print(today) 
            # date=today.strftime('%y%m%d')
            # # print(date)
            # field = ('id','date','page') 
            # field = str(field).replace("'", "")
            # query = str(tuple([id])+tuple([date])+tuple(['config'])) 
            # # print(query)
            # sql = "INSERT INTO G1_3{0} VALUES{1}".format(field, query)
            # cursor.execute(sql)
            # # print(sql)

            db.commit()
            return True
        except Exception: 
            db.rollback()
            return None
        finally:
            db.close()

    def post(self, request): 
        print(self.__class__.__name__)
        if request.method == 'POST':
            data = json.loads(request.body)
            accesslog(data)     
            
            # if data["state"] == 1:
            #     if "user" not in data:
            #         res = self.query(data)
            #         # return None                    
            #     elif data["user"]["id"] in ["master", "iso"]:
            #         res = self.query(data)
            #     else:
            #         res = self.querydetail(data)
                    
            #     res["state"] = data["state"]
            #     return Response(res)
            if data["state"] == 2:
                res = self.query_new(data)
                res["state"] = data["state"]
                return Response(res)
            elif data["state"] == 0: 
                res = self.modify0(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            elif data["state"] == -3:
                res = self.update(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            # elif data["state"] == -1:
            #     res = self.modify1(data)
            #     if res == True:
            #         return Response({"state": -200})
            #     else:
            #         return Response({"state": -400})
            elif data["state"] == -2:
                res = self.reset(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            else:
                return None


class PostInfo2(APIView):

    def query(self):
        db, cursor = opendb()
        try:
            res = {}
            res["Information"] = {}
            sql = "SELECT * FROM G2"
            cursor.execute(sql)
            results = cursor.fetchall()
            cnt = 0
            res["Information"][1] = {}
            res["Information"][0] = {}
            for row in results:
                c = row[1]
                # if row[1] not in res["Information"]:
                #     res["Information"][row[1]] = {}
                if row[2] not in res["Information"][c]:
                    res["Information"][c][row[2]] = {}
                res["Information"][c][row[2]]["id"] = row[3]
                res["Information"][c][row[2]]["shape"] = row[4]
                if "data" not in res["Information"][c][row[2]]:
                    res["Information"][c][row[2]]["data"] = {} 
                res["Information"][c][row[2]]["data"][row[5]] = list(row[6:18])
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def modify(self, data):
        db, cursor = opendb()
        try:
            sql = "SELECT * FROM G2"
            cursor.execute(sql)
            description = cursor.description
            sql = "DELETE FROM G2"
            cursor.execute(sql)
            for k1, v1 in data["Information"].items():
                id1 = tuple([k1])
                for k2, v2 in v1.items():
                    id2 = tuple([k2])
                    id3 = tuple([v2["id"]])
                    shape = tuple([v2["shape"]])
                    for k3, v3 in v2["data"].items():
                        date = tuple([k3])
                        value = tuple(v3)
                        field = []
                        for i in range(1, 18):
                            field.append(description[i][0])
                        field = str(tuple(field)).replace("'", "")
                        query = id1 + id2 + id3 + shape + date + value
                        sql = "INSERT INTO G2{0} VALUES{1}".format(field, str(query))
                        cursor.execute(sql)

            
            today=datetime.date.today() 
            # print(today) 
            date=today.strftime('%y%m%d')
            # print(date)
            field = ('id','date','page') 
            field = str(field).replace("'", "")
            query = str(tuple([id])+tuple([date])+tuple(['history'])) 
            # print(query)
            sql = "INSERT INTO G1_3{0} VALUES{1}".format(field, query)
            cursor.execute(sql)
            # print(sql)

            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def addition(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb() 
        try: 
            sql = "SELECT * FROM G2_12_new"
            # print(sql)
            cursor.execute(sql)
            description = cursor.description
            date = tuple([data["date"]])
            id1 = tuple(["sheet1"]) 
            for k1, v1 in data["sheet1"].items():
                id2 = tuple([k1])
                print("id2") 
                for k2, v2 in v1.items():  
                    id3 = tuple([k2])
                    value = tuple(v2)  
                    # print(value)
                    field = []
                    for i in range(1, 29):   
                        field.append(description[i][0])
                    field = str(tuple(field)).replace("'", "")
                    # print(field)
                    query = id1 + id2 + id3 + date + value
                    print(date)  
                    sql = "INSERT INTO G2_12_new{0} VALUES{1}".format(field, str(query))
                    # print(sql)
                    cursor.execute(sql)


            # print("start to modify")
            sql = "SELECT * FROM G2_3_new"
            cursor.execute(sql)
            description = cursor.description
            # print(len(description))
            field = [] 
            for i in range(1, len(description)): 
                field.append(description[i][0])

            for k, v in data["sheet3"].items():
                # query = [v1["id"]] + date
                # print(field[0])
                newfield = []
                query=[]
                for k1,v1 in v.items():
                    if k1 in field:
                        newfield.append(k1)
                        query.append(v1)
                query = tuple(query) + date
                newfield += ["date"]
                newfield = str(tuple(newfield)).replace("'", "")
                # print(newfield)
                # print(query)
                sql2 = "INSERT INTO G2_3_new{0} VALUES{1}".format(newfield, str(query))
                # print(sql2)
                cursor.execute(sql2)

            sql = "SELECT * FROM G2_4_new"
            cursor.execute(sql)
            description = cursor.description
            # print(len(description))
            field = [] 
            for i in range(1, len(description)): 
                field.append(description[i][0])
            # field = str(tuple(field)).replace("'", "")
            # print(field)

            for k, v in data["sheet4"].items():
                # query = [v1["id"]] + date
                # print(field[0])
                newfield = []
                query=[]
                for k1,v1 in v.items():
                    if k1 in field:
                        newfield.append(k1)
                        query.append(v1)
                query = tuple(query) + date
                newfield += ["date"]
                newfield = str(tuple(newfield)).replace("'", "")
                # print(newfield)
                # print(query)
                sql2 = "INSERT INTO G2_4_new{0} VALUES{1}".format(newfield, str(query))
                # print(sql2)
                cursor.execute(sql2)

            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def post(self, request): 
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        if request.method == 'POST':
            data = json.loads(request.body)
            accesslog(data)
            if data["state"] > 0:
                res = self.query()
                res["state"] = data["state"]
                return Response(res)
            elif data["state"] == -1:
                res = self.modify(data)
                if res == True: 
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            elif data["state"] == -2:
                res = self.addition(data)
                if res == True: 
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            else:
                return None 


class PostInfo4(APIView):
    def query_old(self):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb()
        try:
            res = {}
            sql = "SELECT * FROM G3"
            cursor.execute(sql)
            results = cursor.fetchall()
            for row in results:
                res["point1"] = list(row[1:25])
                res["point2"] = list(row[25:49])
                res["point3"] = list(row[49:73])
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()
  
    def query(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        # ret = getGraph(data["page"], "master")

        today=datetime.date.today() 
        yesterday = datetime.date.today() - datetime.timedelta(days=1)
        tomorrow = datetime.date.today() + datetime.timedelta(days=1)
        userdone = querylog(today.strftime('%y%m%d'),tomorrow.strftime('%y%m%d'))
        # print("userdone:") 
        # print(userdone) 

        db, cursor = opendb() 
        sql = "SELECT id FROM G1_1"
        # print(sql)
        cursor.execute(sql)
        results = cursor.fetchall() 
        # print(results)
        userG = [row[0] for row in results]  
        # print("userG")
        # print(userG)  
  
        sql = "SELECT id FROM " + graphTable + " where page = 'config' "
        # print(sql)
        cursor.execute(sql)
        results = cursor.fetchall()
        userS = [row[0] for row in results]

        userGdone = [user for user in userG if user in userdone]
        userGdoing = [user for user in userG if user not in userdone]
        # print("userG") 
        # print(userGdone)  
        # print(userGdoing)  
        userSdone = [user for user in userS if user in userdone]
        userSdoing = [user for user in userS if user not in userdone]
        # print("userS")  
        # print(userSdone) 
        # print(userSdoing)  
        # ret = {"state": 2}
        ret = {0: userGdoing, 1: userGdone, 
                        2: userSdoing, 3: userSdone, 
                        "ratio": [round(len(userGdone)/len(userG),3),
                                round(len(userSdone)/len(userS),3)]
                }
        return ret
 
    def work(self):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        data = {"state": -1}
        url = "http://114.215.111.61/rest_service"
        data_json = json.dumps(data)
        headers = {'Content-type': 'application/json'}
        response = requests.post(url, data=data_json, headers=headers)
        return True 

    def modify(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        ret = setGraph(data)
        
        return ret

    def post(self, request):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        # if request.method == 'POST':
        data = json.loads(request.body)
        data["user"] = {"id": "master"}
        accesslog(data)
        res = None
        if data["state"] > 0:
            res = self.query(data)
            res["state"] = data["state"]
            return Response(res)
        elif data["state"] == -1:
            res = self.work() 
            # if res != None:
            #     return Response({"state": -200})
            # else:
            #     return Response({"state": -400})
        elif data["state"] == -2:
            res = self.modify(data)
            # if res != None:
            #     return Response({"state": -200})
            # else:
            #     return Response({"state": -400})
        # else:
        #     return None

        if res != None:
            return Response({"state": -200})
        else:
            return Response({"state": -400})

 
class PostInfo5(APIView):

    def query(self):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb()
        try:
            res = {}
            sql1 = "SELECT * FROM G5_1"
            cursor.execute(sql1)
            results = cursor.fetchall()
            for row in results:
                res["quantity"] = row[1]
                res["profit"] = row[2]
                res["count"] = row[3]
            sql2 = "SELECT * FROM G5_2"
            cursor.execute(sql2)
            results = cursor.fetchall()
            res["sheet1"] = {}
            for row in results:
                temp = {
                    "name":row[1],
                    "Nq": row[2],
                    "Nmax": row[3],
                    "Nmin": row[4],
                    "Cq": row[5],
                    "Cmax": row[6],
                    "Cmin": row[7]
                }
                res["sheet1"][row[1]] = temp

            # sql3 = "SELECT * FROM G5_3"
            # cursor.execute(sql3)
            # results = cursor.fetchall()
            # res["sheet2"] = {}
            # for row in results:
            #     if row[1] not in res["sheet2"]:
            #         res["sheet2"][row[1]] = {}
            #     temp = {
            #         "id": row[1],
            #         "Q": row[2],
            #         "count": row[3],
            #         "Cq": row[4],
            #         "Cp": row[5],
            #         "eps": row[6],
            #         "Ccost": row[7],
            #         "CPerfit": row[8],
            #         "Fq": row[9],
            #         "Fc": row[10],
            #         "Fp": row[11],
            #         "spend": row[12],
            #         "pcost": row[13]
            #     }
            #     res["sheet2"][row[1]]["info"] = temp

            #     res["sheet2"][row[1]]["sheet2_1"] = {}
            #     res["sheet2"][row[1]]["sheet2_1"]["t1"] = []
            #     res["sheet2"][row[1]]["sheet2_1"]["t2"] = []
            #     res["sheet2"][row[1]]["sheet2_1"]["t3"] = []
            #     res["sheet2"][row[1]]["sheet2_1"]["t4"] = []
            #     res["sheet2"][row[1]]["sheet2_2"] = {}
            # return res
                

            # sql4 = "SELECT * FROM G5_4"
            # cursor.execute(sql4)
            # results = cursor.fetchall()
            # for row in results:
            #     if row[1] not in res["sheet2"]:
            #         res["sheet2"][row[1]] = {}
            #     temp = {
            #         "t1": list(row[2:12]),
            #         "t2": list(row[12:22]),
            #         "t3": list(row[22:32]),
            #         "t4": list(row[32:42]),
            #     }
            #     res["sheet2"][row[1]]["sheet2_1"] = temp
            # sql5 = "SELECT * FROM G5_5"
            # cursor.execute(sql5)
            # results = cursor.fetchall()
            # for row in results:
            #     if row[1] not in res["sheet2"]:
            #         res["sheet2"][row[1]] = {}
            #     temp = {
            #         "minL": row[3],
            #         "maxL": row[4],
            #         "price": row[5]
            #     }
            #     if "sheet2_2" not in res["sheet2"][row[1]]:
            #         res["sheet2"][row[1]]["sheet2_2"] = {}
            #     res["sheet2"][row[1]]["sheet2_2"][row[2]] = temp
            
            # res["sheet2"] = {}

            sql6 = "SELECT * FROM G5_6"
            cursor.execute(sql6)
            results = cursor.fetchall()
            res["data"] = {}
            for row in results:
                temp = {
                    "id": row[1],
                    "Cq": row[2],
                    "Nq": row[3],
                    "delta": row[4],
                    "Np": row[5],
                    "Cp": row[6],
                    "Nperfit": row[7],
                    "Npperfit": row[8],
                    "Fq": row[9],
                    "Fp": row[10],
                    "Fperfit": row[11],
                    "perfit": row[12],
                    "pperfit": row[13]
                }
                res["data"][row[1]] = temp
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def querydetail(self,data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb()
        try:
            res = {}
            id = data["user"]["id"]
            res["quantity"] = 0
            res["profit"] = 0
            res["count"] = 0
            res["sheet1"] = {}
            res["sheet2"] = {}
            sql6 = "SELECT * FROM G5_6 where id='"+id+"'"
            cursor.execute(sql6)
            results = cursor.fetchall()
            res["data"] = {}
            for row in results:
                temp = {
                    "id": row[1],
                    "Cq": row[2],
                    "Nq": row[3],
                    "delta": row[4],
                    "Np": row[5],
                    "Cp": row[6],
                    "Nperfit": row[7],
                    "Npperfit": row[8],
                    "Fq": row[9],
                    "Fp": row[10],
                    "Fperfit": row[11],
                    "perfit": row[12],
                    "pperfit": row[13]
                }
                res["data"][row[1]] = temp
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def modify(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb()
        try:
            sql1 = "SELECT * FROM G5_1"
            cursor.execute(sql1)
            description1 = cursor.description
            sql2 = "SELECT * FROM G5_2"
            cursor.execute(sql2)
            description2 = cursor.description
            # sql3 = "SELECT * FROM G5_3"
            # cursor.execute(sql3)
            # description3 = cursor.description
            # sql4 = "SELECT * FROM G5_4"
            # cursor.execute(sql4)
            # description4 = cursor.description
            # sql5 = "SELECT * FROM G5_5"
            # cursor.execute(sql5)
            # description5 = cursor.description
            sql6 = "SELECT * FROM G5_6"
            cursor.execute(sql6)
            description6 = cursor.description
            sql1 = "DELETE FROM G5_1" 
            cursor.execute(sql1)
            sql2 = "DELETE FROM G5_2"
            cursor.execute(sql2)
            # sql3 = "DELETE FROM G5_3"
            # cursor.execute(sql3)
            # sql4 = "DELETE FROM G5_4"
            # cursor.execute(sql4)
            # sql5 = "DELETE FROM G5_5"
            # cursor.execute(sql5)
            sql6 = "DELETE FROM G5_6"
            cursor.execute(sql6)
            field = [description1[1][0], description1[2][0], description1[3][0]]
            field = str(tuple(field)).replace("'", "")
            query = (data["quantity"], data["profit"], data["count"])
            sql1 = "INSERT INTO G5_1{0} VALUES{1}".format(field, str(query))
            cursor.execute(sql1)
            for k1, v1 in data["sheet1"].items():
                field = []
                for i in range(1, 8):
                    field.append(description2[i][0])
                field = str(tuple(field)).replace("'", "")
                query = []
                for k2, v2 in v1.items():
                    query.append(v2)
                query = tuple(query)
                sql2 = "INSERT INTO G5_2{0} VALUES{1}".format(field, str(query))
                cursor.execute(sql2)

            # for k1, v1 in data["sheet2"].items():
            #     field = []
            #     for i in range(1, 14):
            #         field.append(description3[i][0])
            #     field = str(tuple(field)).replace("'", "")
            #     query = []
            #     for k2, v2 in v1["info"].items():
            #         query.append(v2)
            #     query = tuple(query)
            #     sql3 = "INSERT INTO G5_3{0} VALUES{1}".format(field, str(query))
                
            #     cursor.execute(sql3)
            #     field = []
            #     for i in range(1, 42):
            #         field.append(description4[i][0])
            #     field = str(tuple(field)).replace("'", "")
            #     query = tuple([k1])
            #     query = query + tuple(v1["sheet2_1"]["t1"])
            #     query = query + tuple(v1["sheet2_1"]["t2"])
            #     query = query + tuple(v1["sheet2_1"]["t3"])
            #     query = query + tuple(v1["sheet2_1"]["t4"])
            #     sql4 = "INSERT INTO G5_4{0} VALUES{1}".format(field, str(query))
            #     cursor.execute(sql4)
            #     for k2, v2 in v1["sheet2_2"].items():
            #         field = []
            #         for i in range(1, 6):
            #             field.append(description5[i][0])
            #         field = str(tuple(field)).replace("'", "")
            #         query = []
            #         query.append(k1)
            #         query.append(k2)
            #         query.append(v2["minL"])
            #         query.append(v2["maxL"])
            #         query.append(v2["price"])
            #         query = tuple(query)
            #         sql5 = "INSERT INTO G5_5{0} VALUES{1}".format(field, str(query))
            #         cursor.execute(sql5)

            for k1, v1 in data["data"].items():
                field = []
                for i in range(1, 14):
                    field.append(description6[i][0])
                field = str(tuple(field)).replace("'", "")
                query = []
                for k2, v2 in v1.items():
                    query.append(v2)
                query = tuple(query)
                sql6 = "INSERT INTO G5_6{0} VALUES{1}".format(field, str(query)) 
                cursor.execute(sql6)
 
            # # print(sql6)
            # id = data["user"]["id"]
            # today=datetime.date.today() 
            # print(today)  
            # date=today.strftime('%y%m%d')
            # # print(date)
            # field = ('id','date','page') 
            # field = str(field).replace("'", "")
            # query = str(tuple([id])+tuple([date])+tuple(['history'])) 
            # # print(query)
            # sql = "INSERT INTO G1_3{0} VALUES{1}".format(field, query)
            # cursor.execute(sql)
            # # print(sql)

            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def post(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            accesslog(data)
            if data["state"] > 0:
                if "user" not in data:
                    res = self.query()                
                elif data["user"]["id"] in ["master", "iso"]:
                    res = self.query()
                else:
                    res = self.querydetail(data)

                res["state"] = data["state"]
                return Response(res)
            elif data["state"] == -1:
                res = self.modify(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            else:
                return None
 
    
class PostInfo6(APIView):
 
    def query(self):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb()
        try:
            res = {}
            sql = "SELECT * FROM G6_1"
            cursor.execute(sql)
            results = cursor.fetchall()
            res["sheet1"] = {}
            res["label"] = {} 
            # res["label"]["count"] = row[1]
            # res["label"]["profit"] = row[2]
            # res["label"]["quantity"] = row[3]
            for row in results:
                temp = {
                    "q": row[4],
                    "maxN": row[5],
                    "minN": row[6],
                    "Nq": row[7],
                    "Nmax": row[8],
                    "Nmin": row[9]
                }
                res["sheet1"][0] = temp 
                res["count"] = row[1]
                res["profit"] = row[2]
                res["quantity"] = row[3]

            sql = "SELECT * FROM G6_2"
            cursor.execute(sql)
            results = cursor.fetchall()
            res["data"] = []
            for row in results:
                temp = {
                    "id": row[1],
                    "Q": row[2],
                    "count": row[3],
                    "Cq": row[4],
                    "Cp": row[5],
                    "eps": row[6],
                    "Ccost": row[7],
                    "Cperfit": row[8],
                    "Fq": row[9],
                    "Fc": row[10],
                    "Fp": row[11],
                    "spend": row[12],
                    "pcost": row[13]
                }
                res["data"].append(temp)

            # sql = "SELECT * FROM G6_4"
            # cursor.execute(sql)
            # results = cursor.fetchall()
            # res["sheet2"] = {}
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()
    
    def querydetail(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        print(data)
        db, cursor = opendb() 
        try:
            res = {}
            id = data["user"]["id"]
            sql = "SELECT * FROM G6_2 where id='" + id + "'"
            cursor.execute(sql)
            results = cursor.fetchall()
            res["data"] = []
            for row in results:
                temp = {
                    "id": row[1],
                    "Q": row[2],
                    "count": row[3],
                    "Cq": row[4],
                    "Cp": row[5],
                    "eps": row[6],
                    "Ccost": row[7],
                    "Cperfit": row[8],
                    "Fq": row[9],
                    "Fc": row[10],
                    "Fp": row[11],
                    "spend": row[12],
                    "pcost": row[13]
                }
                res["data"].append(temp)

            res["sheet1"] = {}
            res["sheet2"] = {}
            res["count"] = 0
            res["profit"] = 0
            res["quantity"] = 0
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()
     
    def querysubpage(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb()
        try:   
            # id = data["user"]["id"]
            print(data)
            id = data["id"]  
            print(id)
            res = getGraph(data["page"], id) 
            print(res)
            # sql = "SELECT * FROM G6_2 where id='" + id + "'"
            # print(sql)
            # cursor.execute(sql)
            # results = cursor.fetchall()
            # print(results)
            # res["data"] = {}
            # for row in results: 
            #     print(row)
            #     res["data"]["id"] = row[1],
            #     res["data"]["Q"] = row[2],
            #     res["data"]["count"] = row[3],
            #     res["data"]["Cq"] = row[4],
            #     res["data"]["Cp"] = row[5],
            #     res["data"]["eps"] = row[6],
            #     res["data"]["Ccost"] = row[7],
            #     res["data"]["Cperfit"] = row[8],
            #     res["data"]["Fq"] = row[9],
            #     res["data"]["Fc"] = row[10],
            #     res["data"]["Fp"] = row[11],
            #     res["data"]["spend"] =row[12],
            #     res["data"]["pcost"] = row[13]
            # print("result is:")
            # print(res)
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()
 
    def modify(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb()
        try:
            # print("start to modify")
            sql = "SELECT * FROM G6_1"
            cursor.execute(sql)
            description1 = cursor.description
            sql = "SELECT * FROM G6_2"
            cursor.execute(sql)
            description2 = cursor.description
            # sql = "SELECT * FROM G6_3" 
            # cursor.execute(sql)
            # description3 = cursor.description
            # sql = "SELECT * FROM G6_4" 
            # cursor.execute(sql)
            # description4 = cursor.description
            # print(description4)

            sql = "DELETE FROM G6_1"
            cursor.execute(sql)
            sql = "DELETE FROM G6_2"
            cursor.execute(sql)
            # sql = "DELETE FROM G6_3"
            # cursor.execute(sql)
            # sql = "DELETE FROM G6_4"
            # cursor.execute(sql)


            # field = description1[1][0].replace("'", "")
            # query = data["count"]
            # sql1 = "INSERT INTO G6_1({0}) VALUES({1})".format(field, str(query))
            # cursor.execute(sql1)
            for k1, v1 in data["sheet1"].items():
                field = [] 
                for i in range(1, len(description1)):
                    field.append(description1[i][0])
                field = str(tuple(field)).replace("'", "")
                query = [] 
                # query.append(k1) 
                for k2, v2 in v1.items():
                    query.append(v2)
                query = tuple(query)
                sql3 = "INSERT INTO G6_1{0} VALUES{1}".format(field, str(query))
                print(sql3)
                cursor.execute(sql3)
                # print(sql3)

            # for k1, v1 in data["sheet2"].items():
            #     field = []
            #     id = v1["id"]
            #     label = v1["label"]
            #     value = v1["value"]
            #     # print(value)

            #     for i in range(1,27):
            #         field.append(description4[i][0])
            #     field = str(tuple(field)).replace("'", "")
            #     # print(field)
            #     query = []
            #     query.append(id)
            #     query.append(label)
            #     for v2 in value:
            #         query.append(v2)
            #     query = tuple(query)
            #     sql = "INSERT INTO G6_4{0} VALUES{1}".format(field, str(query))
            #     cursor.execute(sql) 

            for d in data["data"]: 
                field = []
                for i in range(1, 14):
                    field.append(description2[i][0])
                field = str(tuple(field)).replace("'", "")
                query = []
                for k, v in d.items():
                    query.append(v)   
                query = tuple(query)
                sql2 = "INSERT INTO G6_2{0} VALUES{1}".format(field, str(query))
                cursor.execute(sql2)

            if "Information" in data:
                setGraph(data)

            db.commit()
            return True
        except Exception:
            db.rollback()
            print("error occurs and exception")
            return None
        finally:
            db.close()

    def post(self, request):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        if request.method == 'POST':
            data = json.loads(request.body)
            accesslog(data)
            if data["state"] > 0:
                if "subpage" in data:
                    res = self.querysubpage(data)
                else:
                    if "user" not in data:
                        res = self.query()                
                    elif data["user"]["id"] in ["master", "iso"]:
                        res = self.query()
                    else:
                        # res = self.query()
                        res = self.querydetail(data)

                if res:
                    res["state"] = data["state"]
                return Response(res)
            elif data["state"] == -1:
                res = self.modify(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            else:
                return None


class PostInfo7(APIView):

    def query(self):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb()
        try:
            res = {}
            for c in range(1, 7):
                res["sheet%d"%(c)] = {}
                sql = "SELECT * FROM G7_%d"%(c)
                cursor.execute(sql)
                results = cursor.fetchall()
                for row in results:
                    res["sheet%d"%(c)][row[1]] = list(row[2:26])

            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def query_new(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        id = 'master'
        ret = getGraph(data["page"], id)
        print(ret)
        return ret
        # db, cursor = opendb()
        # try:
        #     res = {}
        #     page = data["page"]
        #     sql = "SELECT * FROM G7_new where page='" + page + "'"
        #     cursor.execute(sql)
        #     results = cursor.fetchall()  
        #     # print(results)
        #     # caption = ["日前线路潮流图","实时线路潮流图","节点出力曲线","机组出力曲线","售电侧出力曲线","线路潮流曲线"]
        #     # axis_x  = ['时间','时间','时间','时间','时间','时间']
        #     # axis_y  = ['潮流/容量(%)','潮流/容量(%)','出力（MW)','出力（MW)','负荷（MW)','潮流/容量(%)']
            
        #     # for c in range(1, len(caption)+1):
        #     #     res["sheet%d"%(c)] = {}
        #     # for row in results:
        #     #     sheetid = row[-1]
        #     #     num = len(res[sheetid])
        #     #     id = row[1]
        #     #     res[sheetid][num] = {}
        #     #     res[sheetid][num]["id"] = id
        #     #     res[sheetid][num]["value"] = list(row[2:26])
        #     # for c in range(1, len(caption)+1):
        #     #     res["sheet%d"%(c)]["id"] = caption[c-1]
        #     #     res["sheet%d"%(c)]["axis_x"] = axis_x[c-1]
        #     #     res["sheet%d"%(c)]["axis_y"] = axis_y[c-1]

        #     captions = dict()

        #     # for c in range(1, 7):
        #     #     res["sheet%d"%(c)] = {}
        #     for row in results:
        #         sheetid = row[-4] 
        #         if sheetid not in res:
        #             res[sheetid] = {}
        #         num = len(res[sheetid])
        #         id = row[1]
        #         res[sheetid][num] = {}
        #         res[sheetid][num]["id"] = id
        #         res[sheetid][num]["value"] = list(row[2:26])
        #         if sheetid not in captions.keys():
        #             captions[sheetid] = [row[-3],row[-2],row[-1]]
        #     print(captions) 
        #     for sheetid in captions.keys():
        #         res[sheetid]["caption"] = captions[sheetid][0]
        #         res[sheetid]["axis_x"] = captions[sheetid][1]
        #         res[sheetid]["axis_y"] = captions[sheetid][2]
        #     return res
        # except Exception:
        #     db.rollback()
        #     return None
        # finally:
        #     db.close()
  
    def modify(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        ret = setGraph(data)
        return ret
        # db, cursor = opendb()
        # try:
        #     sql = "SELECT * FROM G7_new"
        #     description = [] 
        #     cursor.execute(sql)
        #     description = cursor.description
        #     sql = "DELETE FROM G7_new"
        #     cursor.execute(sql)
        #     print(description)  

        #     field = []
        #     for i in range(1, len(description)):
        #         field.append(description[i][0])
        #     field = str(tuple(field)).replace("'", "")
        #     print(field)

        #     for c in range(1, 7):
        #         for k, v in data["sheet%d"%(c)].items():
        #             query = tuple([k]) + tuple(v) + tuple(["sheet%d"%(c)])
        #             query = str(tuple(query))
        #             sql = "INSERT INTO G7_new{0} VALUES{1}".format(field, query)
        #             print(sql)
        #             cursor.execute(sql) 

 
        #     # description = [None]
        #     # for i in range(1, 7):
        #     #     sql = "SELECT * FROM G7_%d"%(i)
        #     #     cursor.execute(sql)
        #     #     description.append(cursor.description)
        #     #     sql = "DELETE FROM G7_%d"%(i)
        #     #     cursor.execute(sql)

        #     # for c in range(1, 7):
        #     #     field = []
        #     #     for i in range(1, 26):
        #     #         field.append(description[c][i][0])
        #     #     field = str(tuple(field)).replace("'", "")
        #     #     for k, v in data["sheet%d"%(c)].items():
        #     #         query = tuple([k]) + tuple(v)
        #     #         query = str(tuple(query))
        #     #         sql = "INSERT INTO G7_{0}{1} VALUES{2}".format(c, field, query)
        #     #         cursor.execute(sql)

        #     db.commit()
        #     return True
        # except Exception:
        #     db.rollback()
        #     return None
        # finally:
        #     db.close()

    def post(self, request):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        if request.method == 'POST':
            data = json.loads(request.body)
            accesslog(data)
            if data["state"] > 0:
                if "user" in data:
                    res = self.query_new(data)
                else:
                    res = self.query()
                res["state"] = data["state"]
                return Response(res)
            elif data["state"] == -1:
                res = self.modify(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            else:
                return None


class PostInfo8_old(APIView):

    def query(self):
        db, cursor = opendb()
        try:
            res = {}
            for c in range(1, 4):
                res["sheet%d"%(c)] = {}
                sql = "SELECT * FROM G8_%d"%(c)
                cursor.execute(sql)
                results = cursor.fetchall()
                for row in results:
                    res["sheet%d"%(c)][row[1]] = list(row[2:26])

            res["sheet4"] = {}
            sql = "SELECT * FROM G8_4"
            cursor.execute(sql)
            results = cursor.fetchall()
            for row in results:
                res["sheet4"][row[1]] = list(row[2:12])
            
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def query_new(self):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb()
        try:
            res = {}
            sql = "SELECT * FROM G8_new"
            cursor.execute(sql)
            results = cursor.fetchall()
            # caption = ["节点电价曲线","节点日前电价","节点实时电价"]
            # axis_x  = ['时间','时间','时间']
            # axis_y  = ['电价','电价','电价']
 
            captions = dict()

            # for c in range(1, 4):
            #     res["sheet%d"%(c)] = {}
            for row in results:
                sheetid = row[-4]
                print(row)
                print(sheetid) 
                if sheetid not in res:
                    res[sheetid] = {}
                num = len(res[sheetid])
                id = row[1] 
                res[sheetid][num] = {}
                res[sheetid][num]["id"] = id
                res[sheetid][num]["value"] = list(row[2:26])
                if sheetid not in captions.keys():
                    captions[sheetid] = [row[-3],row[-2],row[-1]]
            print(captions) 
            for sheetid in captions.keys():
                res[sheetid]["caption"] = captions[sheetid][0]
                res[sheetid]["axis_x"] = captions[sheetid][1]
                res[sheetid]["axis_y"] = captions[sheetid][2]

            # for c in range(1, len(caption)+1):
            #     res["sheet%d"%(c)]["id"] = caption[c-1]
            #     res["sheet%d"%(c)]["axis_x"] = axis_x[c-1]
            #     res["sheet%d"%(c)]["axis_y"] = axis_y[c-1]
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def modify(self, data):
        # print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        # db, cursor = opendb()
        # try:
        #     description = [None]
        #     for i in range(1, 6):
        #         sql = "SELECT * FROM G8_%d"%(i)
        #         cursor.execute(sql)
        #         description.append(cursor.description)
        #         sql = "DELETE FROM G8_%d"%(i)
        #         cursor.execute(sql)

        #     for c in range(1, 4):
        #         field = []
        #         for i in range(1, 26):
        #             field.append(description[c][i][0])
        #         field = str(tuple(field)).replace("'", "")
        #         for k, v in data["sheet%d"%(c)].items():
        #             query = tuple([k]) + tuple(v)
        #             query = str(tuple(query))
        #             sql = "INSERT INTO G8_{0}{1} VALUES{2}".format(c, field, query)
        #             cursor.execute(sql)

        #     field = []
        #     for i in range(1, 12):
        #         field.append(description[4][i][0])
        #     field = str(tuple(field)).replace("'", "")
        #     for k, v in data["sheet4"].items():
        #         query = tuple([k]) + tuple(v)
        #         query = str(tuple(query))
        #         sql = "INSERT INTO G8_4{0} VALUES{1}".format(field, query)
        #         cursor.execute(sql)

        #     db.commit()
        #     return True
        # except Exception:
        #     db.rollback()
        #     return None
        # finally:
        #     db.close()
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        ret = setGraph(data)
        return ret

    def modify_new(self, data):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        db, cursor = opendb()
        try:
            sql = "SELECT * FROM G8_new"
            description = [] 
            cursor.execute(sql)
            description = cursor.description
            sql = "DELETE FROM G8_new"
            cursor.execute(sql)
            print(description)  

            field = []
            for i in range(1, len(description)):
                field.append(description[i][0])
            field = str(tuple(field)).replace("'", "")
            print(field)
  
            for c in range(1, 4):
                for k, v in data["sheet%d"%(c)].items():
                    query = tuple([k]) + tuple(v) + tuple(["sheet%d"%(c)])
                    query = str(tuple(query))
                    sql = "INSERT INTO G8_new{0} VALUES{1}".format(field, query)
                    print(sql)
                    cursor.execute(sql) 

            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()
 
    def post(self, request):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        if request.method == 'POST':
            data = json.loads(request.body)
            accesslog(data)
            if data["state"] > 0:
                if "user" in data:
                    res = self.query_new()
                else:
                    res = self.query()
                res["state"] = data["state"]
                return Response(res)
            elif data["state"] == -1:
                if "user" in data:  
                    res = self.modify_new(data)
                else:
                    res = self.modify(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            else:
                return None


class PostInfo9(APIView): 

    def query(self,data):

        start = data["submit"]["start"]
        end = data["submit"]["end"]
        user = querylog(start, end)
        res = {"user":user}
        return res
        # print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        # db, cursor = opendb()
        # try:
        #     res = {}
        #     start = data["submit"]["start"]
        #     end = data["submit"]["end"]
        #     sql = "SELECT DISTINCT id FROM G1_3 WHERE time>='" + start + "' AND time<'"\
        #                  + end + "' AND page='config'" 
        #     print(sql)
        #     cursor.execute(sql)    
        #     results = cursor.fetchall() 
        #     records = [] 
        #     for submit in results:
        #         records.append(submit[0])
        #     res["user"] = records      
        #     print(res)       
        #     return res
        # except Exception:
        #     db.rollback()
        #     return None
        # finally:
        #     db.close() 

    def post(self, request):
        print("running in " + self.__class__.__name__ + "::" + sys._getframe().f_code.co_name)
        if request.method == 'POST':
            data = json.loads(request.body)
            if data["state"] > 0:
                res = self.query(data)                     
                res["state"] = data["state"]
                return Response(res)
            else:
                return None


class PostInfo2_old(APIView):

    def query(self):
        db, cursor = opendb()
        try:
            res = {}
            res["Information"] = {}
            sql = "SELECT * FROM G2"
            cursor.execute(sql)
            results = cursor.fetchall()
            cnt = 0
            res["Information"][1] = {}
            res["Information"][0] = {}
            for row in results:
                c = row[1]
                # if row[1] not in res["Information"]:
                #     res["Information"][row[1]] = {}
                if row[2] not in res["Information"][c]:
                    res["Information"][c][row[2]] = {}
                res["Information"][c][row[2]]["id"] = row[3]
                res["Information"][c][row[2]]["shape"] = row[4]
                if "data" not in res["Information"][c][row[2]]:
                    res["Information"][c][row[2]]["data"] = {} 
                res["Information"][c][row[2]]["data"][row[5]] = list(row[6:18])
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def modify(self, data):
        db, cursor = opendb()
        try:
            sql = "SELECT * FROM G2"
            cursor.execute(sql)
            description = cursor.description
            sql = "DELETE FROM G2"
            cursor.execute(sql)
            for k1, v1 in data["Information"].items():
                id1 = tuple([k1])
                for k2, v2 in v1.items():
                    id2 = tuple([k2])
                    id3 = tuple([v2["id"]])
                    shape = tuple([v2["shape"]])
                    for k3, v3 in v2["data"].items():
                        date = tuple([k3])
                        value = tuple(v3)
                        field = []
                        for i in range(1, 18):
                            field.append(description[i][0])
                        field = str(tuple(field)).replace("'", "")
                        query = id1 + id2 + id3 + shape + date + value
                        sql = "INSERT INTO G2{0} VALUES{1}".format(field, str(query))
                        cursor.execute(sql)
            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def post(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            if data["state"] > 0:
                res = self.query()
                res["state"] = data["state"]
                return Response(res)
            elif data["state"] == -1:
                res = self.modify(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            else:
                return None


class PostInfo3_new(APIView):

    def query(self):
        db, cursor = opendb()
        try:
            res = {}
            sql = "SELECT * FROM G3_new"
            cursor.execute(sql)
            results = cursor.fetchall()
            cnt = 0
            for row in results:
                res[str(cnt)] = {}
                res[str(cnt)]["id"] = row[1]
                res[str(cnt)]["shape"] = 0
                res[str(cnt)]["value"] = list(row[2:26])
                cnt += 1
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def modify(self, data):
        db, cursor = opendb()
        try:
            sql = "SELECT * FROM G3_new"
            cursor.execute(sql)
            description = cursor.description
            sql = "DELETE FROM G3_new"
            cursor.execute(sql)
            field = []
            for i in range(1, 26):
                field.append(description[i][0])
            field = str(tuple(field)).replace("'", "")
            for k, v in data.items():
                if k == "state":
                    continue
                name = tuple([v["id"]])
                value = tuple(v["value"])
                query = str(name + value)
                sql = "INSERT INTO G3_new{0} VALUES{1}".format(field, query)
                cursor.execute(sql)
            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()    

    def post(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            if data["state"] > 0:
                res = self.query()
                res["state"] = data["state"]
                return Response(res)
            elif data["state"] == -1:
                res = self.modify(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            else:
                return None


class PostInfo4_new(APIView):

    def query(self):
        db, cursor = opendb()
        try:
            res = {}
            sql = "SELECT * FROM G3_new"
            cursor.execute(sql)
            results = cursor.fetchall()
            cnt = 0
            for row in results:
                res[str(cnt)] = {}
                res[str(cnt)]["id"] = row[1]
                res[str(cnt)]["shape"] = 0
                res[str(cnt)]["value"] = list(row[2:26])
                cnt += 1
            return res
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()

    def modify(self, data):
        db, cursor = opendb()
        try:
            sql = "SELECT * FROM G3_new"
            cursor.execute(sql)
            description = cursor.description
            sql = "DELETE FROM G3_new"
            cursor.execute(sql)
            field = []
            for i in range(1, 26):
                field.append(description[i][0])
            field = str(tuple(field)).replace("'", "")
            for k, v in data.items():
                if k == "state":
                    continue
                name = tuple([v["id"]])
                value = tuple(v["value"])
                query = str(name + value)
                sql = "INSERT INTO G3_new{0} VALUES{1}".format(field, query)
                cursor.execute(sql)
            db.commit()
            return True
        except Exception:
            db.rollback()
            return None
        finally:
            db.close()    

    def work(self):
        data = {"state": -1}
        url = "http://114.215.111.61/rest_service"
        data_json = json.dumps(data)
        headers = {'Content-type': 'application/json'}
        response = requests.post(url, data=data_json, headers=headers)
        return True

    def post(self, request):
        if request.method == 'POST':
            data = json.loads(request.body)
            if data["state"] > 0:
                res = self.query()
                res["state"] = data["state"]
                return Response(res)
            elif data["state"] == -2: 
                res = self.modify(data)
                if res == True:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            elif data["state"] == -1:
                res = self.work()
                if res != None:
                    return Response({"state": -200})
                else:
                    return Response({"state": -400})
            else:
                return None

