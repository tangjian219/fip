# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AuthGroup(models.Model):
    title = models.CharField(max_length=100)
    status = models.IntegerField()
    rules = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupAccess(models.Model):
    uid = models.IntegerField()
    group_id = models.PositiveIntegerField()

    class Meta:
        managed = False
        db_table = 'auth_group_access'
        unique_together = (('uid', 'group_id'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=50)
    content_type_id = models.IntegerField()
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'


class AuthRule(models.Model):
    name = models.CharField(unique=True, max_length=80)
    title = models.CharField(max_length=20)
    type = models.IntegerField()
    status = models.IntegerField()
    condition = models.CharField(max_length=100)
    pid = models.PositiveIntegerField()
    nav = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'auth_rule'


class Config(models.Model):
    name = models.CharField(max_length=50, blank=True, null=True)
    value = models.TextField(blank=True, null=True)
    type = models.CharField(max_length=50, blank=True, null=True)
    states = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'config'


class DjangoContentType(models.Model):
    name = models.CharField(max_length=100)
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class Manager(models.Model):
    user = models.CharField(max_length=100)
    pwd = models.CharField(max_length=100)
    display = models.IntegerField()
    lasttime = models.IntegerField(blank=True, null=True)
    registertime = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'manager'

class G1(models.Model):
    general_id = models.AutoField(primary_key=True)
    id = models.CharField(max_length=20, blank=True, null=True)
    x1 = models.FloatField(blank=True, null=True)
    y1 = models.FloatField(blank=True, null=True)
    x2 = models.FloatField(blank=True, null=True)
    y2 = models.FloatField(blank=True, null=True)
    x3 = models.FloatField(blank=True, null=True)
    y3 = models.FloatField(blank=True, null=True)
    x4 = models.FloatField(blank=True, null=True)
    y4 = models.FloatField(blank=True, null=True)
    x5 = models.FloatField(blank=True, null=True)
    y5 = models.FloatField(blank=True, null=True)
    x6 = models.FloatField(blank=True, null=True)
    y6 = models.FloatField(blank=True, null=True)
    time = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'G1'


class G23New(models.Model):
    general_id = models.AutoField(primary_key=True)
    id = models.CharField(max_length=20, blank=True, null=True)
    cq = models.FloatField(db_column='Cq', blank=True, null=True)  # Field name made lowercase.
    cp = models.FloatField(db_column='Cp', blank=True, null=True)  # Field name made lowercase.
    eps = models.FloatField(blank=True, null=True)
    ccost = models.FloatField(db_column='Ccost', blank=True, null=True)  # Field name made lowercase.
    cperfit = models.FloatField(db_column='CPerfit', blank=True, null=True)  # Field name made lowercase.
    fq = models.FloatField(db_column='Fq', blank=True, null=True)  # Field name made lowercase.
    fc = models.FloatField(db_column='Fc', blank=True, null=True)  # Field name made lowercase.
    fp = models.FloatField(db_column='Fp', blank=True, null=True)  # Field name made lowercase.
    spend = models.FloatField(blank=True, null=True)
    pcost = models.FloatField(blank=True, null=True)
    dq = models.FloatField(db_column='Dq', blank=True, null=True)  # Field name made lowercase.
    dp = models.FloatField(db_column='Dp', blank=True, null=True)  # Field name made lowercase.
    load_profits = models.FloatField(blank=True, null=True)
    load_pure_profits = models.FloatField(blank=True, null=True)
    time = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'G2_3_new'


class G24New(models.Model):
    general_id = models.AutoField(primary_key=True)
    id = models.CharField(max_length=20, blank=True, null=True)
    cq = models.FloatField(db_column='Cq', blank=True, null=True)  # Field name made lowercase.
    cp = models.FloatField(db_column='Cp', blank=True, null=True)  # Field name made lowercase.
    delta = models.FloatField(blank=True, null=True)
    nperfit = models.FloatField(db_column='Nperfit', blank=True, null=True)  # Field name made lowercase.
    npperfit = models.FloatField(db_column='Npperfit', blank=True, null=True)  # Field name made lowercase.
    fq = models.FloatField(db_column='Fq', blank=True, null=True)  # Field name made lowercase.
    fp = models.FloatField(db_column='Fp', blank=True, null=True)  # Field name made lowercase.
    fperfit = models.FloatField(db_column='Fperfit', blank=True, null=True)  # Field name made lowercase.
    perfit = models.FloatField(blank=True, null=True)
    pperfit = models.FloatField(blank=True, null=True)
    pmin = models.FloatField(blank=True, null=True)
    sp1 = models.FloatField(blank=True, null=True)
    k1 = models.FloatField(blank=True, null=True)
    sp2 = models.FloatField(blank=True, null=True)
    k2 = models.FloatField(blank=True, null=True)
    sp3 = models.FloatField(blank=True, null=True)
    k3 = models.FloatField(blank=True, null=True)
    sp4 = models.FloatField(blank=True, null=True)
    k4 = models.FloatField(blank=True, null=True)
    pmax = models.FloatField(blank=True, null=True)
    k5 = models.FloatField(blank=True, null=True)
    nq = models.FloatField(db_column='Nq', blank=True, null=True)  # Field name made lowercase.
    np = models.FloatField(db_column='Np', blank=True, null=True)  # Field name made lowercase.
    gen_cost = models.FloatField(blank=True, null=True)
    gen_pure_profits = models.FloatField(blank=True, null=True)
    time = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'G2_4_new'


class G51(models.Model):
    general_id = models.AutoField(primary_key=True)
    quantity = models.FloatField(blank=True, null=True)
    profit = models.FloatField(blank=True, null=True)
    count = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'G5_1'


class G52(models.Model):
    general_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=20, blank=True, null=True)
    nq = models.FloatField(db_column='Nq', blank=True, null=True)  # Field name made lowercase.
    nmax = models.FloatField(db_column='Nmax', blank=True, null=True)  # Field name made lowercase.
    nmin = models.FloatField(db_column='Nmin', blank=True, null=True)  # Field name made lowercase.
    cq = models.FloatField(db_column='Cq', blank=True, null=True)  # Field name made lowercase.
    cmax = models.FloatField(db_column='Cmax', blank=True, null=True)  # Field name made lowercase.
    cmin = models.FloatField(db_column='Cmin', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'G5_2'


class G56(models.Model):
    generai_id = models.AutoField(primary_key=True)
    id = models.CharField(max_length=20, blank=True, null=True)
    cq = models.FloatField(db_column='Cq', blank=True, null=True)  # Field name made lowercase.
    nq = models.FloatField(db_column='Nq', blank=True, null=True)  # Field name made lowercase.
    delta = models.FloatField(blank=True, null=True)
    np = models.FloatField(db_column='Np', blank=True, null=True)  # Field name made lowercase.
    cp = models.FloatField(db_column='Cp', blank=True, null=True)  # Field name made lowercase.
    nperfit = models.FloatField(db_column='Nperfit', blank=True, null=True)  # Field name made lowercase.
    npperfit = models.FloatField(db_column='Npperfit', blank=True, null=True)  # Field name made lowercase.
    fq = models.FloatField(db_column='Fq', blank=True, null=True)  # Field name made lowercase.
    fp = models.FloatField(db_column='Fp', blank=True, null=True)  # Field name made lowercase.
    fperfit = models.FloatField(db_column='Fperfit', blank=True, null=True)  # Field name made lowercase.
    perfit = models.FloatField(blank=True, null=True)
    pperfit = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'G5_6'


class G61(models.Model):
    general_id = models.AutoField(primary_key=True)
    count = models.IntegerField(blank=True, null=True)
    profit = models.FloatField(blank=True, null=True)
    quantity = models.FloatField(blank=True, null=True)
    q = models.FloatField(blank=True, null=True)
    maxn = models.FloatField(db_column='maxN', blank=True, null=True)  # Field name made lowercase.
    minn = models.FloatField(db_column='minN', blank=True, null=True)  # Field name made lowercase.
    nq = models.FloatField(db_column='Nq', blank=True, null=True)  # Field name made lowercase.
    nmax = models.FloatField(db_column='Nmax', blank=True, null=True)  # Field name made lowercase.
    nmin = models.FloatField(db_column='Nmin', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'G6_1'


class G62(models.Model):
    general_id = models.AutoField(primary_key=True)
    id = models.CharField(max_length=20, blank=True, null=True)
    q = models.FloatField(db_column='Q', blank=True, null=True)  # Field name made lowercase.
    count = models.FloatField(blank=True, null=True)
    cq = models.FloatField(db_column='Cq', blank=True, null=True)  # Field name made lowercase.
    cp = models.FloatField(db_column='Cp', blank=True, null=True)  # Field name made lowercase.
    eps = models.FloatField(blank=True, null=True)
    ccost = models.FloatField(db_column='Ccost', blank=True, null=True)  # Field name made lowercase.
    cperfit = models.FloatField(db_column='CPerfit', blank=True, null=True)  # Field name made lowercase.
    fq = models.FloatField(db_column='Fq', blank=True, null=True)  # Field name made lowercase.
    fc = models.FloatField(db_column='Fc', blank=True, null=True)  # Field name made lowercase.
    fp = models.FloatField(db_column='Fp', blank=True, null=True)  # Field name made lowercase.
    spend = models.FloatField(blank=True, null=True)
    pcost = models.FloatField(blank=True, null=True)
    date = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'G6_2'


class Graph(models.Model):
    generai_id = models.AutoField(primary_key=True)
    id = models.CharField(max_length=20, blank=True, null=True)
    v1 = models.FloatField(blank=True, null=True)
    v2 = models.FloatField(blank=True, null=True)
    v3 = models.FloatField(blank=True, null=True)
    v4 = models.FloatField(blank=True, null=True)
    v5 = models.FloatField(blank=True, null=True)
    v6 = models.FloatField(blank=True, null=True)
    v7 = models.FloatField(blank=True, null=True)
    v8 = models.FloatField(blank=True, null=True)
    v9 = models.FloatField(blank=True, null=True)
    v10 = models.FloatField(blank=True, null=True)
    v11 = models.FloatField(blank=True, null=True)
    v12 = models.FloatField(blank=True, null=True)
    v13 = models.FloatField(blank=True, null=True)
    v14 = models.FloatField(blank=True, null=True)
    v15 = models.FloatField(blank=True, null=True)
    v16 = models.FloatField(blank=True, null=True)
    v17 = models.FloatField(blank=True, null=True)
    v18 = models.FloatField(blank=True, null=True)
    v19 = models.FloatField(blank=True, null=True)
    v20 = models.FloatField(blank=True, null=True)
    v21 = models.FloatField(blank=True, null=True)
    v22 = models.FloatField(blank=True, null=True)
    v23 = models.FloatField(blank=True, null=True)
    v24 = models.FloatField(blank=True, null=True)
    page = models.CharField(max_length=20, blank=True, null=True)
    sheet = models.CharField(max_length=20, blank=True, null=True)
    caption = models.CharField(max_length=20, blank=True, null=True)
    axis_x = models.CharField(max_length=20, blank=True, null=True)
    axis_y = models.CharField(max_length=20, blank=True, null=True)
    ctitle = models.CharField(max_length=20, blank=True, null=True)
    time = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Graph'



class Logs(models.Model):
    general_id = models.AutoField(primary_key=True)
    id = models.CharField(max_length=11, blank=True, null=True)
    date = models.CharField(max_length=11, blank=True, null=True)
    page = models.CharField(max_length=20, blank=True, null=True)
    time = models.DateTimeField(blank=True, null=True)
    state = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'logs'


class News(models.Model):
    title = models.CharField(max_length=64, blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    img = models.CharField(max_length=32, blank=True, null=True)
    type = models.CharField(max_length=10, blank=True, null=True)
    time = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'news'

class Params(models.Model):
    name = models.CharField(max_length=10, blank=True, null=True)
    val = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'params'